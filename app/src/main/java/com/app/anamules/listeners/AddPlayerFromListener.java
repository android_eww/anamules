package com.app.anamules.listeners;

import android.app.Activity;

public interface AddPlayerFromListener
{
    void onAddPlayer(Class activity,int position);
    void onAddPlayerFromContact(int position);
}
