package com.app.anamules.listeners;

import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.golfCourse.Datum;

public interface GolfCourseSelection
{
    void onSelectedRecent(String from, int position, GetRecentCourses datum);
}
