package com.app.anamules.listeners;

public interface EditListener
{
    void onClickEdit(int position);
}
