package com.app.anamules.listeners;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface
{
    @Headers("key:anamules@!&$@!!")
    @GET("api/User/Init/{version}/{deviceType}/{userID}/{lat}/{long}")
    Call<Object> callInit(@Path("version") String version, @Path("deviceType") String deviceType, @Path("userID") String userID, @Path("lat") String lat, @Path("long") String longs);


    @FormUrlEncoded
    @Headers("key:anamules@!&$@!!")
    @POST("api/User/UserSignup")
    Call<Object> callRegister(@Field("FirstName") String firstName,
                              @Field("LastName") String LastName,
                              @Field("Email") String Email,
                              @Field("Zipcode") String Zipcode,
                              @Field("Password") String Password,
                              @Field("Phone") String Phone,
                              @Field("SocialID") String SocialID,
                              @Field("SocialType") String SocialType,
                              @Field("Latitude") String Latitude,
                              @Field("Longitude") String Longitude,
                              @Field("DeviceToken") String DeviceToken,
                              @Field("DeviceOS") String DeviceOS);


    @FormUrlEncoded
    @Headers("key:anamules@!&$@!!")
    @POST("api/User/Login")
    Call<Object> calllogin(@Field("Email") String username,
                              @Field("Latitude") String lat,
                              @Field("Longitude") String lng,
                              @Field("DeviceToken") String device_token,
                              @Field("DeviceOS") String DeviceOS,
                              @Field("Password") String password);


    @FormUrlEncoded
    @Headers("key:anamules@!&$@!!")
    @POST("api/User/ForgotPassword")
    Call<Object> callForgotPassword(@Field("Email") String email);


    @FormUrlEncoded
    @POST("api/User/ChangePassword")
    Call<Object> callChangePass(@Field("ID") String ID,
                           @Field("OldPassword") String OldPassword,
                           @Field("NewPassword") String NewPassword);

    @FormUrlEncoded
    @GET("api/GolfCourse/GetCourses/{Lat}/{Lng}")
    Call<Object> callGetCoursesNear(@Path("Lat") String Lat,
                                @Path("Lng") String Lng);

    @FormUrlEncoded
    @GET("api/GolfCourse/GetCourses/{courseId}")
    Call<Object> callGetCoursesById(@Path("courseId") String courseId);

    @FormUrlEncoded
    @GET("api/GolfCourse/GetTeeBoxes/{courseId}")
    Call<Object> callGetTeeBoxes(@Path("courseId") String courseId);

    @FormUrlEncoded
    @GET("api/GolfCourse/GetHoles/{courseId}")
    Call<Object> callGetHoles(@Path("courseId") String courseId);

    @FormUrlEncoded
    @GET("api/GolfCourse/GetRecentCourses/{UserId}")
    Call<Object> callGetRecentCorses(@Path("UserId") String UserId);

}
