package com.app.anamules.listeners;

import com.app.anamules.been.golfCourse.Datum;

public interface GolfCourseSelectionNear {
    void onSelectedRV(String From, int position, Datum datum);
}
