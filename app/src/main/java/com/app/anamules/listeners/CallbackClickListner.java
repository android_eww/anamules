package com.app.anamules.listeners;

import android.view.View;

public interface CallbackClickListner
{
    void onClickListener(int position);
}
