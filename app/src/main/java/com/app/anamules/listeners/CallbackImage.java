package com.app.anamules.listeners;

import com.app.anamules.activity.parent.ImageActivity;

public interface CallbackImage
{
    void openImageChooser(ImageActivity.CallbackImageListener callbackImageListener);
}
