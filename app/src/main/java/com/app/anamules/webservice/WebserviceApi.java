package com.app.anamules.webservice;

import retrofit2.Call;

public interface WebserviceApi
{

    String BASE_URL = "http://anamules.excellentwebworld.in/";
    String HEADER_KEY = "x-api-key";

    String API_INIT = "Init/";

    String API_REGISTER = "UserSignup";
    String REGISTER_FIRSTNAME = "FirstName";
    String REGISTER_LASTNAME = "LastName";
    String REGISTER_EMAIL = "Email";
    String REGISTER_ZIPCODE = "Zipcode";
    String REGISTER_PASSWORD = "Password";
    String REGISTER_PHONE = "Phone";
    String REGISTER_SOCIALID = "SocialType";
    String REGISTER_SOCIALTYPE = "SocialType";
    String REGISTER_LAT = "Latitude";
    String REGISTER_LON = "Longitude";
    String REGISTER_TOKEN = "DeviceToken";
    String REGISTER_OS = "DeviceOS";

    String KEY_ID = "ID";


    String API_CHANGEPASSWORD = BASE_URL+"api/User/ChangePassword";
    String API_CHANGEPASSWORD_ID = "ID";
    String API_CHANGEPASSWORD_OLDPASS = "OldPassword";
    String API_CHANGEPASSWORD_NEWPASS = "NewPassword";

    String API_UPDATEPROFILE = BASE_URL+"api/User/ProfileUpdate";
    String API_UPDATEPROFILE_ID = "UserId";
    String API_UPDATEPROFILE_FNAME = "FirstName";
    String API_UPDATEPROFILE_LNAME = "LastName";
    String API_UPDATEPROFILE_EMAIL = "Email";
    String API_UPDATEPROFILE_ZIPCODE = "Zipcode";
    String API_UPDATEPROFILE_LAT = "Latitude";
    String API_UPDATEPROFILE_LNG = "Longitude";
    String API_UPDATEPROFILE_PHONE = "Phone";
    String API_UPDATEPROFILE_IMG = "ProfilePicture";


    String API_GOLF_COURSE = BASE_URL+"api/GolfCourse/GetCoursesFromCity/";
    String API_RECENT_PLAYER = BASE_URL + "api/GolfGame/RecentPlayer/";
    String API_ANAMULES_ANIMAL_LIST  = BASE_URL + "api/Anamules/AnamuleList/";

    String API_ANIMLIST = BASE_URL+"api/Anamules/AnimalList";

    String API_ADD_PLAYER = BASE_URL + "api/GolfGame/AddPlayer";
    String ADD_PLAYER_FNAME = "FirstName";
    String ADD_PLAYER_LNAME = "LastName";
    String ADD_PLAYER_EMAIL = "Email";
    String ADD_PLAYER_ZIPCODE = "ZipCode";
    String ADD_PLAYER_IMG = "ProfilePicture";
    String ADD_PLAYER_USERID = "UserID";
    String ADD_PLAYER_TYPE = "PlayerType";

    String API_GET_RECENT_COURSES = BASE_URL + "api/GolfCourse/GetRecentCourses/";

    String API_START_GAME = BASE_URL + "api/GolfGame/StartGame";
    String KEY_COURSE_ID = "CourseID";
    String KEY_OWNER_USER_ID = "OwnerUserID";
    String KEY_PLAYER_LIST = "Playerlist";

    String API_SCORE_GAME_HISTORY = BASE_URL + "api/Score/GameHistory";
    String API_USER_ID = "UserId";
    String API_ANAMULES_RESET_ANAMULES = BASE_URL + "api/Anamules/ResetAnamules/";
    String API_GOLF_COUSE_UPDATE_SCORE = BASE_URL + "api/GolfCourse/UpdateScore";
    String KEY_PLAYER_ID = "PlayerID";
    String KEY_HOLE_ID = "HoleID";
    String KEY_SCORE = "Score";

    String API_GOLF_GAME_INITIAL_START_GAME = BASE_URL + "api/GolfGame/InitialStartGame/";
    String API_GOLFGAME_UPDATE_COURSE = BASE_URL + "api/GolfGame/UpdateCourse";
    String KEY_GOLFBERT_ID = "GolfbertID";
    String KEY_GAME_ID = "GameID";
    String KEY_NAME = "Name";

    String API_EDIT_PLAYER  = BASE_URL + "api/GolfGame/EditPlayer";
    String KEY_FIRST_NAME = "FirstName";
    String KEY_LAST_NAME = "LastName";


    String API_GOLF_GAME_END = BASE_URL + "api/GolfGame/EndGame";
    String API_GOLF_COURSE_TOTAL_UPDATED_SCORE_HOLEWISE = BASE_URL + "api/GolfCourse/TotalUpdatedScoreHoleWise";
    String API_GOLF_COURSE_SCORE_CARD  = BASE_URL + "api/GolfCourse/ScoreCard/";

    String API_SCORE_TAKE_ANAMULE = BASE_URL + "api/score/TakeAnamule";
    String KEY_ANAMULE_ID = "AnamuleID";

    String API_SCORE_GIVE_ANAMULE = BASE_URL + "api/score/GiveAnamule";

    String API_ANAMULES_CURRENT_ANAMULES = BASE_URL + "api/Anamules/CurrentAnamules/";
    String API_SCORE_SCORE_CARD = BASE_URL + "api/score/ScoreCard";
    String API_PAGE_NO = "PageNo";

    String API_GOLF_GAME_UPDATE_COURSE_MANUALLY = BASE_URL + "api/GolfGame/UpdateCourseManually";
    String KEY_HOLE_COUNT = "HoleCount";

    String API_GOLF_GAME_REMOVE_PLAYER = BASE_URL + "api/GolfGame/RemovePlayer";
}
