package com.app.anamules.webservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.anamules.R;
import com.app.anamules.activity.SigninActivity;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.dialog.GeneralDialog;
import com.app.anamules.dialog.Loader;
import com.app.anamules.listeners.DialogClikListener;

import java.util.HashMap;
import java.util.Map;

public class ApiCall
{
    public Loader loader;
    public GeneralDialog dialogCommon;
    Network network;
    Context context;

    public ApiCall(Context context)
    {
        this.context = context;
        loader = new Loader(context);
        dialogCommon = new GeneralDialog(context);
        network = new Network();
    }

    public void callGet(boolean isLoder, String url,String headerValue, GetResponce getResponce)
    {

        if(network.isNetwork(context))
        {
            if(isLoder)
            {
                loader.show();
            }

            RequestQueue mRequestQueue = Volley.newRequestQueue(context);
            Log.e("TAG","Api = "+url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loader.dismiss();

                    getResponce.GetResponce(response);
                    Log.e("TAG","response = "+response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loader.dismiss();
                    //loader.hide();
                    Log.e("TAG", "error = " + error.getMessage());
                    Log.e("TAG", "error = " + error.networkResponse.statusCode);
                    if(error.networkResponse.statusCode == 403)
                    {
                        dialogCommon.showDialog("", "Session Expire", "", "OK", new DialogClikListener() {
                            @Override
                            public void clikOnPositive() {
                                Session.clearUserSession(context);
                                Intent a = new Intent(context, SigninActivity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(a);
                                ((Activity)context).finish();
                            }

                            @Override
                            public void clikOnNegative() {

                            }
                        });
                    }
                    getResponce.error(error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> param = new HashMap<>();
                    param.put(WebserviceApi.HEADER_KEY,headerValue);
                    Log.e("TAG","HEADER = "+param);
                    return param;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> param = new HashMap<>();

                    return param;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;
                    Log.e("TAG","mStatusCode = "+mStatusCode);
                    if(mStatusCode == 403)
                    {
                        dialogCommon.showDialog("", "Session Expire", "", "OK", new DialogClikListener() {
                            @Override
                            public void clikOnPositive() {
                                Session.clearUserSession(context);
                                Intent a = new Intent(context, SigninActivity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(a);
                                ((Activity)context).finish();
                            }

                            @Override
                            public void clikOnNegative() {

                            }
                        });
                    }
                    return super.parseNetworkResponse(response);
                }
            };

            stringRequest.setShouldCache(false);
            mRequestQueue.getCache().clear();
            mRequestQueue.add(stringRequest);
        }
        else
        {
            dialogCommon.setTitle(context.getString(R.string.no_internet));
            dialogCommon.show();
        }
    }

    public void callPost(boolean isLoder, String url, HashMap<String, String> param, String headerValue,GetResponce getResponce)
    {
        Log.e("TAG","Param = "+param);

        if(network.isNetwork(context))
        {
            if(isLoder)
            {
                loader.show();
            }

            RequestQueue mRequestQueue = Volley.newRequestQueue(context);
            Log.e("TAG","Api = "+url);
            Log.e("TAG","PARAM = "+param);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loader.dismiss();

                    Log.e("TAG","response = "+response);
                    getResponce.GetResponce(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    //if(isLoder)
                    loader.dismiss();
                    Log.e("TAG", "error = " + error.getMessage());
                    Log.e("TAG", "error = " + error.networkResponse.statusCode);
                    if(error.networkResponse.statusCode == 403)
                    {
                        dialogCommon.showDialog("", "Session Expire", "", "OK", new DialogClikListener() {
                            @Override
                            public void clikOnPositive() {
                                Session.clearUserSession(context);
                                Intent a = new Intent(context, SigninActivity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(a);
                                ((Activity)context).finish();
                            }

                            @Override
                            public void clikOnNegative() {

                            }
                        });
                    }
                    getResponce.error(error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> param = new HashMap<>();
                    param.put(WebserviceApi.HEADER_KEY,headerValue);
                    Log.e("TAG","HEADER = "+param);
                    return param;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return param;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;
                    Log.e("TAG","mStatusCode = "+mStatusCode);
                    if(mStatusCode == 403)
                    {
                        dialogCommon.showDialog("", "Session Expire", "", "OK", new DialogClikListener() {
                            @Override
                            public void clikOnPositive() {
                                Session.clearUserSession(context);
                                Intent a = new Intent(context, SigninActivity.class);
                                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(a);
                                ((Activity)context).finish();
                            }

                            @Override
                            public void clikOnNegative() {

                            }
                        });
                    }
                    return super.parseNetworkResponse(response);
                }
            };

            stringRequest.setShouldCache(false);
            mRequestQueue.getCache().clear();
            mRequestQueue.add(stringRequest);
        }
        else
        {
            dialogCommon.setTitle(context.getString(R.string.no_internet));
            dialogCommon.show();
        }
    }


    public interface GetResponce
    {
        void GetResponce(String Response);
        void error(String error);
    }
}
