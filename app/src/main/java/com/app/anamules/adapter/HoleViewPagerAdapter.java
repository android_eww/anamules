package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.been.HoleImageBeen;
import com.app.anamules.been.HoleTeeBoxFilterBeen;
import com.app.anamules.view.CustomTextView;

import java.util.List;

public class HoleViewPagerAdapter extends PagerAdapter
{
    Context context;
    LayoutInflater layoutInflater;
    List<HoleTeeBoxFilterBeen> list;
    private String name;

    public HoleViewPagerAdapter(Context context, List<HoleTeeBoxFilterBeen> holeList, String name)
    {
        this.context = context;
        list =  holeList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.name = name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == ((CardView)o);
    }

    public class ViewHolder {
        CustomTextView txt_hole_number, txt_cource_name, txt_par_hcp;
        LinearLayout ll_black, ll_white, ll_yellow, ll_red;
        CustomTextView txt_black, txt_white, txt_yellow, txt_red;
        RecyclerView rec_teeBox;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.item_viewpager, container, false);
        container.addView(view);
        ViewHolder viewHolder = new ViewHolder();

        viewHolder.txt_hole_number = view.findViewById(R.id.txt_hole_number);
        viewHolder.txt_cource_name = view.findViewById(R.id.txt_cource_name);
        viewHolder.txt_par_hcp = view.findViewById(R.id.txt_par_hcp);
        viewHolder.ll_black = view.findViewById(R.id.ll_black);
        viewHolder.ll_white = view.findViewById(R.id.ll_white);
        viewHolder.ll_yellow = view.findViewById(R.id.ll_yellow);
        viewHolder.ll_red = view.findViewById(R.id.ll_red);
        viewHolder.txt_black = view.findViewById(R.id.txt_black);
        viewHolder.txt_white = view.findViewById(R.id.txt_white);
        viewHolder.txt_yellow = view.findViewById(R.id.txt_yellow);
        viewHolder.txt_red = view.findViewById(R.id.txt_red);
        viewHolder.rec_teeBox = view.findViewById(R.id.rec_teeBox);


        viewHolder.txt_hole_number.setText(list.get(position).getHolenumber());
        viewHolder.txt_cource_name.setText(name);
        viewHolder.txt_par_hcp.setText("Par "+list.get(position).getPar()+ " Hcp "+(list.get(position).getHandicap() != null ? list.get(position).getHandicap():0));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);

        viewHolder.rec_teeBox.setLayoutManager(layoutManager);

        TeeBoxAdapter adapter = new TeeBoxAdapter(context, list.get(position).getTeeBoxList());
        viewHolder.rec_teeBox.setAdapter(adapter);

       /* for(int i = 0 ; i < list.get(position).getTeeBoxList().size(); i++) {
            if(list.get(position).getTeeBoxList().get(i).getColor().equalsIgnoreCase("Black")) {
                viewHolder.txt_black.setText(list.get(position).getTeeBoxList().get(i).getLength());
                viewHolder.ll_black.setVisibility(View.VISIBLE);
            } else if(list.get(position).getTeeBoxList().get(i).getColor().equalsIgnoreCase("White")) {
                viewHolder.txt_white.setText(list.get(position).getTeeBoxList().get(i).getLength());
                viewHolder.ll_white.setVisibility(View.VISIBLE);
            } else if(list.get(position).getTeeBoxList().get(i).getColor().equalsIgnoreCase("Yellow")) {
                viewHolder.txt_yellow.setText(list.get(position).getTeeBoxList().get(i).getLength());
                viewHolder.ll_yellow.setVisibility(View.VISIBLE);
            } else if(list.get(position).getTeeBoxList().get(i).getColor().equalsIgnoreCase("Red")) {
                viewHolder.txt_red.setText(list.get(position).getTeeBoxList().get(i).getLength());
                viewHolder.ll_red.setVisibility(View.VISIBLE);
            }
        }*/



        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((CardView)object);
    }
}
