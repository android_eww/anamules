package com.app.anamules.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.anamules.R;
import com.app.anamules.activity.AddPlayerCourseActivity;
import com.app.anamules.activity.HomeActivity;
import com.app.anamules.activity.UserZooActivity;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.EditListener;
import com.app.anamules.view.CustomTextView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HoleAdapter extends RecyclerView.Adapter
{
    Context context;
    EditListener editListener;
    private List<PlayerHoleDetail> playerHoleDetailList;
    String holeNumber;

    public HoleAdapter(Context context, EditListener editListener, List<PlayerHoleDetail> playerHoleDetailList, String holeNumber)
    {
        this.context = context;
        this.editListener = editListener;
        this.playerHoleDetailList = playerHoleDetailList;
        this.holeNumber = holeNumber;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hole_player, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.txt_name.setText(playerHoleDetailList.get(i).getFirstName()+ " "+playerHoleDetailList.get(i).getLastName());
            holder.txt_total_score.setText(playerHoleDetailList.get(i).getTotal_score());
            holder.txt_currentHole.setText(playerHoleDetailList.get(i).getCurrent_hole_score().getScore());
            holder.tv_userAnim.setText(playerHoleDetailList.get(i).getTotal_anamules()+ " "+ ((Integer.parseInt(playerHoleDetailList.get(i).getTotal_anamules()) == 0 || Integer.parseInt(playerHoleDetailList.get(i).getTotal_anamules()) == 1)? "Anamule" : "Anamules"));

            if(playerHoleDetailList.get(i).getProfilePicture() != null && !playerHoleDetailList.get(i).getProfilePicture().contains("No_Image.png")) {
                holder.txt_name_img.setVisibility(View.GONE);
                Picasso.with(context).load(playerHoleDetailList.get(i).getProfilePicture()).into(holder.civ_player);
            } else {
                holder.txt_name_img.setVisibility(View.VISIBLE);
                String ch = playerHoleDetailList.get(i).getFirstName().charAt(0) +""+ playerHoleDetailList.get(i).getLastName().charAt(0);
                holder.txt_name_img.setText(ch.toUpperCase());
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(holder.civ_player);
            }

            holder.iv_edit.setOnClickListener(view -> {
                editListener.onClickEdit(i);
            });

            holder.tv_userAnim.setOnClickListener(view -> {

                List<PlayerHoleDetail> removePlayer = new ArrayList<>();
                removePlayer.addAll(playerHoleDetailList);
                removePlayer.remove(i);

                Intent intent = new Intent(context, UserZooActivity.class);
                intent.putExtra(Constant.USER_ZOO, playerHoleDetailList.get(i));
                intent.putExtra(Constant.USER_LIST, (Serializable) removePlayer);
                intent.putExtra(Constant.HOLE_NUMBER, holeNumber);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }

    @Override
    public int getItemCount() {
        return playerHoleDetailList.size();
    }

    protected class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_edit;
        CustomTextView tv_userAnim, txt_name, txt_total_score, txt_currentHole, txt_name_img;
        CircleImageView civ_player;


        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            iv_edit = itemView.findViewById(R.id.iv_edit);
            tv_userAnim = itemView.findViewById(R.id.tv_userAnim);
            txt_name  = itemView.findViewById(R.id.txt_name);
            txt_total_score = itemView.findViewById(R.id.txt_total_score);
            txt_currentHole = itemView.findViewById(R.id.txt_currentHole);
            civ_player = itemView.findViewById(R.id.civ_player);
            txt_name_img = itemView.findViewById(R.id.txt_name_img);
        }
    }
}
