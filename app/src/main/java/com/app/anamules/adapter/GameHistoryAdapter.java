package com.app.anamules.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.anamules.R;
import com.app.anamules.activity.GameHistoryDetailActivity;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Global;
import com.app.anamules.view.CustomTextView;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.List;

public class GameHistoryAdapter extends RecyclerView.Adapter
{
    public Context context;
    public List<GameHistoryBeen> gameHistoryBeenList;

    public GameHistoryAdapter(Context context, List<GameHistoryBeen> gameHistoryBeenList)
    {
        this.context = context;
        this.gameHistoryBeenList = gameHistoryBeenList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_game_history_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.txt_club_name.setText(gameHistoryBeenList.get(i).getCourseName());

            holder.txt_date.setText(Global.getDateFormated(gameHistoryBeenList.get(i).getFinishedDate()));

            holder.cvGameHistory.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(context, GameHistoryDetailActivity.class);
                    intent.putExtra(Constant.GAME_DETAIL, gameHistoryBeenList.get(i));
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return gameHistoryBeenList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        public CardView cvGameHistory;
        public CustomTextView txt_club_name, txt_date;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            cvGameHistory = itemView.findViewById(R.id.cvGameHistory);
            txt_club_name = itemView.findViewById(R.id.txt_club_name);
            txt_date = itemView.findViewById(R.id.txt_date);
        }
    }
}
