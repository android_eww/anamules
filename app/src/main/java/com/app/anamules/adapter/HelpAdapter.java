package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.anamules.R;
import com.app.anamules.been.animals.Datum;
import com.app.anamules.common.Constant;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.WebserviceApi;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HelpAdapter extends RecyclerView.Adapter
{
    List<Datum> list;
    Context context;

    public HelpAdapter(Context context, List<Datum> list)
    {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_help, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder)viewHolder;
            if(list.get(i).getHowEarned() != null && !list.get(i).getHowEarned().equalsIgnoreCase(""))
            holder.tv_animDes.setText(list.get(i).getHowEarned());
            if(list.get(i).getName() != null && !list.get(i).getName().equalsIgnoreCase(""))
            holder.tv_animName.setText(list.get(i).getName()+" : ");
            if(list.get(i).getImage() != null && !list.get(i).getImage().equalsIgnoreCase(""))
            Picasso.with(context).load(list.get(i).getImage()).into(holder.iv_anim);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_anim;
        CustomTextView tv_animName,tv_animDes;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_anim = itemView.findViewById(R.id.iv_anim);
            tv_animName = itemView.findViewById(R.id.tv_animName);
            tv_animDes = itemView.findViewById(R.id.tv_animDes);
        }
    }
}
