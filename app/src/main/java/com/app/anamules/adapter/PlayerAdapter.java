package com.app.anamules.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.activity.AddManuallyActivity;
import com.app.anamules.activity.AddNearToActivity;
import com.app.anamules.activity.AddRecentPlayerActivity;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.been.player.AddplayerBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.AddPlayerFromListener;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter
{
    Context context;
    int openLayout = Integer.MAX_VALUE;
    int TotalPlayer = 4;
    AddPlayerFromListener addPlayerFrpmListener;
    List<RecentPlayerBeen> list;

    public ApiCall apiCall;
    public String key = "";
    String TAG = "PlayerAdapter";

    public PlayerAdapter(Context context,List<RecentPlayerBeen> list, String key)
    {
        this.context = context;
        this.list = list;
        this.key = key;
        apiCall = new ApiCall(context);
    }

    public void setListener(AddPlayerFromListener addPlayerFrpmListener) {
        this.addPlayerFrpmListener = addPlayerFrpmListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_player, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            if(i == openLayout)
            {
                holder.cardViewAddPlayer.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.cardViewAddPlayer.setVisibility(View.GONE);
            }

            if(list.size() > i && list.get(i).getFirstName() != null && !list.get(i).getFirstName().equalsIgnoreCase(""))
            {
                holder.tv_name.setText(list.get(i).getFirstName() +" "+list.get(i).getLastName());
            }

            holder.ll_mailLayout.setOnClickListener(view -> {
                if(holder.cardViewAddPlayer.getVisibility() == View.VISIBLE)
                {
                    openLayout = Integer.MAX_VALUE;
                    //holder.cardViewAddPlayer.setVisibility(View.GONE);
                }
                else
                {
                    openLayout = i;
                    //holder.cardViewAddPlayer.setVisibility(View.VISIBLE);

                }
                notifyDataSetChanged();
            });

            holder.tv_add_NearMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*addPlayerFrpmListener.onAddPlayer(AddNearToActivity.class, i);
                    openLayout = Integer.MAX_VALUE;
                    notifyDataSetChanged();*/
                }
            });

            holder.tv_addRecentPlayer.setOnClickListener(view -> {
                addPlayerFrpmListener.onAddPlayer(AddRecentPlayerActivity.class, i);
                openLayout = Integer.MAX_VALUE;
                notifyDataSetChanged();
            });

            holder.tv_addManually.setOnClickListener(view -> {
                addPlayerFrpmListener.onAddPlayer(AddManuallyActivity.class, i);
                openLayout = Integer.MAX_VALUE;
                notifyDataSetChanged();
            });

            holder.tv_addFromContact.setOnClickListener(view -> {
                addPlayerFrpmListener.onAddPlayerFromContact(i);
                openLayout = Integer.MAX_VALUE;
                notifyDataSetChanged();
            });

            holder.txt_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(list.get(i).getID() != null && !list.get(i).getID().equalsIgnoreCase("")) {
                        callRemovePlayer(list.get(i).getID(), i);
                    } else {
                        list.remove(i);
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_mailLayout;
        CardView cardViewAddPlayer;
        LinearLayout tv_addFromContact,tv_add_NearMe,tv_addManually,tv_addRecentPlayer;
        CustomTextView tv_name, txt_delete;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ll_mailLayout = itemView.findViewById(R.id.ll_mailLayout);
            cardViewAddPlayer = itemView.findViewById(R.id.cardViewAddPlayer);
            tv_addRecentPlayer = itemView.findViewById(R.id.tv_addRecentPlayer);
            tv_addManually = itemView.findViewById(R.id.tv_addManually);
            tv_add_NearMe = itemView.findViewById(R.id.tv_add_NearMe);
            tv_addFromContact = itemView.findViewById(R.id.tv_addFromContact);
            tv_name = itemView.findViewById(R.id.tv_name);
            txt_delete = itemView.findViewById(R.id.txt_delete);
        }
    }

    public void addPlayer()
    {
        TotalPlayer++;
        list.add(new RecentPlayerBeen());
        notifyDataSetChanged();
    }

    public void callRemovePlayer(String ID, int postion){
        String url = WebserviceApi.API_GOLF_GAME_REMOVE_PLAYER;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_ID, ID);

        apiCall.callPost(true, url, param, key, new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            list.remove(postion);
                            notifyDataSetChanged();

                            String message = "Player removed Successfully";

                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            showDialog(message);

                        } else {
                            String message = "Please Try again...";

                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            showDialog(message);
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Error : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void showDialog(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);

        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setMessage(message);

        dialog.setPositiveButton(context.getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
