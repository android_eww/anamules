package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.anamules.R;
import com.app.anamules.been.AnamulesDetailsBeen;
import com.app.anamules.been.HoleUserAnamulesBeen;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AnumulesUserAdapter extends RecyclerView.Adapter<AnumulesUserAdapter.ViewHolder>{

    private Context context;
    private List<HoleUserAnamulesBeen> list;

    public AnumulesUserAdapter(Context context, List<HoleUserAnamulesBeen> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_anumules_user_item,  viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if(list != null) {
            if(list.get(i).getImage() != null) {
                if (!list.get(i).getImage().equalsIgnoreCase("")) {
                    Picasso.with(context).load(list.get(i).getImage()).into(viewHolder.imageView);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img_animal);
        }
    }
}
