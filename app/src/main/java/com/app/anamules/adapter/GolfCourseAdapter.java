package com.app.anamules.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.activity.GolfCourseListActivity;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.listeners.GolfCourseSelectionNear;
import com.app.anamules.view.CustomTextView;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.List;

public class GolfCourseAdapter extends RecyclerView.Adapter
{
    Context context;
    boolean isClickable = false;
    int selectedPos = Integer.MAX_VALUE;
    String FROM = Constant.FROM_GOLF_NEAR_ME;
    GolfCourseSelectionNear listener;
    List<Datum> list;

    public GolfCourseAdapter(Context context, List<Datum> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_golf, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;
            Datum item = list.get(i);

            holder.scaleRatingBar.setEnabled(false);

            if(selectedPos == i)
            {
                holder.view_selected.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.view_selected.setVisibility(View.GONE);
            }

            if(item.getName() != null && !item.getName().equalsIgnoreCase(""))
            {holder.tv_golfName.setText(item.getName());}
            //Picasso.with(context).load(item.)holder.iv_golfImg

            holder.iv_golfImg.setImageResource(R.drawable.img_golf);
            if(list.get(i).getDistance() != null) {
                holder.txt_distance.setText(list.get(i).getDistance() + " mi");
            }

            holder.img_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, ""+list.get(i).getDistance(), Toast.LENGTH_SHORT).show();
                    showInfoDialgo(list.get(i).getName()+", "+ list.get(i).getAddress().getStreet()+", "+list.get(i).getAddress().getCity()+ ", "+ list.get(i).getAddress().getState() +
                             ", "+ list.get(i).getAddress().getCountry()+"-"+list.get(i).getAddress().getZip());
                }
            });

            holder.tv_city.setText(list.get(i).getAddress().getCity());

        }
    }

    private void showInfoDialgo(String address) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(address)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ScaleRatingBar scaleRatingBar;
        View view_selected;
        ImageView iv_golfImg, img_info;
        CustomTextView tv_golfName, txt_distance, tv_city;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            scaleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
            view_selected = itemView.findViewById(R.id.view_selected);
            iv_golfImg = itemView.findViewById(R.id.iv_golfImg);
            tv_golfName = itemView.findViewById(R.id.tv_golfName);
            txt_distance = itemView.findViewById(R.id.txt_distance);
            img_info = itemView.findViewById(R.id.img_info);
            tv_city = itemView.findViewById(R.id.tv_city);

            itemView.setOnClickListener(view -> {
                if(isClickable)
                {
                    if(selectedPos != getAdapterPosition())
                    {
                        selectedPos = getAdapterPosition();
                        view_selected.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        selectedPos = Integer.MAX_VALUE;
                        view_selected.setVisibility(View.GONE);
                    }
                    notifyDataSetChanged();

                    if(listener != null)
                    {
                        if(selectedPos < list.size()) {
                            listener.onSelectedRV(FROM, selectedPos, list.get(selectedPos));
                        }
                    }

                    //((GolfCourseListActivity)context).selectItemAdapterRefresh(Constant.FROM_GOLF_NEAR_ME);
                }
            });
        }

    }

    public void setClickable(boolean isClickable, String from)
    {
        this.isClickable = isClickable;
        //FROM = from;
    }

    public void setSelectedPos(int position)
    {
        selectedPos = position;
        notifyDataSetChanged();
    }

    public void setListener(GolfCourseSelectionNear listener)
    {
        this.listener = listener;
    }
}
