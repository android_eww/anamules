package com.app.anamules.adapter;

import android.arch.persistence.room.util.StringUtil;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.anamules.R;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.WebserviceApi;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecentPlayerAdapter extends RecyclerView.Adapter<RecentPlayerAdapter.MyViewHolder>
{
    public Context context;
    public List<RecentPlayerBeen> recentPlayerBeenList;
    public int selectPos = -1;
    public boolean isSelect = false;

    public RecentPlayerAdapter(Context context, List<RecentPlayerBeen> recentPlayerBeenList, boolean isSelect)
    {
        this.context = context;
        this.recentPlayerBeenList = recentPlayerBeenList;
        this.isSelect = isSelect;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recent_player_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i)
    {
       /* if(viewHolder instanceof MyViewHolder)
        {*/
            //MyViewHolder holder = (MyViewHolder) viewHolder;

        viewHolder.txt_player.setText(StringUtils.capitalize(recentPlayerBeenList.get(i).getFirstName() + " " + recentPlayerBeenList.get(i).getLastName()));
            if(!recentPlayerBeenList.get(i).getProfilePicture().equalsIgnoreCase("")) {
                Picasso.with(context).load(WebserviceApi.BASE_URL+recentPlayerBeenList.get(i).getProfilePicture()).into(viewHolder.cimg_player);
                viewHolder.txt_name.setVisibility(View.GONE);
            }  else {
                //viewHolder.cimg_player.setCircleBackgroundColor(R.drawable.bg_gray);
                //viewHolder.cimg_player.setCir(context.getResources().getColor(R.color.colorGreen));
                //viewHolder.cimg_player.setBorderWidth(3);
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(viewHolder.cimg_player);
                viewHolder.txt_name.setVisibility(View.VISIBLE);
                String ch = recentPlayerBeenList.get(i).getFirstName().charAt(0) +""+ recentPlayerBeenList.get(i).getLastName().charAt(0);
                viewHolder.txt_name.setText(ch.toUpperCase());
            }

            if(recentPlayerBeenList.get(i).isSelect()) {
                viewHolder.view_selected.setVisibility(View.VISIBLE);
            } else {
                viewHolder.view_selected.setVisibility(View.GONE);
            }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*for(int j = 0 ; j < recentPlayerBeenList.size(); j++) {
                        recentPlayerBeenList.get(j).setSelect(false);
                    }*/

                    if(isSelect) {

                        recentPlayerBeenList.get(i).setSelect(true);
                        selectPos = i;
                        notifyDataSetChanged();
                    }
                }
            });

        //}
    }

    @Override
    public int getItemCount() {
        return recentPlayerBeenList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);

    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView cimg_player;
        CustomTextView txt_player, txt_name;
        View view_selected;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cimg_player = itemView.findViewById(R.id.cimg_player);
            txt_player = itemView.findViewById(R.id.txt_player);
            txt_name = itemView.findViewById(R.id.txt_name);
            view_selected = itemView.findViewById(R.id.view_selected);
        }
    }

}
