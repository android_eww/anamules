package com.app.anamules.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.anamules.R;
import com.app.anamules.been.GameHistoryDetailBeen;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.databinding.RowGameHistoryDetailItemBinding;
import com.app.anamules.view.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ResultDeatilAdapter extends RecyclerView.Adapter
{
    public Context context;
    public List<PlayerHoleDetail> playerHoleDetaiList;
    RowGameHistoryDetailItemBinding binding;
    public int zookeperPostion = -1;

    public ResultDeatilAdapter(Context context, ArrayList<PlayerHoleDetail> playerHoleDetaiList)
    {
        this.context = context;
        this.playerHoleDetaiList = playerHoleDetaiList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.row_game_history_detail_item,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;


            if(!playerHoleDetaiList.get(i).getProfilePicture().equalsIgnoreCase("") && !playerHoleDetaiList.get(i).getProfilePicture().contains("No_Image.png")) {
                binding.txtNameImg.setVisibility(View.GONE);
                Picasso.with(context).load(playerHoleDetaiList.get(i).getProfilePicture()).into(binding.ivUser);
            } else {
                binding.txtNameImg.setVisibility(View.VISIBLE);
                String ch = playerHoleDetaiList.get(i).getFirstName().charAt(0) +""+ playerHoleDetaiList.get(i).getLastName().charAt(0);
                binding.txtNameImg.setText(ch.toUpperCase());
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(binding.ivUser);
            }

            binding.tvUserName.setText(playerHoleDetaiList.get(i).getFirstName() + " "+playerHoleDetaiList.get(i).getLastName());
            binding.tvAnimals.setText(playerHoleDetaiList.get(i).getTotal_anamules()+ " Anamules");
            binding.tvScore.setText("+"+playerHoleDetaiList.get(i).getTotal_score());

            /*if(zookeperPostion == -1) {

                if (Integer.parseInt(playerHoleDetaiList.get(0).getTotal_anamules()) == Integer.parseInt(playerHoleDetaiList.get(i).getTotal_anamules())) {
                    binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorWhite));
                    binding.setPosition(i);
                    binding.setTem(i);
                } else {
                    binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorGreen));
                    binding.setPosition(i);
                    binding.setTem(-2);
                }
            } else {*/

                if(playerHoleDetaiList.get(i).getSelect()) {

                    binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorWhite));
                    binding.setPosition(i);
                    binding.setTem(i);

                } else  {
                    binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorGreen));
                    binding.setPosition(i);
                    binding.setTem(Integer.MAX_VALUE);
                }

            //}

        }
    }

    @Override
    public int getItemCount() {
        return playerHoleDetaiList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView iv_user;
        CustomTextView tv_userName, tv_animals, tv_score, txt_name_img;

        public MyViewHolder(RowGameHistoryDetailItemBinding binding) {
            super(binding.getRoot());

        }
    }
}
