package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.listeners.UserScoreListener;
import com.app.anamules.view.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserInGameAdapter extends RecyclerView.Adapter {

    Context context;
    int selectedPos = 0;//Integer.MAX_VALUE;
    List<PlayerHoleDetail> playerHoleDetailList;
    UserScoreListener listener;

    public UserInGameAdapter(Context context, List<PlayerHoleDetail> playerHoleDetailList, UserScoreListener listener)
    {
        this.context = context;
        this.playerHoleDetailList = playerHoleDetailList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_game_player, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            if(!playerHoleDetailList.get(i).getProfilePicture().equalsIgnoreCase("") && !playerHoleDetailList.get(i).getProfilePicture().contains("No_Image.png")) {
                holder.txt_name.setVisibility(View.GONE);
                Picasso.with(context).load(playerHoleDetailList.get(i).getProfilePicture()).into(holder.iv_user);
            } else {
                String ch = playerHoleDetailList.get(i).getFirstName().charAt(0) +""+ playerHoleDetailList.get(i).getLastName().charAt(0);
                holder.txt_name.setText(ch.toUpperCase());
                holder.txt_name.setVisibility(View.VISIBLE);
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(holder.iv_user);
            }

            holder.tv_user.setText(playerHoleDetailList.get(i).getFirstName()+ " "+playerHoleDetailList.get(i).getLastName());


            if(playerHoleDetailList.get(i).getSelect()) {
                holder.ll_main.setBackgroundResource(R.drawable.bg_player_selected);
                holder.tv_user.setTextColor(context.getResources().getColor(R.color.colorWhite));
            } else {
                holder.ll_main.setBackgroundResource(R.drawable.bg_player);
                holder.tv_user.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int j = 0 ; j < playerHoleDetailList.size(); j++) {
                        playerHoleDetailList.get(j).setSelect(false);
                    }
                    playerHoleDetailList.get(i).setSelect(true);
                    listener.onClickUser(playerHoleDetailList.get(i).getID());
                    holder.ll_main.setBackgroundResource(R.drawable.bg_player_selected);
                    holder.tv_user.setTextColor(context.getResources().getColor(R.color.colorWhite));

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return playerHoleDetailList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_main;
        CustomTextView tv_user, txt_name;
        CircleImageView iv_user;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ll_main = itemView.findViewById(R.id.ll_main);
            tv_user = itemView.findViewById(R.id.tv_user);
            iv_user = itemView.findViewById(R.id.iv_user);
            txt_name = itemView.findViewById(R.id.txt_name);

            itemView.setOnClickListener(view -> {
                selectedPos = getAdapterPosition();
                notifyDataSetChanged();
            });
        }


    }
}
