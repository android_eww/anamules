package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.app.anamules.R;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.listeners.UserZooSelectListener;
import com.app.anamules.view.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter {

    Context context;

    List<PlayerHoleDetail> playerList;
    UserZooSelectListener listener;


    public UserAdapter(Context context, List<PlayerHoleDetail> playerList, UserZooSelectListener listener)
    {
        this.context = context;
        this.playerList = playerList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            if(playerList.get(i).getProfilePicture() != null && !playerList.get(i).getProfilePicture().contains("No_Image.png")) {
                holder.txt_name_img.setVisibility(View.GONE);
                Picasso.with(context).load(playerList.get(i).getProfilePicture()).into(holder.civ_user);
            } else {
                String ch = playerList.get(i).getFirstName().charAt(0) +""+ playerList.get(i).getLastName().charAt(0);
                holder.txt_name_img.setText(ch.toUpperCase());
                holder.txt_name_img.setVisibility(View.VISIBLE);
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(holder.civ_user);
            }
            holder.txt_name.setText(playerList.get(i).getFirstName()+ " "+playerList.get(i).getLastName());
            holder.txt_anamules.setText(playerList.get(i).getTotal_anamules()+ " "+ "Anamules");

            if(playerList.get(i).getSelect()) {
                holder.radioButton.setChecked(true);
                listener.selectPlayer(i);
            } else {
                holder.radioButton.setChecked(false);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    clearSelection();

                    playerList.get(i).setSelect(true);
                    notifyDataSetChanged();
                    listener.selectPlayer(i);


                }
            });

            holder.radioButton.setClickable(false);

        }
    }

    public void clearSelection() {
        for(int j = 0 ; j < playerList.size(); j++) {
            playerList.get(j).setSelect(false);
        }
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView civ_user;
        CustomTextView txt_name, txt_anamules, txt_name_img;
        RadioButton radioButton;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            civ_user = itemView.findViewById(R.id.civ_user);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_anamules = itemView.findViewById(R.id.txt_anamules);
            radioButton = itemView.findViewById(R.id.radioButton);
            txt_name_img = itemView.findViewById(R.id.txt_name_img);
        }
    }
}
