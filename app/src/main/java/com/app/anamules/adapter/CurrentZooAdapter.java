package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.anamules.R;
import com.app.anamules.activity.CurrentZooActivity;
import com.app.anamules.been.AnimalBeen;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.dialog.DialogAnim;
import com.app.anamules.listeners.TackAnamuleListener;
import com.app.anamules.listeners.onCancelSelectAnamules;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.WebserviceApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CurrentZooAdapter extends RecyclerView.Adapter implements onCancelSelectAnamules
{
    Context context;
    boolean isClickable = false;
    boolean userDisplay = false;
    boolean isMultiple = false;

    DialogAnim dialogAnim;
    private List<AnimalBeen> playerList;
    private String From;
    TackAnamuleListener listener;

    public CurrentZooAdapter(Context context, List<AnimalBeen> playerList)
    {
        this.context = context;
        dialogAnim = new DialogAnim(context);

        this.playerList = playerList;
    }

    public CurrentZooAdapter(Context context, List<AnimalBeen> playerList, String From, TackAnamuleListener listener)
    {
        this.context = context;
        dialogAnim = new DialogAnim(context, listener);

        this.playerList = playerList;
        this.From = From;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_current_zoo, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder)viewHolder;

            Picasso.with(context).load(playerList.get(i).getImage()).into(holder.img_animal);

            holder.txt_name.setText(playerList.get(i).getName());

            /*if(!userDisplay)
            {*/
                //holder.iv_user.setVisibility(View.GONE);
            //}
            if(playerList.get(i).getUsedBy() != null) {
                if (!playerList.get(i).getUsedBy().equalsIgnoreCase("") && !playerList.get(i).getUsedBy().contains("No_Image.png")) {
                    Picasso.with(context).load(playerList.get(i).getUsedBy()).into(holder.iv_user);
                    holder.rl_img.setVisibility(View.VISIBLE);
                    holder.txt_name_img.setVisibility(View.GONE);
                } else if(playerList.get(i).getUsedBy().contains("No_Image.png")) {
                    Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(holder.iv_user);
                    //holder.iv_user.setVisibility(View.GONE);
                    String ch = playerList.get(i).getPlayerFirstName().charAt(0) +""+ playerList.get(i).getPlayerLastName().charAt(0);
                    holder.txt_name_img.setVisibility(View.VISIBLE);
                    holder.txt_name_img.setText(ch.toUpperCase());
                    holder.rl_img.setVisibility(View.VISIBLE);
                } else {
                    holder.rl_img.setVisibility(View.GONE);
                }
            }

            if(playerList.get(i).isSelect()) {
                holder.viewCorrect.setVisibility(View.VISIBLE);
            } else {
                holder.viewCorrect.setVisibility(View.GONE);
            }


            holder.itemView.setOnClickListener(view -> {
                if(From != null) {
                    if(From.equalsIgnoreCase(Constant.FROM_THE_ZOO)) {
                       /* for(int j = 0 ; j < playerList.size();j++) {
                            playerList.get(j).setSelect(false);
                        }*/

                        if (isClickable) {

                            //holder.viewCorrect.setVisibility(View.VISIBLE);
                            if(playerList.get(i).isSelect()) {
                                playerList.get(i).setSelect(false);
                            } else {
                                playerList.get(i).setSelect(true);
                                if (isClickable && userDisplay) {
                                    dialogAnim.setValue(playerList.get(i).getImage(), playerList.get(i).getName(), playerList.get(i).getHowEarned(), getSelectAnamuleId(), i,this);
                                    dialogAnim.show();
                                }
                            }
                            notifyDataSetChanged();

                        }


                    }

                    if(From.equalsIgnoreCase(Constant.FROM_USER_ZOO)) {
                        /*for(int j = 0 ; j < playerList.size();j++) {
                            playerList.get(j).setSelect(false);
                        }*/
                        if (isClickable) {

                            //holder.viewCorrect.setVisibility(View.VISIBLE);

                            if(playerList.get(i).isSelect()) {
                                playerList.get(i).setSelect(false);
                            } else {
                                playerList.get(i).setSelect(true);
                            }
                            listener.onClickTackAnamule(getSelectAnamuleId());
                            notifyDataSetChanged();

                        }
                    }
                }

            });

        }
    }

    public String getSelectAnamuleId() {

        String ids = "";

        for(int i = 0 ; i < playerList.size(); i++) {

            if(playerList.get(i).isSelect()) {
                if (ids.equalsIgnoreCase("")) {
                        ids = playerList.get(i).getID();
                } else {
                    ids = ids + "," + playerList.get(i).getID();
                }
            }
        }

        Log.e("TAG", "SelectAnamuleID : "+ids);

        return ids;
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    @Override
    public void onClickClose(int postion) {
        playerList.get(postion).setSelect(false);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView iv_user;
        View viewCorrect;
        ImageView img_animal;
        CustomTextView txt_name, txt_name_img;
        RelativeLayout rl_img;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            iv_user = itemView.findViewById(R.id.iv_user);
            viewCorrect = itemView.findViewById(R.id.view_selected);
            img_animal = itemView.findViewById(R.id.img_animal);
            txt_name = itemView.findViewById(R.id.txt_name);
            rl_img = itemView.findViewById(R.id.rl_img);
            txt_name_img = itemView.findViewById(R.id.txt_name_img);

        }

    }

    public void setItemClicable(boolean isClickable,boolean userDisplay)
    {
        this.isClickable = isClickable;
        this.userDisplay = userDisplay;
    }

    public void setSelection(boolean isMultiple)
    {
        this.isMultiple = isMultiple;
    }
}
