package com.app.anamules.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;

import com.app.anamules.R;
import com.app.anamules.activity.ViewAllGolfCourseActivity;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.listeners.GolfCourseSelectionNear;
import com.app.anamules.view.CustomTextView;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.List;

public class ViewAllGolfAdapter extends RecyclerView.Adapter implements Filterable
{
    Context context;
    int selectedPos = Integer.MAX_VALUE;
    GolfCourseSelectionNear listener;
    List<Datum> list;
    List<Datum> listFilter;

    public ViewAllGolfAdapter(Context context, List<Datum> list) {
        this.context = context;
        this.list = list;
        this.listFilter = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_golf, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.tv_golfName.setText(listFilter.get(i).getName());
            if(listFilter.get(i).getDistance() != null) {
                holder.txt_distance.setText(listFilter.get(i).getDistance() + " mi");
            }

            //holder.cardView.setLayoutParams(new RelativeLayout.LayoutParams(x,x));
            GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams)holder.cardView.getLayoutParams();
            layoutParams.height = 700;
            layoutParams.width = 470;
            if(i % 2 == 0)
            {layoutParams.setMargins(0,0,0,30);}
            else
            {layoutParams.setMargins(0,0,0,30);}

            if(i == listFilter.size()-2 || i == listFilter.size()-1)
            { layoutParams.setMargins(0,0,0,200); }

            holder.scaleRatingBar.setEnabled(false);

            if(selectedPos == i)
            {
                holder.view_selected.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.view_selected.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(view -> {
                if(selectedPos != i)
                {
                    selectedPos = i;
                    holder.view_selected.setVisibility(View.VISIBLE);
                }
                else
                {
                    selectedPos = Integer.MAX_VALUE;
                    holder.view_selected.setVisibility(View.GONE);
                }

                if(Integer.MAX_VALUE != selectedPos) {
                    if (listener != null) {
                        listener.onSelectedRV("", selectedPos, listFilter.get(selectedPos));
                    }
                }

                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return listFilter.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if(charString.isEmpty()) {
                    listFilter = list;
                } else {
                    List<Datum> listFilterTemp = new ArrayList<>();
                    for(Datum row : list) {
                        if(row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            listFilterTemp.add(row);
                        }
                    }

                    listFilter = listFilterTemp;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFilter;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listFilter = (List<Datum>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ScaleRatingBar scaleRatingBar;
        View view_selected;
        CardView cardView;
        CustomTextView tv_golfName, txt_distance;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            scaleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
            view_selected = itemView.findViewById(R.id.view_selected);
            cardView = itemView.findViewById(R.id.cardView);
            tv_golfName = itemView.findViewById(R.id.tv_golfName);
            txt_distance = itemView.findViewById(R.id.txt_distance);


        }
    }

    public void setListener(GolfCourseSelectionNear listener)
    {
        this.listener = listener;
    }


}
