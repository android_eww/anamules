package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.anamules.R;
import com.app.anamules.activity.ContactUsActivity;
import com.app.anamules.been.TeeBoxBeen;
import com.app.anamules.view.CustomTextView;

import java.util.List;

public class TeeBoxAdapter extends RecyclerView.Adapter<TeeBoxAdapter.ViewHolder> {

    private Context context;
    private List<TeeBoxBeen> list;

    public TeeBoxAdapter(Context context, List<TeeBoxBeen> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_tee_box_item, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txt_black.setText(list.get(i).getLength());

        if(list.get(i).getColor().equalsIgnoreCase("white")) {

            viewHolder.view.setBackground(context.getResources().getDrawable(R.drawable.white_hole));

        } else if(list.get(i).getColor().equalsIgnoreCase("black")) {
            viewHolder.view.setBackground(context.getResources().getDrawable(R.drawable.black_hole));
        } else if(list.get(i).getColor().equalsIgnoreCase("yellow")) {
            viewHolder.view.setBackground(context.getResources().getDrawable(R.drawable.yellow_hole));
        } else if(list.get(i).getColor().equalsIgnoreCase("red")) {
            viewHolder.view.setBackground(context.getResources().getDrawable(R.drawable.red_hole));
        } else {
            viewHolder.view.setBackground(context.getResources().getDrawable(R.drawable.silver_hole));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public CustomTextView txt_black;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.view);
            txt_black = itemView.findViewById(R.id.txt_black);

        }
    }
}
