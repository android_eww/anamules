package com.app.anamules.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.been.HoleScoreBeen;
import com.app.anamules.common.Session;
import com.app.anamules.view.CustomTextView;

import java.util.List;

public class UserScoreAdapter extends RecyclerView.Adapter
{
    Context context;
    List<HoleScoreBeen> list;

    public UserScoreAdapter(Context context, List<HoleScoreBeen> list)
    {
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_score, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.txt_hole_number.setText(list.get(i).getHoleNumber());
            holder.txt_score.setText(list.get(i).getScore());
            holder.txt_par_hcp.setText("Par "+list.get(i).getPar()+" Hcp "+((list.get(i).getHcp() != null) ? list.get(i).getHcp() : "0"));
            holder.txt_cource_name.setText(Session.getGameCourseDatail(context).getName());

            AnumulesUserAdapter adapter = new AnumulesUserAdapter(context, list.get(i).getMines());
            AnumulesUserAdapter adapter1 = new AnumulesUserAdapter(context, list.get(i).getPluse());

            LinearLayoutManager layoutMin = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
            LinearLayoutManager layoutPlus  = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

            holder.recy_min.setLayoutManager(layoutMin);
            holder.recy_add.setLayoutManager(layoutPlus);


            holder.recy_min.setAdapter(adapter);
            holder.recy_add.setAdapter(adapter1);

            if(list.get(i).getMines().size() == 0) {

                holder.recy_min.setVisibility(View.GONE);
                holder.img_min.setVisibility(View.GONE);

            } else {
                holder.recy_min.setVisibility(View.VISIBLE);
                holder.img_min.setVisibility(View.VISIBLE);
            }

            if(list.get(i).getPluse().size() == 0) {

                holder.recy_add.setVisibility(View.GONE);
                holder.img_plu.setVisibility(View.GONE);

            } else {
                holder.recy_add.setVisibility(View.VISIBLE);
                holder.img_plu.setVisibility(View.VISIBLE);
            }


            /*for(int j = 0;j<2;j++)
            {
                ImageView imageView = new ImageView(context);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(40,40));
                imageView.setImageResource(R.drawable.demo_animal);
                holder.ll_anim1.addView(imageView);
            }*/
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_anim1,ll_anim2;
        CustomTextView txt_hole_number, txt_score, txt_par_hcp, txt_cource_name;
        RecyclerView recy_min, recy_add;
        ImageView img_min, img_plu;


        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ll_anim1 = itemView.findViewById(R.id.ll_anim1);
            ll_anim2 = itemView.findViewById(R.id.ll_anim2);
            txt_hole_number = itemView.findViewById(R.id.txt_hole_number);
            txt_score = itemView.findViewById(R.id.txt_score);
            txt_par_hcp = itemView.findViewById(R.id.txt_par_hcp);
            txt_cource_name = itemView.findViewById(R.id.txt_cource_name);
            recy_add = itemView.findViewById(R.id.recy_add);
            recy_min = itemView.findViewById(R.id.recy_min);
            img_min = itemView.findViewById(R.id.img_min);
            img_plu = itemView.findViewById(R.id.img_plu);
        }
    }
}
