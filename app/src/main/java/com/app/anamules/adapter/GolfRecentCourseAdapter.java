package com.app.anamules.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.app.anamules.R;
import com.app.anamules.activity.GolfCourseListActivity;
import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.view.CustomTextView;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.List;

public class GolfRecentCourseAdapter extends RecyclerView.Adapter implements Filterable {
    Context context;
    boolean isClickable = false;
    int selectedPos = Integer.MAX_VALUE;
    String FROM = Constant.FROM_RECENTLY_PLAYED;
    GolfCourseSelection listener;
    List<GetRecentCourses> list;
    List<GetRecentCourses> listFilter;
    String from = "";

    public GolfRecentCourseAdapter(Context context, List<GetRecentCourses> list)
    {
        this.context = context;
        this.list = list;
        this.listFilter = list;
    }

    public GolfRecentCourseAdapter(Context context, List<GetRecentCourses> list, String from)
    {
        this.context = context;
        this.list = list;
        this.from = from;
        this.listFilter = list;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_golf, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof GolfRecentCourseAdapter.MyViewHolder)
        {
            GolfRecentCourseAdapter.MyViewHolder holder = (GolfRecentCourseAdapter.MyViewHolder) viewHolder;
            GetRecentCourses item = listFilter.get(i);

            holder.scaleRatingBar.setEnabled(false);

            if(selectedPos == i)
            {
                holder.view_selected.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.view_selected.setVisibility(View.GONE);
            }

            if(!from.equalsIgnoreCase("")) {

                if(from.equalsIgnoreCase("grid")) {

                    GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) holder.cardView.getLayoutParams();
                    layoutParams.height = 700;
                    layoutParams.width = 470;
                    if (i % 2 == 0) {
                        layoutParams.setMargins(0, 0, 0, 30);
                    } else {
                        layoutParams.setMargins(0, 0, 0, 30);
                    }

                    if (i == listFilter.size() - 2 || i == listFilter.size() - 1) {
                        layoutParams.setMargins(0, 0, 0, 200);
                    }
                }
            }

            if(item.getName() != null && !item.getName().equalsIgnoreCase(""))
            {holder.tv_golfName.setText(item.getName());}
            //Picasso.with(context).load(item.)holder.iv_golfImg
            /*if(list.get(i).getDistance() != null) {
                holder.txt_distance.setText(list.get(i).getDistance() + " mi");
            }*/

            holder.img_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*showInfoDialgo(list.get(i).getName()+", "+ list.get(i).getAddress().getStreet()+", "+list.get(i).getAddress().getCity()+ ", "+ list.get(i).getAddress().getState() +
                            ", "+ list.get(i).getAddress().getCountry()+"-"+list.get(i).getAddress().getZip());*/
                }
            });

        }
    }

    private void showInfoDialgo(String address) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(address)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    @Override
    public int getItemCount() {
        return listFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if(charString.isEmpty()) {
                    listFilter = list;
                } else {
                    List<GetRecentCourses> listFilterTemp = new ArrayList<>();
                    for(GetRecentCourses row : list) {
                        if(row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            listFilterTemp.add(row);
                        }
                    }

                    listFilter = listFilterTemp;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFilter;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listFilter = (List<GetRecentCourses>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ScaleRatingBar scaleRatingBar;
        View view_selected;
        ImageView iv_golfImg;
        CustomTextView tv_golfName, txt_distance;
        CardView cardView;
        ImageView img_info;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            scaleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
            view_selected = itemView.findViewById(R.id.view_selected);
            iv_golfImg = itemView.findViewById(R.id.iv_golfImg);
            tv_golfName = itemView.findViewById(R.id.tv_golfName);
            txt_distance = itemView.findViewById(R.id.txt_distance);
            cardView = itemView.findViewById(R.id.cardView);
            img_info = itemView.findViewById(R.id.img_info);

            itemView.setOnClickListener(view -> {
                if(isClickable)
                {
                    if(selectedPos != getAdapterPosition())
                    {
                        selectedPos = getAdapterPosition();
                        view_selected.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        selectedPos = Integer.MAX_VALUE;
                        view_selected.setVisibility(View.GONE);
                    }
                    notifyDataSetChanged();

                    if(selectedPos != Integer.MAX_VALUE) {

                        if (listener != null) {
                            if (selectedPos < listFilter.size()) {
                                listener.onSelectedRecent(FROM, selectedPos, listFilter.get(selectedPos));
                            }
                        }
                    }

                    //((GolfCourseListActivity)context).selectItemAdapterRefresh(Constant.FROM_RECENTLY_PLAYED);
                }
            });
        }

    }

    public void setClickable(boolean isClickable, String from)
    {
        this.isClickable = isClickable;
        //FROM = from;
    }

    public void setSelectedPos(int position)
    {
        selectedPos = position;
        this.notifyDataSetChanged();
    }

    public void setListener(GolfCourseSelection listener)
    {
        this.listener = listener;
    }
}

