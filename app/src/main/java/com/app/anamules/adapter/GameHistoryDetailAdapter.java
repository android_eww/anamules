package com.app.anamules.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.anamules.R;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.been.GameHistoryDetailBeen;
import com.app.anamules.been.PlayerDetailBeen;
import com.app.anamules.databinding.RowGameHistoryDetailItemBinding;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.WebserviceApi;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class GameHistoryDetailAdapter extends RecyclerView.Adapter
{
    public Context context;
    public List<PlayerDetailBeen> gameHistoryDetailBeenList;
    RowGameHistoryDetailItemBinding binding;

    public GameHistoryDetailAdapter(Context context, List<PlayerDetailBeen> gameHistoryDetailBeenList)
    {
        this.context = context;
        this.gameHistoryDetailBeenList = gameHistoryDetailBeenList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),R.layout.row_game_history_detail_item,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i)
    {
        if(viewHolder instanceof MyViewHolder)
        {
            MyViewHolder holder = (MyViewHolder) viewHolder;
            binding.setPosition(i);

            binding.tvUserName.setText(gameHistoryDetailBeenList.get(i).getFirstName() + " "+ gameHistoryDetailBeenList.get(i).getLastName());

            if(!gameHistoryDetailBeenList.get(i).getProfilePicture().equalsIgnoreCase("") && !gameHistoryDetailBeenList.get(i).getProfilePicture().contains("No_Image.png")){
                binding.txtNameImg.setVisibility(View.GONE);
                Picasso.with(context).load(WebserviceApi.BASE_URL + gameHistoryDetailBeenList.get(i).getProfilePicture()).into(binding.ivUser);
            } else {
                binding.txtNameImg.setVisibility(View.VISIBLE);
                String ch = gameHistoryDetailBeenList.get(i).getFirstName().charAt(0) +""+ gameHistoryDetailBeenList.get(i).getLastName().charAt(0);
                binding.txtNameImg.setText(ch.toUpperCase());
                Picasso.with(context).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(binding.ivUser);
            }

            binding.tvAnimals.setText(gameHistoryDetailBeenList.get(i).getAnamules_details().size()+ ((gameHistoryDetailBeenList.get(i).getAnamules_details().size() == 0) || (gameHistoryDetailBeenList.get(i).getAnamules_details().size() == 1)? " Anamule" :" Anamules"));

            int valueScore = 0;

            for(int j = 0 ; j < gameHistoryDetailBeenList.get(i).getPlayer_score().size(); j++) {
                valueScore += Integer.parseInt(gameHistoryDetailBeenList.get(i).getPlayer_score().get(j).getScore());
            }

            binding.tvScore.setText(valueScore+"");

            if(i == 0)
            {
                binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorWhite));
            }
            else
            {
                binding.ivUser.setBorderColor(context.getResources().getColor(R.color.colorGreen));
            }
        }
    }

    @Override
    public int getItemCount() {
        return gameHistoryDetailBeenList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        public MyViewHolder(RowGameHistoryDetailItemBinding binding) {
            super(binding.getRoot());
        }
    }
}
