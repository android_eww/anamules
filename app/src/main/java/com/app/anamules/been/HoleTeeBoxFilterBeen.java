package com.app.anamules.been;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HoleTeeBoxFilterBeen implements Serializable {

    private String holeid;
    private String holenumber;
    private String par;
    private String handicap;
    private List<TeeBoxBeen> teeBoxList = new ArrayList<>();

    public HoleTeeBoxFilterBeen() {

    }

    public HoleTeeBoxFilterBeen(String holeid, String holenumber, String par, String handicap, List<TeeBoxBeen> teeBoxList) {
        this.holeid = holeid;
        this.holenumber = holenumber;
        this.par = par;
        this.handicap = handicap;
        this.teeBoxList = teeBoxList;
    }

    public String getHoleid() {
        return holeid;
    }

    public void setHoleid(String holeid) {
        this.holeid = holeid;
    }

    public String getHolenumber() {
        return holenumber;
    }

    public void setHolenumber(String holenumber) {
        this.holenumber = holenumber;
    }

    public String getPar() {
        return par;
    }

    public void setPar(String par) {
        this.par = par;
    }

    public String getHandicap() {
        return handicap;
    }

    public void setHandicap(String handicap) {
        this.handicap = handicap;
    }

    public List<TeeBoxBeen> getTeeBoxList() {
        return teeBoxList;
    }

    public void setTeeBoxList(List<TeeBoxBeen> teeBoxList) {
        this.teeBoxList = teeBoxList;
    }
}
