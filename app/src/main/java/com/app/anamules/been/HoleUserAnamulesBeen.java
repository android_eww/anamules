package com.app.anamules.been;

import android.media.Image;

import java.io.Serializable;

public class HoleUserAnamulesBeen implements Serializable {

    private String ID;
    private String GameID;
    private String PlayerID;
    private String HoleID;
    private String AnamuleID;
    private String GainLoseBit;
    private String Status;
    private String Name;
    private String Image;

    public HoleUserAnamulesBeen(String ID, String gameID, String playerID, String holeID, String anamuleID, String gainLoseBit, String status, String name, String image) {
        this.ID = ID;
        GameID = gameID;
        PlayerID = playerID;
        HoleID = holeID;
        AnamuleID = anamuleID;
        GainLoseBit = gainLoseBit;
        Status = status;
        Name = name;
        Image = image;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getPlayerID() {
        return PlayerID;
    }

    public void setPlayerID(String playerID) {
        PlayerID = playerID;
    }

    public String getHoleID() {
        return HoleID;
    }

    public void setHoleID(String holeID) {
        HoleID = holeID;
    }

    public String getAnamuleID() {
        return AnamuleID;
    }

    public void setAnamuleID(String anamuleID) {
        AnamuleID = anamuleID;
    }

    public String getGainLoseBit() {
        return GainLoseBit;
    }

    public void setGainLoseBit(String gainLoseBit) {
        GainLoseBit = gainLoseBit;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
