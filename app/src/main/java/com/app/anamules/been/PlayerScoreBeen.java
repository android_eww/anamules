package com.app.anamules.been;

import java.io.Serializable;

public class PlayerScoreBeen implements Serializable {

    private String ID;
    private String PlayerID;
    private String HoleID;
    private String Score;

    public PlayerScoreBeen() {

    }

    public PlayerScoreBeen(String ID, String playerID, String holeID, String score) {
        this.ID = ID;
        PlayerID = playerID;
        HoleID = holeID;
        Score = score;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPlayerID() {
        return PlayerID;
    }

    public void setPlayerID(String playerID) {
        PlayerID = playerID;
    }

    public String getHoleID() {
        return HoleID;
    }

    public void setHoleID(String holeID) {
        HoleID = holeID;
    }

    public String getScore() {
        return Score;
    }

    public void setScore(String score) {
        Score = score;
    }
}
