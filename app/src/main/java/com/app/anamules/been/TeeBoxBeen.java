package com.app.anamules.been;

import java.io.Serializable;

public class TeeBoxBeen implements Serializable {

    private String color;
    private String length;
    private String teeboxtype;

    public TeeBoxBeen() {

    }

    public TeeBoxBeen(String color, String length, String teeboxtype) {
        this.color = color;
        this.length = length;
        this.teeboxtype = teeboxtype;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getTeeboxtype() {
        return teeboxtype;
    }

    public void setTeeboxtype(String teeboxtype) {
        this.teeboxtype = teeboxtype;
    }
}
