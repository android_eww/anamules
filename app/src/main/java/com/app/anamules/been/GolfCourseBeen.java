package com.app.anamules.been;



public class GolfCourseBeen {


    private String CourseName;
    private String HoleCount;
    private String GolfClubID;
    private String ID;
    private String Name;
    private String Address;
    private String Latitude;
    private String Longitude;
    private String GolfbertID;

    public GolfCourseBeen(String courseName,
                          String holeCount,
                          String golfClubID,
                          String ID,
                          String name,
                          String address,
                          String latitude,
                          String longitude) {
        CourseName = courseName;
        HoleCount = holeCount;
        GolfClubID = golfClubID;
        this.ID = ID;
        Name = name;
        Address = address;
        Latitude = latitude;
        Longitude = longitude;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getHoleCount() {
        return HoleCount;
    }

    public void setHoleCount(String holeCount) {
        HoleCount = holeCount;
    }

    public String getGolfClubID() {
        return GolfClubID;
    }

    public void setGolfClubID(String golfClubID) {
        GolfClubID = golfClubID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getGolfbertID() {
        return GolfbertID;
    }

    public void setGolfbertID(String golfbertID) {
        GolfbertID = golfbertID;
    }
}
