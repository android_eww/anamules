package com.app.anamules.been;

import java.io.Serializable;

public class AnamulesDetailsBeen implements Serializable {

    private String ID;
    private String GameID;
    private String PlayerID;
    private String HoleID;
    private String AnamuleID;
    private String GainLoseBit;
    private String AnamuleName;
    private String AnamuleImage;

    public AnamulesDetailsBeen(String ID,
                               String gameID,
                               String playerID,
                               String holeID,
                               String anamuleID,
                               String gainLoseBit,
                               String anamuleName,
                               String anamuleImage) {
        this.ID = ID;
        GameID = gameID;
        PlayerID = playerID;
        HoleID = holeID;
        AnamuleID = anamuleID;
        GainLoseBit = gainLoseBit;
        AnamuleName = anamuleName;
        AnamuleImage = anamuleImage;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getPlayerID() {
        return PlayerID;
    }

    public void setPlayerID(String playerID) {
        PlayerID = playerID;
    }

    public String getHoleID() {
        return HoleID;
    }

    public void setHoleID(String holeID) {
        HoleID = holeID;
    }

    public String getAnamuleID() {
        return AnamuleID;
    }

    public void setAnamuleID(String anamuleID) {
        AnamuleID = anamuleID;
    }

    public String getGainLoseBit() {
        return GainLoseBit;
    }

    public void setGainLoseBit(String gainLoseBit) {
        GainLoseBit = gainLoseBit;
    }

    public String getAnamuleName() {
        return AnamuleName;
    }

    public void setAnamuleName(String anamuleName) {
        AnamuleName = anamuleName;
    }

    public String getAnamuleImage() {
        return AnamuleImage;
    }

    public void setAnamuleImage(String anamuleImage) {
        AnamuleImage = anamuleImage;
    }
}
