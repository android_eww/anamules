package com.app.anamules.been;

import java.io.Serializable;

public class AnimalBeen implements Serializable {

    /*"ID": "1",
            "Name": "Beaver",
            "HowEarned": "the swing creates a large divot",
            "Image": "http://anamules.excellentwebworld.in/assets/images/anamules/iconBeaver.png",
            "Sound": "",
            "UsedBy": ""*/

    private String ID;
    private String Name;
    private String HowEarned;
    private String Image;
    private String Sound;
    private String UsedBy;
    private String PlayerID;
    private String GameID;
    private String PlayerFirstName;
    private String PlayerLastName;
    private boolean isSelect = false;


    public AnimalBeen(String ID, String name, String howEarned, String image, String sound) {
        this.ID = ID;
        Name = name;
        HowEarned = howEarned;
        Image = image;
        Sound = sound;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHowEarned() {
        return HowEarned;
    }

    public void setHowEarned(String howEarned) {
        HowEarned = howEarned;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSound() {
        return Sound;
    }

    public void setSound(String sound) {
        Sound = sound;
    }

    public String getUsedBy() {
        return UsedBy;
    }

    public void setUsedBy(String usedBy) {
        UsedBy = usedBy;
    }

    public String getPlayerID() {
        return PlayerID;
    }

    public void setPlayerID(String playerID) {
        PlayerID = playerID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getPlayerFirstName() {
        return PlayerFirstName;
    }

    public void setPlayerFirstName(String playerFirstName) {
        PlayerFirstName = playerFirstName;
    }

    public String getPlayerLastName() {
        return PlayerLastName;
    }

    public void setPlayerLastName(String playerLastName) {
        PlayerLastName = playerLastName;
    }
}
