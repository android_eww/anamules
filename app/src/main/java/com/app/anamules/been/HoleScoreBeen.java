package com.app.anamules.been;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HoleScoreBeen implements Serializable {

    private String holeNumber;
    private String score;
    private List<HoleUserAnamulesBeen> pluse = new ArrayList<>();
    private List<HoleUserAnamulesBeen> mines = new ArrayList<>();
    private String par;
    private String hcp;

    public HoleScoreBeen() { }

    public HoleScoreBeen(String holeNumber, String score, List<HoleUserAnamulesBeen> pluse, List<HoleUserAnamulesBeen> mines) {
        this.holeNumber = holeNumber;
        this.score = score;
        this.pluse = pluse;
        this.mines = mines;
    }

    public String getHoleNumber() {
        return holeNumber;
    }

    public void setHoleNumber(String holeNumber) {
        this.holeNumber = holeNumber;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public List<HoleUserAnamulesBeen> getPluse() {
        return pluse;
    }

    public void setPluse(List<HoleUserAnamulesBeen> pluse) {
        this.pluse = pluse;
    }

    public List<HoleUserAnamulesBeen> getMines() {
        return mines;
    }

    public void setMines(List<HoleUserAnamulesBeen> mines) {
        this.mines = mines;
    }

    public String getPar() {
        return par;
    }

    public void setPar(String par) {
        this.par = par;
    }

    public String getHcp() {
        return hcp;
    }

    public void setHcp(String hcp) {
        this.hcp = hcp;
    }
}
