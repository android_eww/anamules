package com.app.anamules.been;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameHistoryBeen implements Serializable {

    private String ID;
    private String OwnerUserID;
    private String CourseID;
    private String GameStatus;
    private String CreatedDate;
    private String FinishedDate;
    private String ZookeeperUserID;
    private String CourseName;
    private List<PlayerDetailBeen> player_detail = new ArrayList<>();

    public GameHistoryBeen(String ID,
                           String ownerUserID,
                           String courseID,
                           String gameStatus,
                           String createdDate,
                           String finishedDate,
                           String zookeeperUserID,
                           String courseName,
                           List<PlayerDetailBeen> player_detail) {
        this.ID = ID;
        OwnerUserID = ownerUserID;
        CourseID = courseID;
        GameStatus = gameStatus;
        CreatedDate = createdDate;
        FinishedDate = finishedDate;
        ZookeeperUserID = zookeeperUserID;
        CourseName = courseName;
        this.player_detail = player_detail;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOwnerUserID() {
        return OwnerUserID;
    }

    public void setOwnerUserID(String ownerUserID) {
        OwnerUserID = ownerUserID;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getGameStatus() {
        return GameStatus;
    }

    public void setGameStatus(String gameStatus) {
        GameStatus = gameStatus;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getFinishedDate() {
        return FinishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        FinishedDate = finishedDate;
    }

    public String getZookeeperUserID() {
        return ZookeeperUserID;
    }

    public void setZookeeperUserID(String zookeeperUserID) {
        ZookeeperUserID = zookeeperUserID;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public List<PlayerDetailBeen> getPlayer_detail() {
        return player_detail;
    }

    public void setPlayer_detail(List<PlayerDetailBeen> player_detail) {
        this.player_detail = player_detail;
    }
}
