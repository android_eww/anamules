package com.app.anamules.been;

import java.io.Serializable;

public class GameDataBeen implements Serializable {

    private String ID;
    private String OwnerUserID;
    private String CourseID;
    private String GameStatus;
    private String CreatedDate;
    private String FinishedDate;
    private String ZookeeperUserID;

    public GameDataBeen(String ID, String ownerUserID, String courseID, String gameStatus, String createdDate, String finishedDate, String zookeeperUserID) {
        this.ID = ID;
        OwnerUserID = ownerUserID;
        CourseID = courseID;
        GameStatus = gameStatus;
        CreatedDate = createdDate;
        FinishedDate = finishedDate;
        ZookeeperUserID = zookeeperUserID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOwnerUserID() {
        return OwnerUserID;
    }

    public void setOwnerUserID(String ownerUserID) {
        OwnerUserID = ownerUserID;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getGameStatus() {
        return GameStatus;
    }

    public void setGameStatus(String gameStatus) {
        GameStatus = gameStatus;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getFinishedDate() {
        return FinishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        FinishedDate = finishedDate;
    }

    public String getZookeeperUserID() {
        return ZookeeperUserID;
    }

    public void setZookeeperUserID(String zookeeperUserID) {
        ZookeeperUserID = zookeeperUserID;
    }
}
