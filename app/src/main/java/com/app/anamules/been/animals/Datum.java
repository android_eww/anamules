package com.app.anamules.been.animals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

@SerializedName("ID")
@Expose
private String iD;
@SerializedName("Name")
@Expose
private String name;
@SerializedName("HowEarned")
@Expose
private String howEarned;
@SerializedName("Image")
@Expose
private String image;
@SerializedName("Sound")
@Expose
private String sound;

public String getID() {
return iD;
}

public void setID(String iD) {
this.iD = iD;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getHowEarned() {
return howEarned;
}

public void setHowEarned(String howEarned) {
this.howEarned = howEarned;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getSound() {
return sound;
}

public void setSound(String sound) {
this.sound = sound;
}

}
