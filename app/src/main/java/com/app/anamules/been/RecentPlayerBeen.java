package com.app.anamules.been;


import java.io.Serializable;

public class RecentPlayerBeen implements Serializable {

    private String ID;
    private String GameID;
    private String UserID;
    private String FirstName;
    private String LastName;
    private String Email;
    private String ProfilePicture;
    private String ZipCode;
    private String PlayerType;
    private String IsZookeeper;
    private String PromoID;
    private String position;
    private boolean isSelect = false;
    private String PlayerEmail;

    public RecentPlayerBeen() {
        this.ID = null;
        GameID = null;
        UserID = null;
        FirstName = "Enter";
        LastName = "Player";
        Email = null;
        ProfilePicture = null;
        ZipCode = null;
        PlayerType = null;
        IsZookeeper = null;
        PromoID = null;
    }

    public RecentPlayerBeen(String ID, String gameID, String userID, String firstName, String lastName, String email, String profilePicture, String zipCode, String playerType, String isZookeeper, String promoID, String position) {
        this.ID = ID;
        GameID = gameID;
        UserID = userID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        ProfilePicture = profilePicture;
        ZipCode = zipCode;
        PlayerType = playerType;
        IsZookeeper = isZookeeper;
        PromoID = promoID;
        this.position = position;
    }

    public RecentPlayerBeen(String ID, String gameID, String userID, String firstName, String lastName, String email, String profilePicture, String zipCode, String playerType, String isZookeeper, String promoID) {
        this.ID = ID;
        GameID = gameID;
        UserID = userID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        ProfilePicture = profilePicture;
        ZipCode = zipCode;
        PlayerType = playerType;
        IsZookeeper = isZookeeper;
        PromoID = promoID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPlayerType() {
        return PlayerType;
    }

    public void setPlayerType(String playerType) {
        PlayerType = playerType;
    }

    public String getIsZookeeper() {
        return IsZookeeper;
    }

    public void setIsZookeeper(String isZookeeper) {
        IsZookeeper = isZookeeper;
    }

    public String getPromoID() {
        return PromoID;
    }

    public void setPromoID(String promoID) {
        PromoID = promoID;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getPlayerEmail() {
        return PlayerEmail;
    }

    public void setPlayerEmail(String playerEmail) {
        PlayerEmail = playerEmail;
    }
}
