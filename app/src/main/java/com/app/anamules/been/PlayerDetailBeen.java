package com.app.anamules.been;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayerDetailBeen implements Serializable {

    private String ID;
    private String GameID;
    private String UserID;
    private String FirstName;
    private String LastName;
    private String Email;
    private String ProfilePicture;
    private String ZipCode;
    private String PlayerType;
    private String IsZookeeper;
    private String PromoID;
    private List<AnamulesDetailsBeen> anamules_details = new ArrayList<>();
    private List<PlayerScoreBeen> player_score = new ArrayList<>();

    public PlayerDetailBeen(String ID,
                            String gameID,
                            String userID,
                            String firstName,
                            String lastName,
                            String email,
                            String profilePicture,
                            String zipCode,
                            String playerType,
                            String isZookeeper,
                            String promoID,
                            List<AnamulesDetailsBeen> anamules_details,
                            List<PlayerScoreBeen> player_score) {
        this.ID = ID;
        GameID = gameID;
        UserID = userID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        ProfilePicture = profilePicture;
        ZipCode = zipCode;
        PlayerType = playerType;
        IsZookeeper = isZookeeper;
        PromoID = promoID;
        this.anamules_details = anamules_details;
        this.player_score = player_score;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPlayerType() {
        return PlayerType;
    }

    public void setPlayerType(String playerType) {
        PlayerType = playerType;
    }

    public String getIsZookeeper() {
        return IsZookeeper;
    }

    public void setIsZookeeper(String isZookeeper) {
        IsZookeeper = isZookeeper;
    }

    public String getPromoID() {
        return PromoID;
    }

    public void setPromoID(String promoID) {
        PromoID = promoID;
    }

    public List<AnamulesDetailsBeen> getAnamules_details() {
        return anamules_details;
    }

    public void setAnamules_details(List<AnamulesDetailsBeen> anamules_details) {
        this.anamules_details = anamules_details;
    }

    public List<PlayerScoreBeen> getPlayer_score() {
        return player_score;
    }

    public void setPlayer_score(List<PlayerScoreBeen> player_score) {
        this.player_score = player_score;
    }
}
