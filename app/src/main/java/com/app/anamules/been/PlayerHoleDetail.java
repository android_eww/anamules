package com.app.anamules.been;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayerHoleDetail implements Serializable {

    private String ID;
    private String GameID;
    private String UserID;
    private String FirstName;
    private String LastName;
    private String Email;
    private String ProfilePicture;
    private String ZipCode;
    private String PlayerType;
    private String IsZookeeper;
    private String PromoID;
    private PlayerScoreBeen current_hole_score = new PlayerScoreBeen();
    private String total_score;
    private String total_anamules;
    private Boolean isSelect = false;

    public PlayerHoleDetail() {

    }

    public PlayerHoleDetail(String ID,
                            String gameID,
                            String userID,
                            String firstName,
                            String lastName,
                            String email,
                            String profilePicture,
                            String zipCode,
                            String playerType,
                            String isZookeeper,
                            String promoID,
                            PlayerScoreBeen current_hole_score,
                            String total_score,
                            String total_anamules) {
        this.ID = ID;
        GameID = gameID;
        UserID = userID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        ProfilePicture = profilePicture;
        ZipCode = zipCode;
        PlayerType = playerType;
        IsZookeeper = isZookeeper;
        PromoID = promoID;
        this.current_hole_score = current_hole_score;
        this.total_score = total_score;
        this.total_anamules = total_anamules;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPlayerType() {
        return PlayerType;
    }

    public void setPlayerType(String playerType) {
        PlayerType = playerType;
    }

    public String getIsZookeeper() {
        return IsZookeeper;
    }

    public void setIsZookeeper(String isZookeeper) {
        IsZookeeper = isZookeeper;
    }

    public String getPromoID() {
        return PromoID;
    }

    public void setPromoID(String promoID) {
        PromoID = promoID;
    }

    public PlayerScoreBeen getCurrent_hole_score() {
        return current_hole_score;
    }

    public void setCurrent_hole_score(PlayerScoreBeen current_hole_score) {
        this.current_hole_score = current_hole_score;
    }

    public String getTotal_score() {
        return total_score;
    }

    public void setTotal_score(String total_score) {
        this.total_score = total_score;
    }

    public String getTotal_anamules() {
        return total_anamules;
    }

    public void setTotal_anamules(String total_anamules) {
        this.total_anamules = total_anamules;
    }

    public Boolean getSelect() {
        return isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }
}
