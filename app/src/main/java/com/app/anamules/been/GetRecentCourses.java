package com.app.anamules.been;

import java.io.Serializable;

public class GetRecentCourses implements Serializable {

    private String ID;
    private String OwnerUserID;
    private String CourseID;
    private String GameStatus;
    private String CreatedDate;
    private String FinishedDate;
    private String ZookeeperUserID;
    private String GolfbertID;
    private String GolfClubID;
    private String Name;
    private String HoleCount;
    private String AddedBy;

    public GetRecentCourses(String ID, String ownerUserID, String courseID, String gameStatus, String createdDate, String finishedDate, String zookeeperUserID, String golfbertID, String golfClubID, String name, String holeCount, String addedBy) {
        this.ID = ID;
        OwnerUserID = ownerUserID;
        CourseID = courseID;
        GameStatus = gameStatus;
        CreatedDate = createdDate;
        FinishedDate = finishedDate;
        ZookeeperUserID = zookeeperUserID;
        GolfbertID = golfbertID;
        GolfClubID = golfClubID;
        Name = name;
        HoleCount = holeCount;
        AddedBy = addedBy;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOwnerUserID() {
        return OwnerUserID;
    }

    public void setOwnerUserID(String ownerUserID) {
        OwnerUserID = ownerUserID;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getGameStatus() {
        return GameStatus;
    }

    public void setGameStatus(String gameStatus) {
        GameStatus = gameStatus;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getFinishedDate() {
        return FinishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        FinishedDate = finishedDate;
    }

    public String getZookeeperUserID() {
        return ZookeeperUserID;
    }

    public void setZookeeperUserID(String zookeeperUserID) {
        ZookeeperUserID = zookeeperUserID;
    }

    public String getGolfbertID() {
        return GolfbertID;
    }

    public void setGolfbertID(String golfbertID) {
        GolfbertID = golfbertID;
    }

    public String getGolfClubID() {
        return GolfClubID;
    }

    public void setGolfClubID(String golfClubID) {
        GolfClubID = golfClubID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHoleCount() {
        return HoleCount;
    }

    public void setHoleCount(String holeCount) {
        HoleCount = holeCount;
    }

    public String getAddedBy() {
        return AddedBy;
    }

    public void setAddedBy(String addedBy) {
        AddedBy = addedBy;
    }
}
