package com.app.anamules.been.player;

public class AddplayerBeen
{
    String id,fname,lname,mobileno,position,email;

    public AddplayerBeen() {
        this.id = null;
        this.fname = "Enter";
        this.lname = "Player";
        this.mobileno = null;
        this.position = null;
        this.email = null;
    }

    public AddplayerBeen(String id, String fname, String lname, String mobileno, String position, String email) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.mobileno = mobileno;
        this.position = position;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
