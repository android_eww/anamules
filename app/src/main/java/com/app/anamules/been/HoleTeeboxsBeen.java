package com.app.anamules.been;

import java.io.Serializable;

public class HoleTeeboxsBeen implements Serializable {

    private String holeid;
    private String holenumber;
    private String color;
    private String length;
    private String par;
    private String handicap;
    private String teeboxtype;

    public HoleTeeboxsBeen(String holeid, String holenumber, String color, String length, String par, String handicap, String teeboxtype) {
        this.holeid = holeid;
        this.holenumber = holenumber;
        this.color = color;
        this.length = length;
        this.par = par;
        this.handicap = handicap;
        this.teeboxtype = teeboxtype;
    }

    public String getHoleid() {
        return holeid;
    }

    public void setHoleid(String holeid) {
        this.holeid = holeid;
    }

    public String getHolenumber() {
        return holenumber;
    }

    public void setHolenumber(String holenumber) {
        this.holenumber = holenumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getPar() {
        return par;
    }

    public void setPar(String par) {
        this.par = par;
    }

    public String getHandicap() {
        return handicap;
    }

    public void setHandicap(String handicap) {
        this.handicap = handicap;
    }

    public String getTeeboxtype() {
        return teeboxtype;
    }

    public void setTeeboxtype(String teeboxtype) {
        this.teeboxtype = teeboxtype;
    }
}
