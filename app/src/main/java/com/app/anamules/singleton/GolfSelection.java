package com.app.anamules.singleton;

import android.content.Context;

import com.app.anamules.been.golfCourse.GolfCourse;
import com.app.anamules.common.Constant;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.listeners.GolfCourseSelectionNear;

public class GolfSelection
{

    public static GolfSelection instance;
    Context context;
    public static GolfCourseSelectionNear golfCourseSelection;

    public GolfSelection(Context context,GolfCourseSelectionNear golfCourseSelection)
    {
        this.context = context;
        this.golfCourseSelection = golfCourseSelection;
    }

    public static GolfSelection setCallBack(Context constant,GolfCourseSelectionNear golfCourseSelection)
    {
        instance = new GolfSelection(constant,golfCourseSelection);
        return instance;
    }

    public static GolfCourseSelectionNear getCallBack()
    {
        return golfCourseSelection;
    }
}
