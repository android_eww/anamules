package com.app.anamules.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.anamules.BuildConfig;
import com.app.anamules.R;
import com.app.anamules.been.login.Login;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.databinding.ActivitySigninBinding;
import com.app.anamules.listeners.ApiInterface;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiClient;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigninActivity extends BaseActivity implements View.OnClickListener {

    ActivitySigninBinding binding;
    public TextView tvForgotPass;
    public LinearLayout llSignupBottom;
    ApiInterface apiService;

    //
    LoginButton loginButton;
    CallbackManager callbackManager;

    //
    private GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient ;
    private final int RC_SIGN_IN = 5;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_signin);
        init();
        initGoogleAPIClient();
        checkPermissions();

        apiService = ApiClient.getClient().create(ApiInterface.class);
        retriveToken();
    }

    private void init()
    {
        loginButton = findViewById(R.id.login_button);
        tvForgotPass  = findViewById(R.id.tvForgotPass);
        llSignupBottom  = findViewById(R.id.llSignupBottom);

        binding.tvSigin.setOnClickListener(this);
        binding.tvSignup.setOnClickListener(this);
        binding.tvLogin.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);


        binding.tvRegister.setOnClickListener(view -> {
            checkValidation();
        });

        binding.txtPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SigninActivity.this, HelpActivity.class);
                intent.putExtra(Constant.NAME, getResources().getString(R.string.txt_privicyPolicy));
                intent.putExtra(Constant.PDF_FILE_NAME, "privacy_policy.pdf");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        });

        binding.txtTermsOnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SigninActivity.this, HelpActivity.class);
                intent.putExtra(Constant.NAME, getResources().getString(R.string.txt_termsofservice));
                intent.putExtra(Constant.PDF_FILE_NAME, "terms_of_use.pdf");
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        });


        ///Facebook login
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()
                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                Log.e("TAG", loggedIn + " ??");
                if(loggedIn)
                {
                    getUserProfile(AccessToken.getCurrentAccessToken());
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        binding.llFb.setOnClickListener(view -> {

            boolean loggedOut = AccessToken.getCurrentAccessToken() == null;

            if (!loggedOut) {

                Log.e("TAG", "Username is: " + Profile.getCurrentProfile().getName());
                Log.e("TAG", "Username is: " + Profile.getCurrentProfile().getProfilePictureUri(200, 200));

                //Using Graph API
                getUserProfile(AccessToken.getCurrentAccessToken());
            }
            else
            {
                //LoginManager.getInstance().logOut();
                loginButton.performClick();
            }
        });

        ///Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        binding.llGoogle.setOnClickListener(view -> {

            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
            if(account != null)
            {
                handleSignInResult(account);
            }
            else
            {
                signIn();
            }
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut()
    {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        signIn();
                    }
                }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                signIn();
            }
        });
    }

    private void checkValidation()
    {
        if(!Network.isNetwork(activity))
        {
            //dialogCommon.setTitle(activity.getString(R.string.no_internet));
            showInternetDialog();
            //dialogCommon.show();
        }
        else if(binding.tvFirstName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_first_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvFirstName.requestFocus();
            return;
        }
        else if(binding.tvLastName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_last_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvLastName.requestFocus();
            return;
        }
        else if(binding.tvEmail.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(binding.tvEmail.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvEmail.requestFocus();
            return;
        }
        else if(binding.tvMobileNo.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_mobile),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvMobileNo.requestFocus();
            return;
        }
        else if(binding.tvZipCode.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_zipcode),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvZipCode.requestFocus();
            return;
        }
        else if(binding.tvPassword.getText().toString().trim().equalsIgnoreCase(""))
        {

            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvPassword.requestFocus();
            return;

        } else if(binding.tvPassword.getText().toString().length() < 6) {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_password_lenth),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvPassword.requestFocus();
            return;
        }
        else if(binding.tvVarifyPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_varify_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvVarifyPassword.requestFocus();
            return;
        }
        else if(!binding.tvVarifyPassword.getText().toString().trim().equalsIgnoreCase(binding.tvPassword.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_password_confirm_password_not_match),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvVarifyPassword.requestFocus();
            return;
        }
        else
        {
            loader.show();
            Call<Object> call = apiService.callRegister(binding.tvFirstName.getText().toString(),binding.tvLastName.getText().toString(),binding.tvEmail.getText().toString(),
                    binding.tvZipCode.getText().toString(),binding.tvPassword.getText().toString(),binding.tvMobileNo.getText().toString(),null,null,
                    Constant.lat_+"",Constant.lon_+"", Session.getUserSession(Session.USER_PREFERENCE_KEY_TOKEN,activity),"android");

            Log.e("TAG","url = "+call.request().url());
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object>call, Response<Object> response) {
                    loader.dismiss();
                    String x = gson.toJson(response.body());
                    Log.e("TAG","response = "+x);
                    login = gson.fromJson(x, Login.class);
                    if(login.getStatus())
                    {
                        Session.saveUserSession(Session.USER_PREFERENCE_LOGIN, x, activity);
                        Intent intent = new Intent(activity,HomeActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                        finish();
                    }
                    else
                    {
                        dialogCommon.showDialog("", login.getMessage(),"","Ok",null);
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.e("TAG","t = "+t.getMessage());
                    loader.dismiss();
                }
            });
        }
    }

    @Override
    public void onClick(View view)
    {
        if(view == binding.tvSigin)
        {
            binding.llSignin.setVisibility(View.VISIBLE);
            binding.llSignup.setVisibility(View.GONE);
            llSignupBottom.setVisibility(View.GONE);
            binding.tvSigin.setTextColor(getResources().getColor(R.color.colorGreen));
            binding.tvSignup.setTextColor(getResources().getColor(R.color.colorTextHint));
            binding.tvSigin.setBackground(getResources().getDrawable(R.drawable.bg_rounded_border));
            binding.tvSignup.setBackground(getResources().getDrawable(R.drawable.bg_rounded_border_grey));
        }
        else if(view == binding.tvSignup)
        {
            binding.llSignin.setVisibility(View.GONE);
            binding.llSignup.setVisibility(View.VISIBLE);
            llSignupBottom.setVisibility(View.VISIBLE);
            binding.tvSigin.setTextColor(getResources().getColor(R.color.colorTextHint));
            binding.tvSignup.setTextColor(getResources().getColor(R.color.colorGreen));
            binding.tvSigin.setBackground(getResources().getDrawable(R.drawable.bg_rounded_border_grey));
            binding.tvSignup.setBackground(getResources().getDrawable(R.drawable.bg_rounded_border));
        }
        else if(view == binding.tvLogin)
        {
            checkLoginValidation();
        }
        else if(view == tvForgotPass)
        {
            Intent intent = new Intent(activity,ForgotPasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        }

    }

    private void checkLoginValidation()
    {
        if(!Network.isNetwork(activity))
        {
            /*dialogCommon.setTitle(activity.getString(R.string.no_internet));
            dialogCommon.show();*/
            showInternetDialog();
        }
        else if(binding.etEmail.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(binding.etEmail.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etEmail.requestFocus();
            return;
        }
        else if(binding.etPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etPassword.requestFocus();
            return;
        }
        else
        {

            Log.e(TAG, "param : "+ binding.etEmail.getText().toString().trim() + " "+ Constant.lat_ + " "+ Constant.lon_ + " "+ Session.getUserSession(Session.USER_PREFERENCE_KEY_TOKEN,activity) + " "+ binding.etPassword.getText().toString().trim());

            loader.show();
            Call<Object> call = apiService.calllogin(binding.etEmail.getText().toString().trim(), Constant.lat_+"",Constant.lon_+"",
                    Session.getUserSession(Session.USER_PREFERENCE_KEY_TOKEN,activity),"android", binding.etPassword.getText().toString().trim());

            Log.e("TAG","url = "+call.request().url());
            Log.e("TAG","url = "+call.request().headers());
            Log.e("TAG","url = "+call.request().body().toString());
            Log.e("TAG","url = "+call.request().toString());
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object>call, Response<Object> response) {
                    loader.dismiss();
                    String x = gson.toJson(response.body());
                    Log.e("TAG","response = "+x);
                    Log.e("TAG","response = "+response.code());
                    login = gson.fromJson(x, Login.class);
                    if(login.getStatus())
                    {
                        Session.saveUserSession(Session.USER_PREFERENCE_LOGIN,x,activity);
                        Intent intent = new Intent(activity,HomeActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                        finish();
                    }
                    else
                    {
                        dialogCommon.showDialog("","Your email or password is incorrect.","","Ok",null);
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    loader.dismiss();
                    Log.e("TAG","t = "+t.getMessage());
                }
            });
        }
    }

    public void retriveToken()
    {
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Session.saveUserSession(Session.USER_PREFERENCE_KEY_TOKEN,token,activity);
                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.e(TAG, msg);
                        //Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN)
        {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInAccount acct = null;
            try {
                acct = task.getResult(ApiException.class);
                handleSignInResult(acct);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }

    }

    ///google signin
    private void handleSignInResult(GoogleSignInAccount acct) {
        try {


            // Signed in successfully, show authenticated UI.
            String id = acct.getId();
            Log.e("TAG","id = "+id);
            String personName = acct.getDisplayName();
            Log.e("TAG","personName = "+personName);
            String personGivenName = acct.getGivenName();
            Log.e("TAG","personGivenName = "+personGivenName );
            String personFamilyName = acct.getFamilyName();
            Log.e("TAG","personFamilyName = "+personFamilyName);
            String personEmail = acct.getEmail();
            Log.e("TAG","personEmail= "+personEmail);
            String personId = acct.getId();
            Log.e("TAG","personId = "+personId);
            Uri personPhoto = acct.getPhotoUrl();
            Log.e("TAG","personPhoto = "+personPhoto);

        } catch (Exception e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getMessage());
        }
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                            Log.e("TAG", "First Name: " + first_name + "\nLast Name: " + last_name);
                            Log.e("TAG", "email: " + email);
                            Log.e("TAG", "image_url: " + image_url);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }
}
