package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.databinding.ActivityChangePasswordBinding;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityChangePasswordBinding binding;
    public String TAG = "ChangePasswordActivity";
    public ChangePasswordActivity activity;

    public LinearLayout llBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_change_password);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_change_password);
        activity = ChangePasswordActivity.this;

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        llBack = findViewById(R.id.llBack);

        llBack.setOnClickListener(this);


        binding.tvDone.setOnClickListener(view -> {
            checkValidation();
        });

    }

    private void checkValidation()
    {
        if (!Network.isNetwork(activity))
        {
            /*dialogCommon.setTitle(activity.getString(R.string.no_internet));
            dialogCommon.show();*/

            showInternetDialog();
        }
        else if (binding.etOldPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent, getString(R.string.error_enter_old_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etOldPassword.requestFocus();
            return;
        }
        else if (binding.etPassword.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent, getString(R.string.error_enter_new_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etPassword.requestFocus();
            return;
        }
        else if (binding.etVarifyPass.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent, getString(R.string.error_enter_varify_password),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etVarifyPass.requestFocus();
            return;
        }
        else if (!binding.etVarifyPass.getText().toString().trim().equalsIgnoreCase(binding.etPassword.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent, getString(R.string.error_password_confirm_password_not_match),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etVarifyPass.requestFocus();
            return;
        }
        else {

            String url = WebserviceApi.API_CHANGEPASSWORD;
            HashMap<String,String> param = new HashMap<>();
            param.put(WebserviceApi.API_CHANGEPASSWORD_ID,login.getData().getID());
            param.put(WebserviceApi.API_CHANGEPASSWORD_NEWPASS,binding.etVarifyPass.getText().toString());
            param.put(WebserviceApi.API_CHANGEPASSWORD_OLDPASS,binding.etOldPassword.getText().toString());

            apiCall.callPost(true, url, param, login.getKey(),new ApiCall.GetResponce() {
                @Override
                public void GetResponce(String Response) {
                    Log.e("TAG","response = "+Response);
                    try {
                        JSONObject jsonObject = new JSONObject(Response);
                        dialogCommon.showDialog("",jsonObject.getString("message"),"","Ok",null);
                        if(jsonObject.getBoolean("status"))
                        {
                            binding.etVarifyPass.setText("");
                            binding.etOldPassword.setText("");
                            binding.etPassword.setText("");
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG, "ERROR : "+e.getMessage());
                    }
                }

                @Override
                public void error(String error) {
                    Log.e(TAG, "ERROR : "+error);
                }
            });
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
