package com.app.anamules.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.app.anamules.R;
import com.app.anamules.adapter.GameHistoryDetailAdapter;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.been.GameHistoryDetailBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Global;
import com.app.anamules.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

public class GameHistoryDetailActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "GameHistoryDetailActivity";
    public GameHistoryDetailActivity activity;

    public RecyclerView rvGameDetail;
    public GameHistoryDetailAdapter gameHistoryDetailAdapter;
    public List<GameHistoryDetailBeen> gameHistoryDetailBeenList = new ArrayList<>();
    private GameHistoryBeen gameHistoryBeen;
    private CustomTextView txt_cource_name, txt_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_history_detail);

        activity = GameHistoryDetailActivity.this;

        init();
    }

    private void init() {

        Intent intent = getIntent();
        gameHistoryBeen = (GameHistoryBeen) intent.getSerializableExtra(Constant.GAME_DETAIL);


        Log.e(TAG,"init()");

        callToolbar(true,false);


        tvTitle.setText(gameHistoryBeen.getCourseName());

        txt_date = findViewById(R.id.txt_date);
        rvGameDetail = findViewById(R.id.rvGameDetail);
        txt_cource_name = findViewById(R.id.txt_cource_name);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvGameDetail.setLayoutManager(layoutManager);
        gameHistoryDetailAdapter = new GameHistoryDetailAdapter(activity, gameHistoryBeen.getPlayer_detail());
        rvGameDetail.setAdapter(gameHistoryDetailAdapter);

        txt_cource_name.setText(gameHistoryBeen.getCourseName());

        txt_date.setText(Global.getDateFormated(gameHistoryBeen.getCreatedDate()));



        llBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }
}
