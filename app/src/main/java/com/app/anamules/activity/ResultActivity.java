package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.app.anamules.R;
import com.app.anamules.adapter.GameHistoryDetailAdapter;
import com.app.anamules.adapter.ResultDeatilAdapter;
import com.app.anamules.been.GameHistoryDetailBeen;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Global;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.databinding.ActivityResultBinding;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ResultActivity extends BaseActivity
{
    ResultActivity activity;
    ActivityResultBinding binding;
    ResultDeatilAdapter adapter;
    List<GameHistoryDetailBeen> gameHistoryDetailBeenList = new ArrayList<>();
    ArrayList<PlayerHoleDetail> playerHoleDetailsList, sortPlayerHoleDetailsList;
    int tieCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_result);
        activity = ResultActivity.this;
        init();
    }

    private void init() {

        binding.lottie.playAnimation();

        playerHoleDetailsList = new ArrayList<>();
        sortPlayerHoleDetailsList = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        binding.rvWinner.setLayoutManager(layoutManager);


        binding.tvSendDrink.setOnClickListener(view -> {

            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                callEndGame(Session.getCurrentGame(activity).getOwnerUserID(), Session.getCurrentGame(activity).getID());
            }
        });

        //binding.ivBack.setVisibility(View.VISIBLE);
        binding.ivBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.tvSelectRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i = 0 ; i < playerHoleDetailsList.size(); i++) {
                    playerHoleDetailsList.get(i).setSelect(false);
                }

                int radom = new Random().nextInt(tieCount);
                Log.e(TAG, "Random : "+radom);
                adapter.zookeperPostion = radom;
                binding.tvSelectRandom.setVisibility(View.GONE);
                binding.tvSendDrink.setVisibility(View.VISIBLE);
                playerHoleDetailsList.get(radom).setSelect(true);
                setHolePlayerAdapter();
                binding.tvSendDrink.setText("Send free Beverage to " + playerHoleDetailsList.get(radom).getFirstName() + " " + playerHoleDetailsList.get(radom).getLastName());
                //adapter.notifyDataSetChanged();

            }
        });

        callTotalUpdatedScoredHoleWise("18", Session.getCurrentGame(activity).getID());
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void callEndGame(String OwnerUserID, String gameID) {
        String url = WebserviceApi.API_GOLF_GAME_END;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_OWNER_USER_ID, OwnerUserID);
        param.put(WebserviceApi.KEY_GAME_ID, gameID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            String barcode = jsonObject.getString("barcode");
                            String message = jsonObject.getString("message");

                            Intent intent = new Intent(activity, OfferActivity.class);
                            intent.putExtra(Constant.BARCODE, barcode);
                            intent.putExtra(Constant.NAME, playerHoleDetailsList.get(0).getFirstName() + " "+ playerHoleDetailsList.get(0).getLastName());
                            // // | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();

                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void callTotalUpdatedScoredHoleWise(String HoleID, String GameID) {

        String url = WebserviceApi.API_GOLF_COURSE_TOTAL_UPDATED_SCORE_HOLEWISE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_HOLE_ID, HoleID);
        param.put(WebserviceApi.KEY_GAME_ID, GameID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            playerHoleDetailsList.clear();

                            JSONArray jsonArray = jsonObject.getJSONArray("player");

                            for(int i = 0 ; i < jsonArray.length(); i++ ) {
                                PlayerHoleDetail playerHoleDetail = gson.fromJson(jsonArray.getJSONObject(i).toString(), PlayerHoleDetail.class);
                                playerHoleDetailsList.add(playerHoleDetail);
                            }

                            Log.e(TAG, "callTotalUpdatedScoredHoleWise() Size : "+playerHoleDetailsList.size());


                            sortAnamule();

                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });


    }

    private void setHolePlayerAdapter() {
        adapter = new ResultDeatilAdapter(activity, playerHoleDetailsList);
        binding.rvWinner.setAdapter(adapter);
    }

    private void sortAnamule() {
        //setHolePlayerAdapter();

        for(int i = 0 ; i < playerHoleDetailsList.size(); i++) {
            for(int j = i+1; j < playerHoleDetailsList.size(); j++) {
                if (Integer.parseInt(playerHoleDetailsList.get(i).getTotal_anamules()) < Integer.parseInt(playerHoleDetailsList.get(j).getTotal_anamules())) {
                    PlayerHoleDetail playerHoleDetail = playerHoleDetailsList.get(i);
                    playerHoleDetailsList.set(i, playerHoleDetailsList.get(j));
                    playerHoleDetailsList.set(j, playerHoleDetail);
                }
            }
        }



        for(int i = 0 ; i < playerHoleDetailsList.size(); i++) {
            if(Integer.parseInt(playerHoleDetailsList.get(i).getTotal_anamules()) == Integer.parseInt(playerHoleDetailsList.get(0).getTotal_anamules())) {
                playerHoleDetailsList.get(i).setSelect(true);
                tieCount++;
            }
        }

        setHolePlayerAdapter();

        if(tieCount == 1) {
            binding.tvSelectRandom.setVisibility(View.GONE);
            binding.tvSendDrink.setVisibility(View.VISIBLE);
            binding.tvSendDrink.setText("Send free Beverag to " + playerHoleDetailsList.get(0).getFirstName() + " " + playerHoleDetailsList.get(0).getLastName());
        } else {
            binding.tvSelectRandom.setVisibility(View.VISIBLE);
            binding.tvSendDrink.setVisibility(View.GONE);
        }

    }
}
