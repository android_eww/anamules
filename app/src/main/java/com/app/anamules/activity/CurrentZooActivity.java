package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.adapter.CurrentZooAdapter;
import com.app.anamules.been.AnimalBeen;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.databinding.ActivityCurrentzooBinding;
import com.app.anamules.listeners.TackAnamuleListener;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class CurrentZooActivity extends BaseActivity implements TackAnamuleListener
{
    ActivityCurrentzooBinding binding;
    CurrentZooAdapter adapter;

    private List<AnimalBeen> animalList;
    PlayerHoleDetail playerHoleDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_currentzoo);
        init();
    }

    private void init() {

        Intent intent = getIntent();
        playerHoleDetail = (PlayerHoleDetail) intent.getSerializableExtra(Constant.USER_ZOO);

        callToolbar(false,false);

        animalList = new ArrayList<>();

        if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_CURRENT_ZOO))
        {
            tvTitle.setText(Constant.FROM_CURRENT_ZOO);
            binding.tvTackAnamule.setVisibility(View.GONE);
            getAnimalList(Session.getCurrentGame(CurrentZooActivity.this).getID(), Constant.FROM_CURRENT_ZOO);

        } else if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_THE_ZOO)) {
            tvTitle.setText(Constant.FROM_THE_ZOO);
            binding.tvTackAnamule.setVisibility(View.VISIBLE);
            getAnimalList(Session.getCurrentGame(CurrentZooActivity.this).getID(), Constant.FROM_THE_ZOO);
        }

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.tvTackAnamule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter != null) {
                    if(!adapter.getSelectAnamuleId().equalsIgnoreCase("")) {
                        if(!Network.isNetwork(activity)) {
                            showInternetDialog();
                        } else {
                            callTackAnamule(Session.getCurrentGame(activity).getID(), playerHoleDetail.getID(), adapter.getSelectAnamuleId(), playerHoleDetail.getCurrent_hole_score().getHoleID());
                        }
                    } else {
                        Toast.makeText(activity, "Please select anamules.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }


    private void getAnimalList(String GameID, String From) {

        String url = WebserviceApi.API_ANAMULES_ANIMAL_LIST + GameID;

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for(int i = 0 ;  i < jsonArray.length(); i++) {
                                AnimalBeen animalBeen = gson.fromJson(jsonArray.get(i).toString(), AnimalBeen.class);
                                animalList.add(animalBeen);
                            }

                            Collections.sort(animalList, new sortByAnimul());

                            setAdapter(From);

                        } else {
                            Log.e(TAG, "ERROR : API ERROR ");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void setAdapter(String From) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity,3);
        binding.rvZoo.setLayoutManager(layoutManager);
        adapter = new CurrentZooAdapter(activity, animalList, From, this);
        binding.rvZoo.setAdapter(adapter);
        adapter.setItemClicable(true,true);
    }

    @Override
    public void onClickTackAnamule(String anuleID) {
        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {
            callTackAnamule(Session.getCurrentGame(activity).getID(), playerHoleDetail.getID(), anuleID, playerHoleDetail.getCurrent_hole_score().getHoleID());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void callTackAnamule(String gameID, String PlayerID, String AnamuleID, String HoleID) {

        /*GameID:57
        PlayerID:119
        AnamuleID:4
        HoleID:1*/
        String url = WebserviceApi.API_SCORE_TAKE_ANAMULE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_GAME_ID, gameID);
        param.put(WebserviceApi.KEY_PLAYER_ID, PlayerID);
        param.put(WebserviceApi.KEY_ANAMULE_ID, AnamuleID);
        param.put(WebserviceApi.KEY_HOLE_ID, HoleID);

        apiCall.callPost(true, url, param, login.getKey(),  new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            String message = "Try again...";

                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "ERROR : API");
                            //finish();
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception : ");
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : ");
            }
        });


    }

    public class sortByAnimul implements Comparator<AnimalBeen>  {

        @Override
        public int compare(AnimalBeen o1, AnimalBeen o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
