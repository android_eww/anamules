package com.app.anamules.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.adapter.HelpAdapter;
import com.app.anamules.been.animals.Animls;
import com.app.anamules.been.animals.Datum;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.dialog.Loader;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;


import java.util.ArrayList;
import java.util.List;



public class HelpActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "HelpActivity";
    public HelpActivity activity;

    public TextView tvBack;
    private RecyclerView rv_help;
    private HelpAdapter helpAdapter;

    private Animls animls;
    List<Datum> list = new ArrayList<>();
    public WebView webView;
    PDFView pdfView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        activity = HelpActivity.this;

        init();
    }

    private void init() {

        Log.e(TAG,"init()");

        Intent intent = getIntent();

        String pdf_name = intent.getStringExtra(Constant.PDF_FILE_NAME);
        String name = intent.getStringExtra(Constant.NAME);

        callToolbar(false,false);
        tvTitle.setText(name);

        tvBack = findViewById(R.id.tvBack);

        webView = (WebView) findViewById(R.id.webView);



        pdfView = findViewById(R.id.pdfView);

        //pdfView.fromAsset("anamules_game_rules.pdf").load();

        pdfView.fromAsset(pdf_name)
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();



        //loader.show();
        webView.setWebViewClient(new WebViewClient());

        /*webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click...");

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);
                loader.dismiss();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);

            }
        });*/
        //pdfView.setMinZoom(100);
        //pdfView.setMidZoom(200);
        //pdfView.setMaxZoom(100);



        try {
            WebSettings settings = webView.getSettings();
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setSupportMultipleWindows(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setAppCacheEnabled(true);
            settings.setAppCacheMaxSize(10 * 1024 * 1024);
            settings.setAppCachePath("");
            settings.setDatabaseEnabled(true);
            settings.setDomStorageEnabled(true);
            settings.setGeolocationEnabled(true);
            settings.setSaveFormData(false);
            settings.setSavePassword(false);
            settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            // Flash settings
            settings.setPluginState(WebSettings.PluginState.ON);

            // Geo location settings
            settings.setGeolocationEnabled(true);
            //settings.setGeolocationDatabasePath("/data/data/selendroid");


        } catch (Exception e) {
            //SelendroidLogger.error("Error configuring web view", e);
        }


        if(!Network.isNetwork(activity)) {

            showInternetDialog();
        } else {
            webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+"http://projects.excellentwebworld.info/sites/default/files/Anamules%20Game%20Rules%20%281%29.pdf");
        }


        tvBack.setOnClickListener(this);

        //callAnimListApi();
    }

    private void callAnimListApi()
    {
        apiCall.callGet(true, WebserviceApi.API_ANAMULES_ANIMAL_LIST+"0", login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response)
            {
                animls = gson.fromJson(Response,Animls.class);
                if(animls.getData() != null)
                {list .addAll(animls.getData());}

                rv_help = findViewById(R.id.rv_help);
                rv_help.setLayoutManager(new LinearLayoutManager(activity));
                helpAdapter = new HelpAdapter(activity,list);
                rv_help.setAdapter(helpAdapter);
                rv_help.setNestedScrollingEnabled(false);
            }

            @Override
            public void error(String error) {

            }
        });
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.tvBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
