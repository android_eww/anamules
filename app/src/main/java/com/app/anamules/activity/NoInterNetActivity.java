package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.app.anamules.R;
import com.app.anamules.activity.parent.ImageActivity;
import com.app.anamules.common.Network;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.databinding.ActivityNoInternetBinding;
import com.app.anamules.listeners.CallbackImage;

public class NoInterNetActivity extends ImageActivity implements SwipeRefreshLayout.OnRefreshListener
{
    ActivityNoInternetBinding binding;
    NoInterNetActivity activity;

    CallbackImage callbackImage;
    Uri imageUri;
    String imageFilePth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_no_internet);
        activity = NoInterNetActivity.this;
        init();
    }

    private void init()
    {
        try {
            callbackImage= (CallbackImage) activity;
        }
        catch (Exception e)
        {
            Log.e("TAG","ccc = "+e.getMessage());
        }

        binding.swipeListener.setOnRefreshListener(this);

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.ivGolfImg.setOnClickListener(view -> {
            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                this.imageFilePth = imageFilePath;
                binding.ivGolfImg.setImageURI(imageUri);
            });
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRefresh() {
        Log.e("TAG","Refresh");

        if(!Network.isNetwork(activity))
        {
            binding.swipeListener.setRefreshing(false);
            new SnackbarUtils(binding.mainContent,getString(R.string.no_internet),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
        }
        else
        {
            binding.swipeListener.setRefreshing(false);
            Intent intent = new Intent(activity,GolfCourseListActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            finish();
        }
    }
}
