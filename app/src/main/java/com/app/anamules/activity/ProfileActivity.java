package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.app.anamules.R;
import com.app.anamules.activity.parent.ImageActivity;
import com.app.anamules.been.login.Data;
import com.app.anamules.been.login.Login;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.databinding.ActivityProfileBinding;
import com.app.anamules.dialog.Loader;
import com.app.anamules.listeners.CallbackImage;
import com.app.anamules.webservice.WebserviceApi;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ProfileActivity extends ImageActivity
{
    ProfileActivity activity;
    ActivityProfileBinding binding;

    CallbackImage callbackImage;
    Uri imageUri;
    String imageFilePth;
    Loader loader;
    private Login profileUpdate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_profile);
        activity = ProfileActivity.this;
        loader = new Loader(activity);
        init();
    }

    private void init()
    {

        try {
            callbackImage= (CallbackImage) activity;
        }
        catch (Exception e)
        {
            Log.e("TAG","ccc = "+e.getMessage());
        }

        binding.llBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.ivUserImg.setOnClickListener(view -> {

            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                this.imageFilePth = imageFilePath;
                binding.ivUserImg.setImageURI(imageUri);
                binding.txtName.setVisibility(View.GONE);
            });

        });


        binding.tvUpdateProfile.setOnClickListener(view -> {
            checkValidation();
        });


        setValue();
    }

    private void checkValidation()
    {
        if(!Network.isNetwork(activity))
        {
            /*dialogCommon.setTitle(activity.getString(R.string.no_internet));
            dialogCommon.show();*/

            showInternetDialog();
        }
        else if(binding.etFirstName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_first_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etFirstName.requestFocus();
            return;
        }
        else if(binding.etLastName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_last_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etLastName.requestFocus();
            return;
        }
        else if(binding.etEmail.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(binding.etEmail.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etEmail.requestFocus();
            return;
        }
        else if(binding.tvMobileNo.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_mobile),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.tvMobileNo.requestFocus();
            return;
        }
        else if(binding.etZipCode.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_zipcode),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etZipCode.requestFocus();
            return;
        }
        else
        {
            callUpdate();
        }
    }

    private void callUpdate()
    {
        loader.show();
        final OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder requestBody= new MultipartBody.Builder().setType(MultipartBody.FORM);

        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_FNAME,binding.etFirstName.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_LNAME,binding.etLastName.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_EMAIL,binding.etEmail.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_ID,login.getData().getID());
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_LAT, Constant.lat_+"");
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_LNG,Constant.lon_+"");
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_PHONE,binding.tvMobileNo.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_ZIPCODE,binding.etZipCode.getText().toString().trim());

        if(imageFilePth != null && !imageFilePth.isEmpty())
        {
            //File file =new File(imageUri.getPath());
            File file = new  File(imageFilePth);
            requestBody.addFormDataPart(WebserviceApi.API_UPDATEPROFILE_IMG,file.getName(),RequestBody.create(MediaType.parse("image/jpg"),file));
            Log.e("TAG","imgpath param = "+imageFilePth);
        }

        RequestBody body = requestBody.build();
        final okhttp3.Request request = new okhttp3.Request.Builder().url(WebserviceApi.API_UPDATEPROFILE).post(body).header(WebserviceApi.HEADER_KEY,login.getKey()).build();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                okhttp3.Response response = null;
                try {
                    response = client.newCall(request).execute();

                    Log.e(TAG, "Responce : "+response.body().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {

                    //Log.e("TAG","add recipe api responce = "+response.body().string());
                    return response.body().string();
                } catch (Exception e) {
                    e.printStackTrace();
                    return "";
                }
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                loader.dismiss();
                Log.e("TAG","res = "+response);
                if(response == null || response.equalsIgnoreCase("")){return;}
                profileUpdate  = gson.fromJson(response,Login.class);
                dialogCommon.showDialog("", "Profile updated successfully.","","Ok",null);
                if(profileUpdate.getStatus())
                {
                    //login.setData(null);
                    //login.setData(profileUpdate.getData());
                    /*login.getData().setProfilePicture(profileUpdate.getData().getProfilePicture());
                    login.setData(profileUpdate.getData());*/
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        /*String x = gson.toJson(login);
                        Session.saveUserSession(Session.USER_PREFERENCE_LOGIN, jsonObject.getJSONObject("data").toString(), activity);*/
                        //refreshDateLogin();

                        Data data = gson.fromJson(jsonObject.getJSONObject("data").toString(), Data.class);

                        login.getData().setProfilePicture(data.getProfilePicture());
                        login.getData().setFirstName(data.getFirstName());
                        login.getData().setLastName(data.getLastName());
                        login.getData().setEmail(data.getEmail());
                        login.getData().setZipcode(data.getZipcode());
                        login.getData().setPhone(data.getPhone());
                        login.getData().setLatitude(data.getLatitude());
                        login.getData().setLongitude(data.getLongitude());
                        login.getData().setCreatedDate(data.getCreatedDate());

                        String x = gson.toJson(login);
                        Session.saveUserSession(Session.USER_PREFERENCE_LOGIN, x, activity);
                        login = null;

                        refreshDateLogin();
                    } catch (Exception e) {

                    }
                }
            }
        }.execute();
    }

    private void setValue()
    {
        binding.etEmail.setClickable(false);
        binding.etEmail.setFocusableInTouchMode(false);

        binding.etFirstName.setText(login.getData().getFirstName());
        binding.etLastName.setText(login.getData().getLastName());
        binding.etEmail.setText(login.getData().getEmail());
        if(login.getData().getPhone() != null && !login.getData().getPhone().equalsIgnoreCase(""))
        {binding.tvMobileNo.setText(login.getData().getPhone());}
        if(login.getData().getZipcode() != null && !login.getData().getZipcode().equalsIgnoreCase(""))
        {binding.etZipCode.setText(login.getData().getZipcode());}
        if(login.getData().getProfilePicture() != null && !login.getData().getProfilePicture().contains("No_Image.png"))
        {
            binding.txtName.setVisibility(View.GONE);
            Picasso.with(activity).load(login.getData().getProfilePicture()).into(binding.ivUserImg);
        } else {
            binding.txtName.setVisibility(View.VISIBLE);
            String ch = login.getData().getFirstName().charAt(0) +""+ login.getData().getLastName().charAt(0);
            binding.txtName.setText(ch.toUpperCase());
            Picasso.with(activity).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(binding.ivUserImg);
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e("TAG","onBackPressed()");
        //super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }



}
