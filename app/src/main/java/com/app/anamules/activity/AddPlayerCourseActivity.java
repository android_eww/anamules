package com.app.anamules.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.adapter.PlayerAdapter;
import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.GolfCourseBeen;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.been.golfCourse.GolfCourse;
import com.app.anamules.been.player.AddplayerBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.databinding.ActivityPlayerSelectionBinding;
import com.app.anamules.dialog.Loader;
import com.app.anamules.listeners.AddPlayerFromListener;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.singleton.GolfSelection;
import com.app.anamules.view.CustomEditText;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;
import com.facebook.common.Common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddPlayerCourseActivity extends BaseActivity implements AddPlayerFromListener {

    ActivityPlayerSelectionBinding binding;
    AddPlayerCourseActivity activity;
    PlayerAdapter playerAdapter;
    List<RecentPlayerBeen> addplayerBeens = new ArrayList<>();
    List<RecentPlayerBeen> playerlist = new ArrayList<>();
    List<GolfCourseBeen> CourseData = new ArrayList<>();

    //
    int position;

    boolean isGolfSelect = false;
    private int addPlyaerCount = 0;
    private GetRecentCourses getRecentCourses;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_player_selection);
        activity = AddPlayerCourseActivity.this;
        init();
    }

    private void init() {

        Intent intentData = getIntent();
        addplayerBeens = (ArrayList<RecentPlayerBeen>) intentData.getSerializableExtra(Constant.PLAYER_LIST);
        getRecentCourses = (GetRecentCourses) intentData.getSerializableExtra(Constant.GOLF_COURCE);

        if(addplayerBeens == null) {
            addplayerBeens = new ArrayList<>();
        }


        /*addplayerBeens.add(new RecentPlayerBeen());
        addplayerBeens.add(new RecentPlayerBeen());
        addplayerBeens.add(new RecentPlayerBeen());
        addplayerBeens.add(new RecentPlayerBeen());*/

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        binding.rvPlayer.setLayoutManager(layoutManager);
        playerAdapter = new PlayerAdapter(activity, addplayerBeens, login.getKey());
        binding.rvPlayer.setAdapter(playerAdapter);
        playerAdapter.setListener(this);

        if(addplayerBeens != null) {
            addPlyaerCount = addplayerBeens.size();
        }

        binding.ivAddPlayer.setOnClickListener(view -> {
            playerAdapter.addPlayer();
        });

        if(getRecentCourses != null && getRecentCourses.getName() != null) {
            binding.llChooseCourse.setVisibility(View.GONE);
            binding.imgGround.setImageResource(R.drawable.img_golf);
            binding.llInfoCourse.setVisibility(View.VISIBLE);

            isGolfSelect = true;

            binding.tvGolfName.setText(getRecentCourses.getName());
            binding.txtDistance.setText("0 mi");
        }

        binding.llChooseCourse.setOnClickListener(view -> {
            if (!Network.isNetwork(activity)) {
                Intent intent = new Intent(activity, NoInterNetActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            } else {
                /*GolfSelection.setCallBack(activity, new GolfCourseSelection() {
                    @Override
                    public void onSelectedRV(String From, int position, Datum datum) {
                        // Log.e("TAG","datum = "+datum.getName());
                    }

                    @Override
                    public void onSelectedRecent(String from, int position, GetRecentCourses datum) {

                    }
                });*/

                Intent intent = new Intent(activity, GolfCourseListActivity.class);
                startActivityForResult(intent, Constant.SELECTED_GOLF_RQQUEST);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        binding.imgGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Network.isNetwork(activity)) {
                    Intent intent = new Intent(activity, NoInterNetActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                /*GolfSelection.setCallBack(activity, new GolfCourseSelection() {
                    @Override
                    public void onSelectedRV(String From, int position, Datum datum) {
                        // Log.e("TAG","datum = "+datum.getName());
                    }

                    @Override
                    public void onSelectedRecent(String from, int position, GetRecentCourses datum) {

                    }
                });*/

                    Intent intent = new Intent(activity, GolfCourseListActivity.class);
                    startActivityForResult(intent, Constant.SELECTED_GOLF_RQQUEST);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        });



        binding.tvStartRound.setOnClickListener(view -> {

            if(!isGolfSelect) {
                Toast.makeText(activity, "Please select course first.", Toast.LENGTH_SHORT).show();
            } else if(addPlyaerCount < 2) {
                Toast.makeText(activity, "Please add Player.", Toast.LENGTH_SHORT).show();
            } else {
                callStartGame(login.getData().getID(), Session.getCurrentGame(activity).getID());
            }

        });

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });
    }

    @Override
    public void onAddPlayer(Class aClass, int position) {
        this.position = position;
        Intent intent = new Intent(activity, aClass);
        intent.putExtra(Constant.PLAYER_ID, addplayerBeens.get(position).getID());
        //intent.putExtra(Constant.LIST_PLAYERS, (Serializable) addplayerBeens);
        startActivityForResult(intent, Constant.SELECTED_PLAYER);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    @Override
    public void onAddPlayerFromContact(int position) {
        this.position = position;
        if (isReadPermissionAvailable()) {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.putExtra(Constant.PLAYER_ID, addplayerBeens.get(position).getID());
            startActivityForResult(intent, Constant.SELECTED_CONTACT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.SELECTED_PLAYER) {
            if (resultCode == RESULT_OK) {

                String player_id = data.getStringExtra(Constant.PLAYER_ID);

                if(player_id == null || player_id.equalsIgnoreCase("")) {

                    int player_type = data.getIntExtra(Constant.TYPE_PLAYER, 0);
                    if (player_type == 1) {

                        String player = data.getStringExtra(Constant.SELECTED_PLAYER_DATA);

                        try {

                            addplayerBeens.clear();
                            JSONArray jsonArray = new JSONArray(player);

                            for(int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
                                RecentPlayerBeen recentPlayerBeen = new RecentPlayerBeen();
                                recentPlayerBeen.setID(jsonObject.getString("ID"));
                                recentPlayerBeen.setEmail(jsonObject.getString("Email"));
                                recentPlayerBeen.setGameID(jsonObject.getString("GameID"));
                                recentPlayerBeen.setPosition(position + "");
                                recentPlayerBeen.setFirstName(jsonObject.getString("FirstName"));
                                recentPlayerBeen.setLastName(jsonObject.getString("LastName"));
                                recentPlayerBeen.setPlayerType(jsonObject.getString("PlayerType"));
                                recentPlayerBeen.setProfilePicture(jsonObject.getString("ProfilePicture"));
                                recentPlayerBeen.setPromoID(jsonObject.getString("PromoID"));
                                recentPlayerBeen.setZipCode(jsonObject.getString("ZipCode"));
                                recentPlayerBeen.setIsZookeeper(jsonObject.getString("IsZookeeper"));

                               // addplayerBeens.set(position, recentPlayerBeen);
                                addplayerBeens.add(recentPlayerBeen);

                                addPlyaerCount++;
                            }

                            playerAdapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            Log.e("TAG", "error = " + e.getMessage());
                        }
                    } else if (player_type == 2) {
                        /*RecentPlayerBeen recentPlayerBeen = (RecentPlayerBeen) data.getSerializableExtra(Constant.SELECTED_PLAYER_DATA);
                        addplayerBeens.set(position, recentPlayerBeen);
                        playerAdapter.notifyDataSetChanged();
                        addPlyaerCount++;*/

                        String userIds = "";

                        ArrayList<RecentPlayerBeen> player = (ArrayList<RecentPlayerBeen>) data.getSerializableExtra(Constant.SELECTED_PLAYER_DATA);

                        for(int i = 0;i<player.size();i++)
                        {
                            addplayerBeens.set(position, player.get(i));
                            playerAdapter.notifyDataSetChanged();
                            addPlyaerCount++;

                            if(userIds.equalsIgnoreCase("")) {
                                userIds = player.get(i).getID();
                            } else {
                                userIds = userIds + ","+ player.get(i).getID();
                            }

                            if((player.size()-1)!=i)
                            {
                                playerAdapter.addPlayer();
                                position++;
                            }
                        }



                        callAddPlayer("", "", "", "recent", Session.getCurrentGame(activity).getID(), userIds);
                    }
                } else {
                    int player_type = data.getIntExtra(Constant.TYPE_PLAYER, 0);
                    if (player_type == 1) {

                        String player = data.getStringExtra(Constant.SELECTED_PLAYER_DATA);

                        try {

                            JSONObject jsonObject = new JSONObject(player);
                            RecentPlayerBeen recentPlayerBeen = new RecentPlayerBeen();
                            recentPlayerBeen.setID(jsonObject.getString("ID"));
                            recentPlayerBeen.setEmail(jsonObject.getString("Email"));
                            recentPlayerBeen.setGameID(jsonObject.getString("GameID"));
                            recentPlayerBeen.setPosition(position + "");
                            recentPlayerBeen.setFirstName(jsonObject.getString("FirstName"));
                            recentPlayerBeen.setLastName(jsonObject.getString("LastName"));
                            recentPlayerBeen.setPlayerType(jsonObject.getString("PlayerType"));
                            recentPlayerBeen.setProfilePicture(jsonObject.getString("ProfilePicture"));
                            recentPlayerBeen.setPromoID(jsonObject.getString("PromoID"));
                            recentPlayerBeen.setZipCode(jsonObject.getString("ZipCode"));
                            recentPlayerBeen.setIsZookeeper(jsonObject.getString("IsZookeeper"));
                            addplayerBeens.set(position, recentPlayerBeen);
                            playerAdapter.notifyDataSetChanged();
                            addPlyaerCount++;

                        } catch (Exception e) {
                            Log.e("TAG", "error = " + e.getMessage());
                        }
                    } else if (player_type == 2) {
                        /*RecentPlayerBeen recentPlayerBeen = (RecentPlayerBeen) data.getSerializableExtra(Constant.SELECTED_PLAYER_DATA);
                        addplayerBeens.set(position, recentPlayerBeen);
                        playerAdapter.notifyDataSetChanged();
                        addPlyaerCount++;
                        //callAddPlayer(recentPlayerBeen.getFirstName(), recentPlayerBeen.getLastName(), recentPlayerBeen.getEmail(), "recent", Session.getCurrentGame(activity).getID(), recentPlayerBeen.getID());
                        callEditPlayer(recentPlayerBeen.getFirstName(), recentPlayerBeen.getLastName(), Session.getCurrentGame(activity).getID(), player_id, "recent");*/

                        String userIds = "";

                        ArrayList<RecentPlayerBeen> player = (ArrayList<RecentPlayerBeen>) data.getSerializableExtra(Constant.SELECTED_PLAYER_DATA);

                        for(int i = 0;i<player.size();i++)
                        {
                            addplayerBeens.set(position, player.get(i));
                            playerAdapter.notifyDataSetChanged();
                            addPlyaerCount++;

                            if(userIds.equalsIgnoreCase("")) {
                                userIds = player.get(i).getID();
                            } else {
                                userIds = userIds + ","+ player.get(i).getID();
                            }

                            if((player.size()-1)!=i)
                            {
                                playerAdapter.addPlayer();
                                position++;
                            }
                        }



                        callAddPlayer("", "", "", "recent", Session.getCurrentGame(activity).getID(), userIds);

                    }
                }
            }
        } else if (requestCode == Constant.SELECTED_CONTACT) {
            if (resultCode == RESULT_OK) {
                String name = "";
                String cNumber = "";
                String emailAddress = "";
                Uri contactData = data.getData();

                Cursor c = managedQuery(contactData, null, null, null, null);
                //Cursor c = cr1.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);

                if (c.moveToFirst()) {

                    String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    /*String email = c.getString(3);
                    Log.e(TAG, "Email : "+email);*/

                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    Log.e("Tag", "number = " + hasPhone);

                    if (hasPhone.equalsIgnoreCase("1")) {
                        Cursor phones = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                null, null);
                        phones.moveToFirst();
                        cNumber = phones.getString(phones.getColumnIndex("data1"));
                        Log.e("Tag", "number = " + cNumber);
                    }
                    name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    Log.e("Tag", "name = " + name);

                }

/////////////////////////////////
                ContentResolver cr = getBaseContext().getContentResolver();
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

                if (cur.getCount() > 0) {

                    Log.e("Content provider", "Reading contact  emails");

                    while (cur.moveToNext()) {
                        String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

                        // Create query to use CommonDataKinds classes to fetch emails
                        Cursor emails = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID
                                        + " = " + contactId, null, null);

                        while (emails.moveToNext()) {

                            // This would allow you get several email addresses
                            emailAddress = emails
                                    .getString(emails
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            Log.e("TAG", "Reading contact  emails " + emailAddress);
                        }

                        emails.close();
                    }

                }
                cur.close();

                if(emailAddress.equalsIgnoreCase("")) {

                    Intent intent = new Intent(activity, AddManuallyActivity.class);
                    intent.putExtra(Constant.NAME, name);
                    intent.putExtra(Constant.MOBILE_NO, cNumber);
                    startActivityForResult(intent, Constant.SELECTED_PLAYER);

                } else {
                    String arr[] = name.split(" ");

                    if(arr.length >= 2) {
                        callAddPlayer(arr[0], arr[1], emailAddress, "contact", Session.getCurrentGame(activity).getID(), "");
                    } else {
                        Intent intent = new Intent(activity, AddManuallyActivity.class);
                        intent.putExtra(Constant.NAME, name);
                        intent.putExtra(Constant.MOBILE_NO, cNumber);
                        startActivityForResult(intent, Constant.SELECTED_PLAYER);
                    }


                }

                /*addplayerBeens.set(position, new AddplayerBeen("", name, "", cNumber, position + "", emailAddress));
                playerAdapter.notifyDataSetChanged();*/

            }
        } else if (requestCode == Constant.SELECTED_GOLF_RQQUEST) {
            if (resultCode == RESULT_OK) {

                String from = data.getStringExtra(Constant.SELECT_TYPE);

                if(from.equals(Constant.FROM_GOLF_NEAR_ME)) {

                    Datum datum = (Datum) data.getSerializableExtra(Constant.SELECTED_GOLF);

//                Log.e("TAG","ddddddddd "+datum.getName());

                    callUpdateCourse(String.valueOf(datum.getId()), login.getData().getID(), Session.getCurrentGame(activity).getID(), datum.getName(), "0.0", "0.0");
                    binding.llChooseCourse.setVisibility(View.GONE);
                    binding.imgGround.setImageResource(R.drawable.img_golf);
                    binding.llInfoCourse.setVisibility(View.VISIBLE);

                    isGolfSelect = true;

                    binding.tvGolfName.setText(datum.getName());
                    if(datum.getDistance() != null) {
                        binding.txtDistance.setText(datum.getDistance() + " mi");
                    }

                } else if(from.equals(Constant.FROM_RECENTLY_PLAYED)) {

                    GetRecentCourses getRecentCourses = (GetRecentCourses) data.getSerializableExtra(Constant.SELECTED_GOLF);

                    callUpdateCourse(String.valueOf(getRecentCourses.getGolfbertID()), login.getData().getID(), Session.getCurrentGame(activity).getID(), getRecentCourses.getName(), "0.0", "0.0");
                    binding.llChooseCourse.setVisibility(View.GONE);
                    binding.imgGround.setImageResource(R.drawable.img_golf);
                    binding.llInfoCourse.setVisibility(View.VISIBLE);

                    isGolfSelect = true;

                    binding.tvGolfName.setText(getRecentCourses.getName());
                    binding.txtDistance.setText("0 mi");

                }

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constant.PERMISSION_READ) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onAddPlayerFromContact(position);
            }
        }
    }


    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.e("TAG", "onNewIntent");

        String from = intent.getStringExtra(Constant.SELECT_TYPE);

        if(from.equals(Constant.FROM_GOLF_NEAR_ME)) {

            Datum datum = (Datum) intent.getSerializableExtra(Constant.SELECTED_GOLF);

//                Log.e("TAG","ddddddddd "+datum.getName());

            if(datum != null) {

                callUpdateCourse(String.valueOf(datum.getId()), login.getData().getID(), Session.getCurrentGame(activity).getID(), datum.getName(), "0.0", "0,0");
                binding.llChooseCourse.setVisibility(View.GONE);
                binding.imgGround.setImageResource(R.drawable.img_golf);
                binding.llInfoCourse.setVisibility(View.VISIBLE);

                isGolfSelect = true;

                binding.tvGolfName.setText(datum.getName());
                if (datum.getDistance() != null) {
                    binding.txtDistance.setText(datum.getDistance() + " mi");
                }
            }

        } else if(from.equals(Constant.FROM_RECENTLY_PLAYED)) {

            GetRecentCourses getRecentCourses = (GetRecentCourses) intent.getSerializableExtra(Constant.SELECTED_GOLF);

            if(getRecentCourses != null) {

                callUpdateCourse(String.valueOf(getRecentCourses.getCourseID()), login.getData().getID(), Session.getCurrentGame(activity).getID(), getRecentCourses.getName(), "0.0", "0.0");
                binding.llChooseCourse.setVisibility(View.GONE);
                binding.imgGround.setImageResource(R.drawable.img_golf);
                binding.llInfoCourse.setVisibility(View.VISIBLE);

                isGolfSelect = true;

                binding.tvGolfName.setText(getRecentCourses.getName());
                binding.txtDistance.setText("0 mi");

            }

        }

        /*Datum datum = (Datum) intent.getSerializableExtra(Constant.SELECTED_GOLF);
        Log.e("TAG", "onNewIntent = " + datum.getName());*/
        super.onNewIntent(intent);
    }

    private void callStartGame(String OwnerUserID, String gameID) {

        HashMap<String, String> params = new HashMap<>();
        params.put(WebserviceApi.KEY_OWNER_USER_ID, OwnerUserID);
        params.put(WebserviceApi.KEY_GAME_ID, gameID);

        apiCall.callPost(true, WebserviceApi.API_START_GAME, params, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.has("status")) {

                        if (jsonObject.getBoolean("status")) {

                            JSONArray jsonArrayPlayer = jsonObject.getJSONArray("player_list");
                            JSONObject jsonCourseData = null;
                            if(jsonObject.getJSONObject("course_data") != null) {
                                jsonCourseData = jsonObject.getJSONObject("course_data");
                            }

                            for (int i = 0; i < jsonArrayPlayer.length(); i++) {
                                RecentPlayerBeen recentPlayerBeen = gson.fromJson(jsonArrayPlayer.get(i).toString(), RecentPlayerBeen.class);
                                playerlist.add(recentPlayerBeen);
                            }


                            if(jsonCourseData != null) {

                                GolfCourseBeen golfCourseBeen = gson.fromJson(jsonCourseData.toString(), GolfCourseBeen.class);
                                Session.saveGameCourseDeatil(jsonCourseData.toString(), activity);

                            }


                            /*for (int i = 0; i < jsonArrayCourseData.length(); i++) {
                                GolfCourseBeen golfCourseBeen = gson.fromJson(jsonArrayCourseData.get(i).toString(), GolfCourseBeen.class);
                                CourseData.add(golfCourseBeen);
                            }*/

                            Log.e(TAG, "playerlist lenth() : " + playerlist.size() + " GolfCourseBeen : " + CourseData.size());



                            //if(jsonCourseData != null) {
                                //if (playerlist.size() >= 2) {
                                    showHoleDialog(Integer.parseInt(Session.getGameCourseDatail(activity).getHoleCount()));
                                /*} else {
                                    Toast.makeText(activity, "Please add Player", Toast.LENGTH_SHORT).show();
                                }*/
                            /*} else {
                                Toast.makeText(activity, "Please select cource first.", Toast.LENGTH_SHORT).show();
                            }*/

                        } else {
                            Log.e(TAG, "ERROR : API ");
                            String message = getResources().getString(R.string.please_try_again);
                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : " + e);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : " + error);
            }
        });

    }

    private void callEditPlayer(String firstName, String lastName, String GameId, String ID, String PlayerType) {
        String url = WebserviceApi.API_EDIT_PLAYER;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_FIRST_NAME, firstName);
        param.put(WebserviceApi.KEY_LAST_NAME, lastName);
        param.put(WebserviceApi.KEY_GAME_ID, GameId);
        param.put(WebserviceApi.KEY_ID, ID);
        param.put(WebserviceApi.ADD_PLAYER_TYPE, PlayerType);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {



                        } else {
                            Log.e(TAG, "ERROR : API ");
                        }


                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }


                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });



    }

    private void callUpdateCourse(String GolfbertID, String OwnerUserID, String  GameID, String Name, String lat, String lng) {
        String url = WebserviceApi.API_GOLFGAME_UPDATE_COURSE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_GOLFBERT_ID, GolfbertID);
        param.put(WebserviceApi.KEY_OWNER_USER_ID, OwnerUserID);
        param.put(WebserviceApi.KEY_GAME_ID, GameID);
        param.put(WebserviceApi.KEY_NAME, Name);
        param.put("Lat", lat);
        param.put("Lng", lng);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            if(jsonObject.has("holes_count")) {
                                if(jsonObject.getString("holes_count").equalsIgnoreCase("0")) {
                                    showHoleCreateDialog(GolfbertID, GameID, Name);
                                }
                            }

                        } else {
                            Log.e(TAG, "ERROR : API ");

                            if(jsonObject.has("holes_count") ) {
                                showHoleCreateDialog(GolfbertID, GameID, Name);
                            }
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void callAddPlayer(String firstName, String lastName, String email, String playerType, String gameID, String userId) {
        String url = WebserviceApi.API_ADD_PLAYER;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.ADD_PLAYER_FNAME, firstName);
        param.put(WebserviceApi.ADD_PLAYER_LNAME, lastName);
        param.put(WebserviceApi.ADD_PLAYER_EMAIL, email);
        param.put(WebserviceApi.ADD_PLAYER_TYPE, playerType);
        param.put(WebserviceApi.KEY_GAME_ID, gameID);
        param.put(WebserviceApi.ADD_PLAYER_USERID, userId);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);
                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            RecentPlayerBeen recentPlayerBeen = gson.fromJson(jsonObject.getJSONObject("data").toString(), RecentPlayerBeen.class);
                            if(userId.equalsIgnoreCase("")) {
                                addplayerBeens.set(position, recentPlayerBeen);
                                playerAdapter.notifyDataSetChanged();
                                addPlyaerCount++;
                            }


                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "EXCEPTION : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void showHoleDialog(int max) {
        Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.layout_get_hole_dialog);
        dialog.setCancelable(false);

        CustomEditText txtHole = dialog.findViewById(R.id.edt_hole_number);
        CustomTextView tv_endRound_one = dialog.findViewById(R.id.tv_endRound_one);
        CustomTextView txt_inc = dialog.findViewById(R.id.txt_inc);

        txt_inc.setText("Enter hole number 1 -"+max);

        tv_endRound_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtHole.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Enter Hole Number", Toast.LENGTH_SHORT).show();
                } else if(Integer.parseInt(txtHole.getText().toString()) == 0) {
                    Toast.makeText(activity, "Please Enter valid hole number", Toast.LENGTH_SHORT).show();
                } else if(Integer.parseInt(txtHole.getText().toString()) > max) {
                    Toast.makeText(activity, "Please Enter valid hole number", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(activity, HoleActivity.class);
                    intent.putExtra(Constant.HOLE_NUMBER, Integer.parseInt(txtHole.getText().toString()));
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    finish();
                    dialog.dismiss();
                }

            }
        });

        dialog.show();
    }

    private void showHoleCreateDialog(String GolfbertID, String GameID, String Name) {
        Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.layout_create_hole_dialog);
        dialog.setCancelable(true);

        CustomEditText edt_hole_number = dialog.findViewById(R.id.edt_hole_number);
        CustomTextView tv_play = dialog.findViewById(R.id.tv_play);
        CustomTextView tv_cancel =  dialog.findViewById(R.id.tv_cancel);


        tv_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_hole_number.getText().equals("")) {
                    Toast.makeText(activity, "Please Enter Holes ", Toast.LENGTH_SHORT).show();
                } else if(Integer.parseInt(edt_hole_number.getText().toString()) == 0) {
                    Toast.makeText(activity, "Please Enter valid hole number", Toast.LENGTH_SHORT).show();
                } else {
                    callUpdateCourceManule(login.getData().getID(), GameID, GolfbertID, Name, edt_hole_number.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void callUpdateCourceManule(String OwnerUserID, String GameID, String GolfBertID, String Name, String HoleCount) {
        String url = WebserviceApi.API_GOLF_GAME_UPDATE_COURSE_MANUALLY;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_OWNER_USER_ID, OwnerUserID);
        param.put(WebserviceApi.KEY_GAME_ID, GameID);
        param.put(WebserviceApi.KEY_GOLFBERT_ID, GolfBertID);
        param.put(WebserviceApi.KEY_NAME, Name);
        param.put(WebserviceApi.KEY_HOLE_COUNT, HoleCount);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.has("status")) {

                            Log.e(TAG, "ADD Sussesful");

                            Toast.makeText(activity, "Holes added Successfully.", Toast.LENGTH_SHORT).show();

                        } else {
                            Log.e(TAG, "ERROR : API ");
                        }

                    } else {
                        Log.e(TAG, "ERROR : no status found");
                    }


                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }
}
