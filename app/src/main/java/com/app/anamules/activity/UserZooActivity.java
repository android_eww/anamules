package com.app.anamules.activity;

import android.bluetooth.BluetoothAssignedNumbers;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.adapter.CurrentZooAdapter;
import com.app.anamules.adapter.UserAdapter;
import com.app.anamules.been.AnimalBeen;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.databinding.ActivityUserzooBinding;
import com.app.anamules.dialog.DialogAnim;
import com.app.anamules.listeners.TackAnamuleListener;
import com.app.anamules.listeners.UserZooSelectListener;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserZooActivity extends BaseActivity implements View.OnClickListener, TackAnamuleListener, UserZooSelectListener
{
    ActivityUserzooBinding binding;
    UserZooActivity activity;
    CurrentZooAdapter adapter;

    //bottomsheet
    UserAdapter userAdapter;
    RecyclerView rv_User;
    LinearLayout bottom_sheet;
    BottomSheetBehavior behavior;
    CustomTextView tv_giveAnimalToUser;

    PlayerHoleDetail playerHoleDetail;

    private List<AnimalBeen> animalList;
    private List<PlayerHoleDetail> playerList;
    private PlayerHoleDetail playerHoleDetailSelect;
    private String anumalID = "";
    private String holeNumber = "";

    LinearLayout llBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_userzoo);
        activity = UserZooActivity.this;
        init();
    }

    private void init()
    {
        Intent intent = getIntent();
        playerHoleDetail = (PlayerHoleDetail) intent.getSerializableExtra(Constant.USER_ZOO);
        playerList = (List<PlayerHoleDetail>) intent.getSerializableExtra(Constant.USER_LIST);
        holeNumber = intent.getStringExtra(Constant.HOLE_NUMBER);

        animalList = new ArrayList<>();

        callToolbar(true,false);
        tvTitle.setText(playerHoleDetail.getFirstName()+"'s Zoo");



        rv_User = findViewById(R.id.rv_User);
        tv_giveAnimalToUser = findViewById(R.id.tv_giveAnimalToUser);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottom_sheet);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity,3);
        binding.rvUserzoo.setLayoutManager(layoutManager);
        //adapter = new CurrentZooAdapter(activity);
        binding.rvUserzoo.setAdapter(adapter);
//        adapter.setItemClicable(true,false);

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(activity);
        rv_User.setLayoutManager(layoutManager1);
        userAdapter = new UserAdapter(activity, playerList, this);
        rv_User.setAdapter(userAdapter);

        binding.tvGiveAnimal.setOnClickListener(view -> {
            if(anumalID != null && !anumalID.equalsIgnoreCase("")) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            } else {
                Toast.makeText(activity, "Please select Anamules", Toast.LENGTH_SHORT).show();
            }

        });

        llBack = findViewById(R.id.llBack);

        llBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.tvTakeAnimal.setOnClickListener(view -> {
            Intent intent1 = new Intent(activity, CurrentZooActivity.class);
            intent1.putExtra(Constant.FROM, Constant.FROM_THE_ZOO);
            intent1.putExtra(Constant.USER_ZOO, playerHoleDetail);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

        });

        tv_giveAnimalToUser.setOnClickListener(view -> {

            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (anumalID != null && !anumalID.equalsIgnoreCase("")) {
                    if (playerHoleDetailSelect != null) {
                        callGiveAnamule(Session.getCurrentGame(activity).getID(), playerHoleDetailSelect.getID(), anumalID, holeNumber);
                    }
                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {
            callCurrentAnamule(playerHoleDetail.getGameID(), playerHoleDetail.getID());
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void callCurrentAnamule(String gameID, String playerID) {
        String url = WebserviceApi.API_ANAMULES_CURRENT_ANAMULES + gameID + "/"+ playerID;

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.has("status")) {

                        if (jsonObject.getBoolean("status")) {

                            animalList.clear();
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for(int i = 0 ; i < jsonArray.length(); i++) {
                                AnimalBeen animalBeen = gson.fromJson(jsonArray.get(i).toString(), AnimalBeen.class);
                                animalList.add(animalBeen);
                            }

                            if(animalList.size() != 0) {

                                binding.rvUserzoo.setVisibility(View.VISIBLE);
                                binding.txtDataNotFound.setVisibility(View.GONE);
                            } else {
                                binding.rvUserzoo.setVisibility(View.GONE);
                                binding.txtDataNotFound.setVisibility(View.VISIBLE);
                            }

                            setAdapter();

                        } else {
                            Log.e(TAG, "ERROR : API");
                            binding.rvUserzoo.setVisibility(View.GONE);
                            binding.txtDataNotFound.setVisibility(View.VISIBLE);
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                        binding.rvUserzoo.setVisibility(View.GONE);
                        binding.txtDataNotFound.setVisibility(View.VISIBLE);
                    }

                }catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                    binding.rvUserzoo.setVisibility(View.GONE);
                    binding.txtDataNotFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : ");
                binding.rvUserzoo.setVisibility(View.GONE);
                binding.txtDataNotFound.setVisibility(View.VISIBLE);
            }
        });

    }

    private void setAdapter() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity,3);
        binding.rvUserzoo.setLayoutManager(layoutManager);
        adapter = new CurrentZooAdapter(activity, animalList, Constant.FROM_USER_ZOO, this);
        binding.rvUserzoo.setAdapter(adapter);
        adapter.setItemClicable(true,true);
    }

    private void callTackAnamule(String gameID, String PlayerID, String AnamuleID, String HoleID) {

        /*GameID:57
        PlayerID:119
        AnamuleID:4
        HoleID:1*/
        String url = WebserviceApi.API_SCORE_TAKE_ANAMULE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_GAME_ID, gameID);
        param.put(WebserviceApi.KEY_PLAYER_ID, PlayerID);
        param.put(WebserviceApi.KEY_ANAMULE_ID, AnamuleID);
        param.put(WebserviceApi.KEY_HOLE_ID, HoleID);

        apiCall.callPost(true, url, param, login.getKey(),  new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {



                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception : ");
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : ");
            }
        });


    }


    public void callGiveAnamule(String gameID, String PlayerID, String AnamuleID, String HoleID) {
        String url = WebserviceApi.API_SCORE_GIVE_ANAMULE;

        /*GameID:43
        PlayerID:118
        AnamuleID:5
        HoleID:1*/
        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_GAME_ID, gameID);
        param.put(WebserviceApi.KEY_PLAYER_ID, PlayerID);
        param.put(WebserviceApi.KEY_ANAMULE_ID, AnamuleID);
        param.put(WebserviceApi.KEY_HOLE_ID, HoleID);

        apiCall.callPost(true, url, param, login.getKey(),  new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {

                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            //userAdapter.clearSelection();
                            anumalID = null;
                            Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            finish();


                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception : ");
                }

            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : ");
            }
        });

    }

    @Override
    public void onClickTackAnamule(String anuleID) {
        this.anumalID = anuleID;
    }

    @Override
    public void selectPlayer(int postion) {
        playerHoleDetailSelect = playerList.get(postion);
    }
}
