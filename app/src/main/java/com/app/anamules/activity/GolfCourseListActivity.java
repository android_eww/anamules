package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.adapter.GolfCourseAdapter;
import com.app.anamules.adapter.GolfRecentCourseAdapter;
import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.been.golfCourse.GolfCourse;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.StartSnapHelper;
import com.app.anamules.databinding.ActivityGolfListBinding;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.listeners.GolfCourseSelectionNear;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GolfCourseListActivity extends BaseActivity implements GolfCourseSelection, GolfCourseSelectionNear {

    ActivityGolfListBinding binding;
    public static GolfCourseListActivity activity;
    GolfCourseAdapter golfCourseAdapter1;
    GolfRecentCourseAdapter golfRecentCourseAdapter;

    //
    private GolfCourse golfCourseNearme, golfCourseRecenly;
    private List<Datum> listRecentlyPlayed = new ArrayList<>();
    private List<Datum> listNearMe = new ArrayList<>();

    String from = "";
    int position = Integer.MAX_VALUE;
    Datum datum = null;
    GetRecentCourses getRecentCourses = null;

    private List<GetRecentCourses> recentCoursesList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_golf_list);
        activity = GolfCourseListActivity.this;
        init();

    }

    private void init() {

        SnapHelper startSnapHelper = new StartSnapHelper();
        startSnapHelper.attachToRecyclerView(binding.rvGolfNearMe);
        SnapHelper startSnapHelper1 = new StartSnapHelper();
        startSnapHelper1.attachToRecyclerView(binding.rvRecentlyPlayed);


        recentCoursesList = new ArrayList<>();


        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });


        binding.tvNearMeViewAll.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ViewAllGolfCourseActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_GOLF_NEAR_ME);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.tvRecentlyViewAll.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ViewAllGolfCourseActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_RECENTLY_PLAYED);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.searchBar.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ViewAllGolfCourseActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_GOLF_NEAR_ME);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });


        binding.tvDone.setOnClickListener(view -> {


            if(from.equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED))
            {
                //datum = listRecentlyPlayed.get(position);

                Intent intent = new Intent();
                intent.putExtra(Constant.SELECT_TYPE, Constant.FROM_RECENTLY_PLAYED);
                intent.putExtra(Constant.SELECTED_GOLF, recentCoursesList.get(position));
                setResult(RESULT_OK, intent);
                finish();

            }
            else if(from.equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME))
            {
                //datum = listNearMe.get(position);

                Intent intent = new Intent();
                intent.putExtra(Constant.SELECT_TYPE, Constant.FROM_GOLF_NEAR_ME);
                intent.putExtra(Constant.SELECTED_GOLF, listNearMe.get(position));
                setResult(RESULT_OK, intent);
                finish();
            }


        });

        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {

            callApiForNear();

            callRecentlyPlayedGolfApi(login.getData().getID());
        }


    }

    private void callRecentlyPlayedGolfApi(String userId) {
        Log.e("TAG", "gpsTracker.getLatitude() = " + gpsTracker.getLatitude());
        Log.e("TAG", "gpsTracker.getLatitude() = " + gpsTracker.getLongitude());
        String url = WebserviceApi.API_GET_RECENT_COURSES + userId;//+ gpsTracker.getLatitude()+"/"+gpsTracker.getLongitude();
        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                /*golfCourseRecenly = gson.fromJson(Response, GolfCourse.class);
                if(golfCourseRecenly.getData() != null)
                {listRecentlyPlayed.addAll(golfCourseRecenly.getData());}*/

                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if (jsonObject.has("status")) {

                        if (jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("RecentCources");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                GetRecentCourses getRecentCourses = gson.fromJson(jsonArray.get(i).toString(), GetRecentCourses.class);
                                recentCoursesList.add(getRecentCourses);
                            }

                            setRecentlyPlayedAdapter(recentCoursesList);
                            if(recentCoursesList.size() > 0) {
                                binding.llRecentlyPlayed.setVisibility(View.VISIBLE);
                            } else {
                                binding.llRecentlyPlayed.setVisibility(View.GONE);
                            }
                        } else {
                            binding.llRecentlyPlayed.setVisibility(View.GONE);
                        }

                    } else {
                        Log.e(TAG, "ERROR : ");
                        binding.llRecentlyPlayed.setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    Log.e(TAG, "ERROR : " + e);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : " + error);
            }
        });
    }

    private void setRecentlyPlayedAdapter(List<GetRecentCourses> listRecentlyPlayed) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        binding.rvRecentlyPlayed.setLayoutManager(linearLayoutManager);
        golfRecentCourseAdapter = new GolfRecentCourseAdapter(activity, listRecentlyPlayed);
        binding.rvRecentlyPlayed.setAdapter(golfRecentCourseAdapter);
        golfRecentCourseAdapter.setClickable(true, Constant.FROM_RECENTLY_PLAYED);
        golfRecentCourseAdapter.setListener(this);

    }

    private void callApiForNear() {
        Log.e("TAG", "gpsTracker.getLatitude() = " + gpsTracker.getLatitude());
        Log.e("TAG", "gpsTracker.getLatitude() = " + gpsTracker.getLongitude());
        String url = WebserviceApi.API_GOLF_COURSE + login.getData().getID() +"/"+ gpsTracker.getLatitude()+"/"+gpsTracker.getLongitude();
        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                golfCourseNearme = gson.fromJson(Response, GolfCourse.class);
                if (golfCourseNearme.getData() != null) {
                    listNearMe.addAll(golfCourseNearme.getData());
                }
                setNearMeAdapter(listNearMe);
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : " + error);
            }
        });
    }

    private void setNearMeAdapter(List<Datum> listNearMe) {
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        binding.rvGolfNearMe.setLayoutManager(linearLayoutManager1);
        golfCourseAdapter1 = new GolfCourseAdapter(activity, listNearMe);
        binding.rvGolfNearMe.setAdapter(golfCourseAdapter1);
        golfCourseAdapter1.setClickable(true, Constant.FROM_GOLF_NEAR_ME);
        golfCourseAdapter1.setListener(this);

    }


    @Override
    public void onSelectedRV(String From, int position, Datum datum) {

        //if (From.equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED)) {
            if (golfRecentCourseAdapter != null) {
                golfRecentCourseAdapter.setSelectedPos(Integer.MAX_VALUE);
            }
        //}

        this.datum = datum;
        from = From;
        this.position = position;
    }

    @Override
    public void onSelectedRecent(String From, int position, GetRecentCourses datum) {
        //if (From.equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME)) {
            if (golfCourseAdapter1 != null) {
                golfCourseAdapter1.setSelectedPos(Integer.MAX_VALUE);
            }
        //}

        from = From;
        this.getRecentCourses = datum;
        this.position = position;
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    /*public void selectItemAdapterRefresh(String from) {
        if(from.equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED)) {
            if (golfCourseAdapter1 != null) {
                golfCourseAdapter1.selectedPos = -1;
                golfCourseAdapter1.notifyDataSetChanged();
            }
        }

        if(from.equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME)) {
            if (golfRecentCourseAdapter != null) {
                golfRecentCourseAdapter.selectedPos = -1;
                golfRecentCourseAdapter.notifyDataSetChanged();
            }
        }
    }*/
}

