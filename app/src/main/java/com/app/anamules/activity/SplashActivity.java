package com.app.anamules.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Movie;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.app.anamules.BuildConfig;
import com.app.anamules.R;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.listeners.ApiInterface;
import com.app.anamules.webservice.ApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity
{
    SplashActivity activity;
    ApiInterface apiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = SplashActivity.this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        printHashKey();
        init();
    }

    private void init()
    {

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity,SigninActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                finish();
            }
        },2000);*/

        if (!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {
            callInitApi();
        }

    }

    private void callInitApi()
    {

        Call<Object> call = apiService.callInit(BuildConfig.VERSION_NAME,"Android", login != null ? login.getData().getID() : "0", gpsTracker.getLatitude()+"", gpsTracker.getLongitude()+"");
        Log.e("TAG","url = "+call.request().url());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object>call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("update") && jsonObject.getBoolean("update"))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setMessage(jsonObject.getString("message"))
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        gotoPlayStore();
                                        dialog.cancel();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.setTitle(getString(R.string.app_name));
                        alert.show();
                    }
                    else if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        String y = Session.getUserSession(Session.USER_PREFERENCE_LOGIN,activity);
                        if(y != null && !y.equalsIgnoreCase(""))
                        {
                            Intent intent = new Intent(activity,HomeActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent(activity,SigninActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("TAG","t = "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Object>call, Throwable t) {
                Log.e("TAG","t = "+t.getMessage());
            }
        });
    }


    public void gotoPlayStore()
    {
        final String appPackageName = getPackageName();
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/apps/internaltest/4701651358104549258")));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/apps/internaltest/4701651358104549258")));
        }
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("TAG", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }

}
