package com.app.anamules.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.anamules.R;
import com.app.anamules.adapter.CurrentZooAdapter;
import com.app.anamules.adapter.RecentPlayerAdapter;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.been.PlayerDetailBeen;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.LoadMoreListener;
import com.app.anamules.common.Network;
import com.app.anamules.databinding.ActivityAddRecentPlayerBinding;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddRecentPlayerActivity extends BaseActivity
{
    AddRecentPlayerActivity activity;
    ActivityAddRecentPlayerBinding binding;
    RecentPlayerAdapter adapter;

    private ArrayList<RecentPlayerBeen> playerList;
    private ArrayList<RecentPlayerBeen> selectedPlayerList = new ArrayList<>();
    String player_id = "";
    SwipeRefreshLayout swipeRefreshLayout;

    private ProgressBar progressBar;

    private int CurrentPage = 1;
    private boolean isLastpage = false;
    private LoadMoreListener mLoadMoreListener;
    RecyclerView.LayoutManager layoutManager;
    private boolean isLoading = false;



    ///
    //List<RecentPlayerBeen> addplayerBeens;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_add_recent_player);
        activity = AddRecentPlayerActivity.this;

        init();
    }

    private void init() {

        player_id = getIntent().getStringExtra(Constant.PLAYER_ID);
        //addplayerBeens = (List<RecentPlayerBeen>) getIntent().getSerializableExtra(Constant.LIST_PLAYERS);

        callToolbar(false,true);
        tvTitle.setText(getString(R.string.activity_recent_players));

        ivRight.setImageResource(R.drawable.ic_delete_button);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        progressBar = findViewById(R.id.progressBar);

        layoutManager = new GridLayoutManager(activity, 3);
        binding.rvRecentlyPlayers.setLayoutManager(layoutManager);

        llRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSelectPlayer();
            }
        });

        playerList = new ArrayList<>();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    callRecentPlayer(false, true);
                }
            }
        });


        binding.tvAdd.setOnClickListener(view -> {
            if(adapter.selectPos >= 0) {

                ////get total selected player
                for(int i =0; i <playerList.size() ; i++)
                {
                    if(playerList.get(i).isSelect())
                    {
                        selectedPlayerList.add(playerList.get(i));
                    }
                }

                Log.e("TAG","Total selectedPlayerList = "+selectedPlayerList.size());

                ///remove selected player which is already selected from recent player
                /*for(int  i = 0;i<selectedPlayerList.size();i++)
                {
                    for(int  j = 0;j<addplayerBeens.size();j++)
                    {
                        if(addplayerBeens.get(j).getID() != null && !addplayerBeens.get(j).getID().equalsIgnoreCase("") &&
                                addplayerBeens.get(j).getID().equalsIgnoreCase(selectedPlayerList.get(i).getID()))
                        {
                            selectedPlayerList.remove(i);
                        }
                    }
                }*/
                Log.e("TAG","selectedPlayerList = "+selectedPlayerList.size());

                Intent intent = new Intent();
                intent.putExtra(Constant.SELECTED_PLAYER_DATA, selectedPlayerList/*playerList.get(adapter.selectPos)*/);
                intent.putExtra(Constant.TYPE_PLAYER, 2);
                intent.putExtra(Constant.PLAYER_ID, player_id);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(activity, "Please select Player.", Toast.LENGTH_SHORT).show();
            }
        });

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        if (playerList.size() > 0) {
            setAdapter(true);
        } else {
            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                callRecentPlayer(true, false);
            }
        }
    }

    private void deleteSelectPlayer() {

        String selectPlayerIds = "";

        for(int i = 0 ; i < playerList.size(); i++) {
            if(playerList.get(i).isSelect()) {
                if (selectPlayerIds.equalsIgnoreCase("")) {
                    selectPlayerIds = playerList.get(i).getID() ;
                } else {
                    selectPlayerIds = selectPlayerIds + "," + playerList.get(i).getID();
                }
            }
        }

        if(selectPlayerIds.equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please select player", Toast.LENGTH_SHORT).show();
            return;
        } else {
            callRemovePlayer(selectPlayerIds);
        }
    }

    private void callRecentPlayer(final boolean isReload, final boolean isRefresh) {



        if(isRefresh) {
            CurrentPage = 1;
        }

        String url = WebserviceApi.API_RECENT_PLAYER + login.getData().getID() + "/" + CurrentPage ;

        apiCall.callGet(isReload, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);
                    swipeRefreshLayout.setRefreshing(false);

                    if(jsonObject.has("status")) {



                        if (isReload || isRefresh) {
                            playerList.clear();
                        }

                        if(jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            List<RecentPlayerBeen> tempList = new ArrayList<>();

                            for(int i = 0; i < jsonArray.length(); i++) {
                                RecentPlayerBeen recentPlayerBeen = gson.fromJson(jsonArray.get(i).toString(), RecentPlayerBeen.class);
                                tempList.add(recentPlayerBeen);
                            }

                            playerList.addAll(tempList);

                            /*------------------5-10-19--------------*/
                           /* for(int i =0;i<playerList.size();i++)
                            {
                                for(int j = 0;j<addplayerBeens.size();j++)
                                {
                                    if(addplayerBeens.get(j).getID() != null && !addplayerBeens.get(j).getID().equalsIgnoreCase("") &&
                                            addplayerBeens.get(j).getID().equalsIgnoreCase(playerList.get(i).getID()))
                                    {
                                        playerList.get(i).setSelect(true);
                                    }
                                }
                            }*/
                            /*------------------5-10-19--------------*/

                            /*setAdapter();

                            if(playerList.size() > 0) {
                                binding.rvRecentlyPlayers.setVisibility(View.VISIBLE);
                                binding.txtPlayerNotAvalibale.setVisibility(View.GONE);
                            } else {
                                binding.rvRecentlyPlayers.setVisibility(View.GONE);
                                binding.txtPlayerNotAvalibale.setVisibility(View.VISIBLE);
                            }*/

                        } else {
                            Log.e(TAG, "ERROR : API ERROR ");
                            binding.rvRecentlyPlayers.setVisibility(View.GONE);
                            binding.txtPlayerNotAvalibale.setVisibility(View.VISIBLE);
                        }

                    } else {

                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {

                    Log.e(TAG, "ERROR : "+e.getMessage());
                } finally {

                    progressBar.setVisibility(View.GONE);

                    List<RecentPlayerBeen> tempList = new ArrayList<>();

                    tempList.addAll(playerList);

                    playerList.clear();

                    playerList.addAll(tempList);

                    if(playerList.size() > 0) {
                        setAdapter(isReload || isRefresh);
                    }

                    if(playerList.size() > 0) {
                        binding.rvRecentlyPlayers.setVisibility(View.VISIBLE);
                        binding.txtPlayerNotAvalibale.setVisibility(View.GONE);
                    } else {
                        binding.rvRecentlyPlayers.setVisibility(View.GONE);
                        binding.txtPlayerNotAvalibale.setVisibility(View.VISIBLE);
                    }

                    if(adapter != null) {
                        adapter.notifyDataSetChanged();
                    }

                    isLoading = false;
                    //isLastpage = isLastPages;

                    if (mLoadMoreListener != null)
                        mLoadMoreListener.currentList(playerList.size());

                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void setAdapter(boolean isReload) {
        if(isReload) {

            adapter = new RecentPlayerAdapter(activity, playerList, true);
            binding.rvRecentlyPlayers.setAdapter(adapter);
            ///adapter.setItemClicable(true, false);

            mLoadMoreListener = new LoadMoreListener((LinearLayoutManager) layoutManager) {
                @Override
                protected void loadMoreItems() {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    CurrentPage++;
                    progressBar.setVisibility(View.VISIBLE);
                    callRecentPlayer(false, false);
                }

                @Override
                public boolean isLastPage() {
                    return isLastpage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            };

            binding.rvRecentlyPlayers.addOnScrollListener(mLoadMoreListener);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    public void callRemovePlayer(String ID) {
        String url = WebserviceApi.API_GOLF_GAME_REMOVE_PLAYER;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_ID, ID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            callRecentPlayer(false, true);
                            //adapter.notifyDataSetChanged();

                            String message = "Player removed Successfully";

                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            showDialog(message);

                        } else {
                            String message = "Please Try again...";

                            if(jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }

                            showDialog(message);
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Error : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void showDialog(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(message);

        dialog.setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
