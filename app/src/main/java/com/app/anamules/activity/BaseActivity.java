package com.app.anamules.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.anamules.R;
import com.app.anamules.been.login.Login;
import com.app.anamules.common.Constant;
import com.app.anamules.common.GpsTracker;
import com.app.anamules.common.Session;
import com.app.anamules.dialog.GeneralDialog;
import com.app.anamules.dialog.Loader;
import com.app.anamules.listeners.ApiInterface;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.ApiClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

public class BaseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public String TAG = "BaseActivity";
    public static BaseActivity activity;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;

    public LinearLayout llBack, llRight;
    public ImageView ivBack, ivRight;
    public TextView tvTitle;


    //Api
    public ApiCall apiCall;
    public ApiInterface apiServicewithDynamic;
    public Gson gson;
    public Loader loader;

    public GeneralDialog dialogCommon;
    public GpsTracker gpsTracker;

    //login session
    public Login login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        apiCall = new ApiCall(activity);
        apiServicewithDynamic = ApiClient.getClient1().create(ApiInterface.class);
        dialogCommon = new GeneralDialog(this);
        gson = new Gson();
        loader = new Loader(activity);
        gpsTracker = new GpsTracker(activity);

        String c = Session.getUserSession(Session.USER_PREFERENCE_LOGIN, activity);
        if(c != null && !c.equalsIgnoreCase(""))
        {
            login = gson.fromJson(c, Login.class);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void refreshDateLogin() {
        String c = Session.getUserSession(Session.USER_PREFERENCE_LOGIN, activity);
        if(c != null && !c.equalsIgnoreCase(""))
        {
            login = gson.fromJson(c, Login.class);
        }
    }

    public void requestReadContactPermission()
    {
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},Constant.PERMISSION_READ);
        }
    }

    public boolean isReadPermissionAvailable()
    {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestReadContactPermission();
            return false;
        }
    }

    public void callToolbar(boolean isBackVisible, boolean isRightVisible)
    {
        Log.e(TAG,"callToolbar()");

        llBack = findViewById(R.id.llBack);
        llRight = findViewById(R.id.llRight);
        ivBack = findViewById(R.id.ivBack);
        ivRight = findViewById(R.id.ivRight);
        tvTitle = findViewById(R.id.tvTitle);


        if (!isBackVisible &&
                !isRightVisible)
        {
            ivBack.setVisibility(View.GONE);
            ivRight.setVisibility(View.GONE);
        }
        else
        {
            if (isBackVisible)
            {
                ivBack.setVisibility(View.VISIBLE);
            }
            else
            {
                ivBack.setVisibility(View.GONE);
            }

            if (isRightVisible)
            {
                ivRight.setVisibility(View.VISIBLE);
            }
            else
            {
                ivRight.setVisibility(View.INVISIBLE);
            }
        }

    }

    public void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestLocationPermission();
            }
            else
            {
                showSettingDialog();
            }
        }
        else
        {
            showSettingDialog();
        }
    }

    private void requestLocationPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);

        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    public void initGoogleAPIClient()
    {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    public void showSettingDialog()
    {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode())
                {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //updateGPSStatus("GPS is Enabled in your device");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    public void createLocationRequest()
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(20000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

        startLocationUpdates();
    }

    public void startLocationUpdates()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,activity);
    }

    public void getLatLang()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if(lastLocation != null)
            {
                Constant.lat_ = lastLocation.getLatitude();
                Constant.lon_ = lastLocation.getLongitude();
            }


            Log.e("TAG", "lat = " + Constant.lat_);
            Log.e("TAG", "lon = " + Constant.lon_);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.e("TAG","Location Changed = "+location.getLatitude());
        Log.e("TAG","Location Changed = "+location.getLongitude());
        Constant.lat_ = location.getLatitude();
        Constant.lon_ = location.getLongitude();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ACCESS_FINE_LOCATION_INTENT_ID:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //If permission granted show location dialog if APIClient is not null
                    if (mGoogleApiClient == null)
                    {
                        initGoogleAPIClient();
                        showSettingDialog();
                    }
                    else
                    {
                        showSettingDialog();
                    }
                }
                else
                {
                    //updateGPSStatus("Location Permission denied.");
                    //Toast.makeText(MainActivity.this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "Result OK");
                        //updateGPSStatus("GPS is Enabled in your device");
                        createLocationRequest();
                        //startLocationUpdates();
                        getLatLang();
                        break;
                    case RESULT_CANCELED:
                        Log.e("Settings", "Result Cancel");
                        //updateGPSStatus("GPS is Disabled in your device");
                        break;
                }
                break;
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void showInternetDialog() {
        new AlertDialog.Builder(activity)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(getResources().getString(R.string.please_check_your_internet))
                .setPositiveButton(getResources().getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }
}
