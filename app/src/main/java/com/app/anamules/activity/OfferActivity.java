package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telecom.Call;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.app.anamules.R;
import com.app.anamules.common.Constant;
import com.app.anamules.databinding.ActivityOfferBinding;
import com.squareup.picasso.Picasso;

public class OfferActivity extends BaseActivity implements View.OnClickListener
{
    ActivityOfferBinding binding;
    OfferActivity activity;
    String barcode = "";
    String name = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer);
        activity = OfferActivity.this;
        init();
    }

    private void init()
    {
        Intent intent = getIntent();
        barcode = intent.getStringExtra(Constant.BARCODE);
        name = intent.getStringExtra(Constant.NAME);

        callToolbar(true,false);
        tvTitle.setText(getString(R.string.activity_offer));

        llBack.setOnClickListener(this);

        if(!barcode.equalsIgnoreCase("")) {
            Picasso.with(activity).load(barcode).into(binding.imgBarcode);
        }

        binding.txtSorray.setText(Html.fromHtml("<b>"+name.toUpperCase()+"</b>"+ ", "+ getResources().getString(R.string.sorry_that_you_lost)));

        binding.tvRedeem.setOnClickListener(view -> {
            if(binding.tvRedeem.getText().toString().equalsIgnoreCase("HOME")) {

                Intent intent1 = new Intent(OfferActivity.this, HomeActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                finish();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
            }
            binding.ivSuccess.setVisibility(View.VISIBLE);
            binding.tvRedeem.setText("HOME");
        });


    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        //super.onBackPressed();
        Intent intent1 = new Intent(OfferActivity.this, HomeActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent1);
        finish();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
