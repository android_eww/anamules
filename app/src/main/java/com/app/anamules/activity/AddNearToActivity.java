package com.app.anamules.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.app.anamules.R;
import com.app.anamules.databinding.ActivityNearToMeBinding;
import com.skyfishjy.library.RippleBackground;

public class AddNearToActivity extends BaseActivity implements View.OnClickListener {
    AddNearToActivity activity;
    ActivityNearToMeBinding binding;

    RippleBackground content;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_near_to_me);
        activity = AddNearToActivity.this;
        init();
    }

    private void init()
    {
        callToolbar(true,false);
        tvTitle.setText(getString(R.string.activity_near_to_you));

        content = findViewById(R.id.content);

        content.startRippleAnimation();

        llBack.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }
}
