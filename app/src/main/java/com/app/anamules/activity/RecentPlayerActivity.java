package com.app.anamules.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.app.anamules.R;
import com.app.anamules.adapter.RecentPlayerAdapter;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.LoadMoreListener;
import com.app.anamules.common.Network;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecentPlayerActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "RecentPlayerActivity";
    public RecentPlayerActivity activity;

    public RecyclerView rvRecentPlayer;
    public RecentPlayerAdapter recentPlayerAdapter;

    private ArrayList<RecentPlayerBeen> playerList;
    private CustomTextView txt_player_not_avalibale;
    boolean isSelect = false;

    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;

    private int CurrentPage = 1;
    private boolean isLastpage = false;
    private LoadMoreListener mLoadMoreListener;
    RecyclerView.LayoutManager layoutManager;
    private boolean isLoading = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_player);

        activity = RecentPlayerActivity.this;

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        isSelect = getIntent().getBooleanExtra(Constant.IS_SELECT, false);

        callToolbar(true, false);
        tvTitle.setText(getString(R.string.activity_recent_players));

        playerList = new ArrayList<>();

        rvRecentPlayer = findViewById(R.id.rvRecentPlayer);
        txt_player_not_avalibale = findViewById(R.id.txt_player_not_avalibale);

        progressBar = findViewById(R.id.progressBar);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        layoutManager = new GridLayoutManager(this, 3);
        rvRecentPlayer.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {

                    callRecentPlayer(false, true);
                }
            }
        });

        llBack.setOnClickListener(this);

        if (playerList.size() > 0) {
            setAdapter(true);
        } else {
            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                callRecentPlayer(true, false);
            }
        }

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    private void callRecentPlayer(final boolean isReload, final boolean isRefresh) {

        if(isRefresh) {
            CurrentPage = 1;
        }

        String url = WebserviceApi.API_RECENT_PLAYER + login.getData().getID() + "/" + CurrentPage;

        apiCall.callGet(isReload, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    swipeRefreshLayout.setRefreshing(false);

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            if (isReload || isRefresh) {
                                playerList.clear();
                            }

                            List<RecentPlayerBeen> tempList = new ArrayList<>();

                            for(int i = 0; i < jsonArray.length(); i++) {
                                RecentPlayerBeen recentPlayerBeen = gson.fromJson(jsonArray.get(i).toString(), RecentPlayerBeen.class);
                                tempList.add(recentPlayerBeen);
                            }

                            Log.e(TAG, "playerList Size : "+playerList.size());

                            playerList.addAll(tempList);

                            /*if(playerList.size() > 0) {
                                rvRecentPlayer.setVisibility(View.VISIBLE);
                                txt_player_not_avalibale.setVisibility(View.GONE);
                            } else {
                                rvRecentPlayer.setVisibility(View.GONE);
                                txt_player_not_avalibale.setVisibility(View.VISIBLE);
                            }

                            setAdapter();*/


                        } else {
                            rvRecentPlayer.setVisibility(View.GONE);
                            txt_player_not_avalibale.setVisibility(View.VISIBLE);
                            Log.e(TAG, "ERROR : API ERROR ");
                        }

                    } else {
                        rvRecentPlayer.setVisibility(View.GONE);
                        txt_player_not_avalibale.setVisibility(View.VISIBLE);
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    rvRecentPlayer.setVisibility(View.GONE);
                    txt_player_not_avalibale.setVisibility(View.VISIBLE);
                    Log.e(TAG, "ERROR : "+e.getMessage());
                } finally {
                    progressBar.setVisibility(View.GONE);

                    List<RecentPlayerBeen> tempList = new ArrayList<>();

                    tempList.addAll(playerList);

                    playerList.clear();

                    playerList.addAll(tempList);

                    if(playerList.size() > 0) {
                        setAdapter(isReload || isRefresh);
                    }

                    if(playerList.size() > 0) {
                        rvRecentPlayer.setVisibility(View.VISIBLE);
                        txt_player_not_avalibale.setVisibility(View.GONE);
                    } else {
                        rvRecentPlayer.setVisibility(View.GONE);
                        txt_player_not_avalibale.setVisibility(View.VISIBLE);
                    }

                    if(recentPlayerAdapter != null) {
                        recentPlayerAdapter.notifyDataSetChanged();
                    }

                    isLoading = false;
                    //isLastpage = isLastPages;

                    if (mLoadMoreListener != null)
                        mLoadMoreListener.currentList(playerList.size());
                }
            }

            @Override
            public void error(String error) {
                rvRecentPlayer.setVisibility(View.GONE);
                txt_player_not_avalibale.setVisibility(View.VISIBLE);
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void setAdapter(boolean isReload) {

        if(isReload) {

            recentPlayerAdapter = new RecentPlayerAdapter(activity, playerList, false);
            recentPlayerAdapter.setHasStableIds(true);
            rvRecentPlayer.setAdapter(recentPlayerAdapter);

            mLoadMoreListener = new LoadMoreListener((LinearLayoutManager) layoutManager) {
                @Override
                protected void loadMoreItems() {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    CurrentPage++;
                    progressBar.setVisibility(View.VISIBLE);
                    callRecentPlayer(false, false);
                }

                @Override
                public boolean isLastPage() {
                    return isLastpage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            };

            rvRecentPlayer.addOnScrollListener(mLoadMoreListener);

        } else {
            recentPlayerAdapter.notifyDataSetChanged();
        }
    }
}
