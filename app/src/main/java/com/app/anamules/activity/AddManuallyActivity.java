package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.app.anamules.R;
import com.app.anamules.activity.parent.ImageActivity;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.databinding.ActivityAddManuallyBinding;
import com.app.anamules.listeners.CallbackImage;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;

public class AddManuallyActivity extends ImageActivity {

    AddManuallyActivity activity;
    ActivityAddManuallyBinding binding;

    CallbackImage callbackImage;
    Uri imageUri;
    String imageFilePth;

    String name, mobile_no, player_id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_add_manually);
        activity = AddManuallyActivity.this;
        init();
    }

    private void init() {

        Intent intent = getIntent();
        name = intent.getStringExtra(Constant.NAME);
        mobile_no  = intent.getStringExtra(Constant.MOBILE_NO);
        player_id = intent.getStringExtra(Constant.PLAYER_ID);

        try {
            callbackImage= (CallbackImage) activity;
        }
        catch (Exception e)
        {
            Log.e("TAG","ccc = "+e.getMessage());
        }

        callToolbar(false,false);
        tvTitle.setText(getString(R.string.activity_add_manually));

        binding.tvAdd.setOnClickListener(view -> {
            checkValidation();
        });

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.ivAddUserImg.setOnClickListener(view -> {

            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                this.imageFilePth = imageFilePath;
                binding.ivAddUserImg.setImageURI(imageUri);
            });

        });

        if(name != null) {
            binding.etFirstName.setText(name);
        }

        if(mobile_no != null) {
            binding.tvMobileNo.setText(mobile_no);
        }
    }

    private void checkValidation()
    {
        if(!Network.isNetwork(activity))
        {
            /*dialogCommon.setTitle(activity.getString(R.string.no_internet));
            dialogCommon.show();*/
            showInternetDialog();
        }
        else if(binding.etFirstName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_first_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etFirstName.requestFocus();
            return;
        }
        else if(binding.etLastName.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_last_name),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etLastName.requestFocus();
            return;
        }
        else if(binding.etEmail.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(binding.etEmail.getText().toString().trim()))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etEmail.requestFocus();
            return;
        }
        else if(binding.etZipCode.getText().toString().trim().equalsIgnoreCase(""))
        {
            new SnackbarUtils(binding.mainContent,getString(R.string.error_enter_zipcode),
                    ContextCompat.getColor(activity, R.color.colorWhite),
                    ContextCompat.getColor(activity, R.color.colorPrimary),
                    ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
            binding.etZipCode.requestFocus();
            return;
        }
        else
        {
            if(player_id == null || player_id.equalsIgnoreCase("")) {
                callAddApi();
            } else {
                callEditApi();
            }
        }
    }

    private void callEditApi()
    {
        loader.show();
        final OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder requestBody= new MultipartBody.Builder().setType(MultipartBody.FORM);

        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_FNAME,binding.etFirstName.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_LNAME,binding.etLastName.getText().toString().trim());
        //requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_EMAIL,binding.etEmail.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_ZIPCODE, binding.etZipCode.getText().toString().trim());
        //requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_USERID, login.getData().getID());
        requestBody.addFormDataPart(WebserviceApi.KEY_GAME_ID, Session.getCurrentGame(activity).getID());
        requestBody.addFormDataPart(WebserviceApi.KEY_ID, player_id);
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_TYPE, "manual");

        if(imageFilePth != null && !imageFilePth.isEmpty())
        {
            File file = new  File(imageFilePth);
            requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_IMG,file.getName(),RequestBody.create(MediaType.parse("image/jpg"),file));
            Log.e("TAG","imgpath param = "+imageFilePth);
        }

        RequestBody body = requestBody.build();
        final okhttp3.Request request = new okhttp3.Request.Builder().url(WebserviceApi.API_EDIT_PLAYER).post(body).header(WebserviceApi.HEADER_KEY,login.getKey()).build();


        Buffer buffer = new Buffer();

        try {
            request.body().writeTo(buffer);
            Log.e("TAG","buffer.readUtf8() = "+buffer.readUtf8());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("TAG","e = "+e.getMessage());
        }

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                okhttp3.Response response = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {

                    //Log.e("TAG","add recipe api responce = "+response.body().string());
                    return response.body().string();
                } catch (Exception e) {
                    e.printStackTrace();
                    return "";
                }
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                loader.dismiss();
                Log.e("TAG","res = "+response);
                if(response == null || response.equalsIgnoreCase("")){return;}
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getBoolean("status"))
                    {
                        Intent intent = new Intent();
                        intent.putExtra(Constant.SELECTED_PLAYER_DATA, jsonObject.get("data").toString());
                        intent.putExtra(Constant.TYPE_PLAYER, 1);
                        intent.putExtra(Constant.PLAYER_ID, player_id);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
                catch (Exception e)
                {

                }
            }
        }.execute();
    }

    private void callAddApi() {
        loader.show();
        final OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder requestBody= new MultipartBody.Builder().setType(MultipartBody.FORM);

        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_FNAME,binding.etFirstName.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_LNAME,binding.etLastName.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_EMAIL,binding.etEmail.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_ZIPCODE, binding.etZipCode.getText().toString().trim());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_USERID,login.getData().getID());
        requestBody.addFormDataPart(WebserviceApi.KEY_GAME_ID, Session.getCurrentGame(activity).getID());
        requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_TYPE, "manual");

        if(imageFilePth != null && !imageFilePth.isEmpty())
        {
            File file = new  File(imageFilePth);
            requestBody.addFormDataPart(WebserviceApi.ADD_PLAYER_IMG,file.getName(),RequestBody.create(MediaType.parse("image/jpg"),file));
            Log.e("TAG","imgpath param = "+imageFilePth);
        }

        RequestBody body = requestBody.build();
        final okhttp3.Request request = new okhttp3.Request.Builder().url(WebserviceApi.API_ADD_PLAYER).post(body).header(WebserviceApi.HEADER_KEY,login.getKey()).build();

        Buffer buffer = new Buffer();

        try {
            request.body().writeTo(buffer);
            Log.e("TAG","buffer.readUtf8() = "+buffer.readUtf8());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("TAG","e = "+e.getMessage());
        }

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                okhttp3.Response response = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {

                    //Log.e("TAG","add recipe api responce = "+response.body().string());
                    return response.body().string();
                } catch (Exception e) {
                    e.printStackTrace();
                    return "";
                }
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                loader.dismiss();
                Log.e("TAG","res = "+response);
                if(response == null || response.equalsIgnoreCase("")){return;}
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getBoolean("status"))
                    {
                        Intent intent = new Intent();
                        intent.putExtra(Constant.SELECTED_PLAYER_DATA, jsonObject.get("data").toString());
                        intent.putExtra(Constant.TYPE_PLAYER, 1);
                        intent.putExtra(Constant.PLAYER_ID, player_id);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
                catch (Exception e)
                {

                }
            }
        }.execute();
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
