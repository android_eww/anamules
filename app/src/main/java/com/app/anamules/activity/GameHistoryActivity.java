package com.app.anamules.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.app.anamules.R;
import com.app.anamules.adapter.GameHistoryAdapter;
import com.app.anamules.been.GameHistoryBeen;
import com.app.anamules.common.LoadMoreListener;
import com.app.anamules.common.Network;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameHistoryActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "GameHistoryActivity";
    public GameHistoryActivity activity;

    public RecyclerView rvGameHistory;
    public GameHistoryAdapter gameHistoryAdapter;
    public List<GameHistoryBeen> gameHistoryBeenList = new ArrayList<>();
    public CustomTextView txt_data_not_found;
    public SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;

    private int CurrentPage = 1;
    private boolean isLastpage = false;
    private LoadMoreListener mLoadMoreListener;
    RecyclerView.LayoutManager layoutManager;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_history);

        activity = GameHistoryActivity.this;

        init();
    }

    private void init() {

        Log.e(TAG,"init()");

        callToolbar(true,true);
        tvTitle.setText(getString(R.string.activity_game_history));

        rvGameHistory = findViewById(R.id.rvGameHistory);
        txt_data_not_found = findViewById(R.id.txt_data_not_found);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        progressBar = findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(this);
        rvGameHistory.setLayoutManager(layoutManager);



        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    callGameHistory(false, true);
                }
            }
        });




        if (gameHistoryBeenList.size() > 0) {
            setAdapter(true);
        } else {
            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                callGameHistory(true, false);
            }
        }

        llBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    private void callGameHistory(final boolean isReload, final boolean isRefresh) {
        String Url = WebserviceApi.API_SCORE_GAME_HISTORY;

        if(isRefresh) {
            CurrentPage = 1;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put(WebserviceApi.API_USER_ID, login.getData().getID());
        params.put(WebserviceApi.API_PAGE_NO, String.valueOf(CurrentPage));



        apiCall.callPost(isReload, Url, params, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        swipeRefreshLayout.setRefreshing(false);

                        if (isReload || isRefresh) {
                            gameHistoryBeenList.clear();
                        }

                        if(jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            List<GameHistoryBeen> tempList = new ArrayList<>();


                            for(int i = 0 ; i < jsonArray.length(); i++) {
                                GameHistoryBeen gameHistoryBeen = gson.fromJson(jsonArray.get(i).toString(), GameHistoryBeen.class);
                                tempList.add(gameHistoryBeen);
                            }

                            if(tempList.size() == 0) {
                                isLastpage = true;
                            } else {
                                gameHistoryBeenList.addAll(tempList);
                                isLastpage = false;
                            }

                            /*gameHistoryAdapter.notifyDataSetChanged();

                            if(gameHistoryBeenList.size() > 0) {
                                rvGameHistory.setVisibility(View.VISIBLE);
                                txt_data_not_found.setVisibility(View.GONE);
                            } else {
                                rvGameHistory.setVisibility(View.GONE);
                                txt_data_not_found.setVisibility(View.VISIBLE);
                            }*/


                        } else {
                            rvGameHistory.setVisibility(View.GONE);
                            txt_data_not_found.setVisibility(View.VISIBLE);
                            Log.e(TAG, "ERROR : api Error");
                        }

                    } else {
                        rvGameHistory.setVisibility(View.GONE);
                        txt_data_not_found.setVisibility(View.VISIBLE);
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    rvGameHistory.setVisibility(View.GONE);
                    txt_data_not_found.setVisibility(View.VISIBLE);
                    Log.e(TAG, "ERROR : " + e);
                } finally {

                    progressBar.setVisibility(View.GONE);

                    List<GameHistoryBeen> tempList = new ArrayList<>();

                    tempList.addAll(gameHistoryBeenList);

                    gameHistoryBeenList.clear();

                    gameHistoryBeenList.addAll(tempList);

                    if(gameHistoryBeenList.size() > 0) {
                        setAdapter(isReload || isRefresh);
                    }

                    if(gameHistoryBeenList.size() > 0) {
                        rvGameHistory.setVisibility(View.VISIBLE);
                        txt_data_not_found.setVisibility(View.GONE);
                    } else {
                        rvGameHistory.setVisibility(View.GONE);
                        txt_data_not_found.setVisibility(View.VISIBLE);
                    }

                    if(gameHistoryAdapter != null) {
                        gameHistoryAdapter.notifyDataSetChanged();
                    }

                    isLoading = false;
                    //isLastpage = isLastPages;

                    if (mLoadMoreListener != null)
                        mLoadMoreListener.currentList(gameHistoryBeenList.size());

                }
            }

            @Override
            public void error(String error) {
                rvGameHistory.setVisibility(View.GONE);
                txt_data_not_found.setVisibility(View.VISIBLE);
                Log.e(TAG, "ERROR : " + error);
            }
        });


    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void setAdapter(boolean isReload) {

        if(isReload) {


            gameHistoryAdapter = new GameHistoryAdapter(activity, gameHistoryBeenList);
            rvGameHistory.setAdapter(gameHistoryAdapter);

            mLoadMoreListener = new LoadMoreListener((LinearLayoutManager) layoutManager) {
                @Override
                protected void loadMoreItems() {
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    CurrentPage++;
                    progressBar.setVisibility(View.VISIBLE);
                    callGameHistory(false, false);
                }

                @Override
                public boolean isLastPage() {
                    return isLastpage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            };

            rvGameHistory.addOnScrollListener(mLoadMoreListener);

        } else {
            gameHistoryAdapter.notifyDataSetChanged();
        }

    }
}
