package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.app.anamules.R;
import com.app.anamules.adapter.GolfCourseAdapter;
import com.app.anamules.adapter.GolfRecentCourseAdapter;
import com.app.anamules.adapter.ViewAllGolfAdapter;
import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.been.golfCourse.GolfCourse;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.databinding.ActivityViewallBinding;
import com.app.anamules.listeners.GolfCourseSelection;
import com.app.anamules.listeners.GolfCourseSelectionNear;
import com.app.anamules.singleton.GolfSelection;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewAllGolfCourseActivity extends BaseActivity implements GolfCourseSelectionNear, GolfCourseSelection
{
    ViewAllGolfCourseActivity activity;
    ActivityViewallBinding binding;

    //
    private GolfCourse golfCourseNearme, golfCourseRecenly;
    private List<Datum> listRecentlyPlayed = new ArrayList<>();
    private List<Datum> listNearMe = new ArrayList<>();

    private List<GetRecentCourses> recentCoursesList;
    //
    Datum datum;
    GetRecentCourses getRecentCourses;
    //ViewAllGolfAdapter allGolfAdapter;
    ViewAllGolfAdapter allGolfAdapter;
    GolfRecentCourseAdapter allGolfAdapterRecent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_viewall);
        activity = ViewAllGolfCourseActivity.this;
        init();
    }

    private void init()
    {
        if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED))
        {
            binding.tvTitle.setText(getString(R.string.title_recentlyPlayed));

            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {

                callRecentlyPlayedGolfApi(login.getData().getID());
            }
        }
        else if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME))
        {
            binding.tvTitle.setText(getString(R.string.txt_golfCourseNewrMe));

            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                callApiForNear();
            }
        }
        else
        {
            binding.tvTitle.setVisibility(View.GONE);
        }

        recentCoursesList = new ArrayList<>();

        binding.tvBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Log.e(TAG, "Editable : "+s);
                if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME)) {
                    if (allGolfAdapter != null) {
                        allGolfAdapter.getFilter().filter(s);
                    }
                } else if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED)) {
                    if(allGolfAdapterRecent != null) {
                        allGolfAdapterRecent.getFilter().filter(s);
                    }
                }
            }
        });

        binding.tvDone.setOnClickListener(view -> {

            if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_GOLF_NEAR_ME)) {
                /*GolfCourseSelectionNear golfCourseSelection = GolfSelection.getCallBack();
                if (golfCourseSelection != null) {
                    golfCourseSelection.onSelectedRV("", Integer.MAX_VALUE, datum);
                }*/
                //GolfCourseListActivity.activity.finish();
                //finish();
                Intent intent = new Intent(activity, AddPlayerCourseActivity.class);
                intent.putExtra(Constant.SELECT_TYPE, Constant.FROM_GOLF_NEAR_ME);
                intent.putExtra(Constant.SELECTED_GOLF, datum);
                //setResult(RESULT_OK, intent);
                startActivity(intent);
                finish();
            } else if(getIntent().getStringExtra(Constant.FROM) != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_RECENTLY_PLAYED)) {

                //GetRecentCourses recentPlayerBeen = GolfSelection.getCallBack();


                Intent intent = new Intent(activity, AddPlayerCourseActivity.class);
                intent.putExtra(Constant.SELECT_TYPE, Constant.FROM_RECENTLY_PLAYED);
                intent.putExtra(Constant.SELECTED_GOLF, getRecentCourses);
                //setResult(RESULT_OK, intent);
                startActivity(intent);
                finish();
            }

        });
    }

    private void callRecentlyPlayedGolfApi(String userId)
    {
        Log.e("TAG","gpsTracker.getLatitude() = "+gpsTracker.getLatitude());
        Log.e("TAG","gpsTracker.getLatitude() = "+gpsTracker.getLongitude());
        String url = WebserviceApi.API_GET_RECENT_COURSES + userId;//+ gpsTracker.getLatitude()+"/"+gpsTracker.getLongitude();
        apiCall.callGet(true, url,login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                /*golfCourseRecenly = gson.fromJson(Response, GolfCourse.class);
                if(golfCourseRecenly.getData() != null)
                {listRecentlyPlayed.addAll(golfCourseRecenly.getData());}*/

                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("RecentCources");

                            for(int i = 0 ; i < jsonArray.length(); i++) {
                                GetRecentCourses getRecentCourses = gson.fromJson(jsonArray.get(i).toString(), GetRecentCourses.class);
                                recentCoursesList.add(getRecentCourses);
                            }

                            setRecentlyPlayedAdapter(recentCoursesList);
                            //binding.llRecentlyPlayed.setVisibility(View.VISIBLE);
                        } else {
                            //binding.llRecentlyPlayed.setVisibility(View.GONE);
                        }

                    } else {
                        Log.e(TAG, "ERROR : ");
                    }


                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }


    private void setRecentlyPlayedAdapter(List<GetRecentCourses> listRecentlyPlayed) {
        LinearLayoutManager layoutManager = new GridLayoutManager(activity,2);
        binding.rvViewAll.setLayoutManager(layoutManager);
        allGolfAdapterRecent = new GolfRecentCourseAdapter(activity, listRecentlyPlayed, "grid");
        allGolfAdapterRecent.setClickable(true, Constant.FROM_RECENTLY_PLAYED);
        binding.rvViewAll.setAdapter(allGolfAdapterRecent);
        allGolfAdapterRecent.setListener(this);
    }

    private void setAdapter(List<Datum> listRecentlyPlayed) {
        LinearLayoutManager layoutManager = new GridLayoutManager(activity,2);
        binding.rvViewAll.setLayoutManager(layoutManager);
        allGolfAdapter = new ViewAllGolfAdapter(activity, listRecentlyPlayed);
        //allGolfAdapter = new GolfCourseAdapter(activity, listRecentlyPlayed);
        binding.rvViewAll.setAdapter(allGolfAdapter);
        allGolfAdapter.setListener(this);
    }

    private void callApiForNear() {

        Log.e("TAG","gpsTracker.getLatitude() = "+gpsTracker.getLatitude());
        Log.e("TAG","gpsTracker.getLatitude() = "+gpsTracker.getLongitude());
        String url = WebserviceApi.API_GOLF_COURSE + login.getData().getID() +"/"+ gpsTracker.getLatitude()+"/"+gpsTracker.getLongitude();
        apiCall.callGet(true, url,login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                golfCourseNearme = gson.fromJson(Response, GolfCourse.class);
                if(golfCourseNearme.getData() != null)
                {listNearMe.addAll(golfCourseNearme.getData());}
                setAdapter(listNearMe);
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }



    @Override
    public void onSelectedRV(String From, int position, Datum datum) {
        this.datum = datum;
    }

    @Override
    public void onSelectedRecent(String from, int position, GetRecentCourses getRecentCourses) {
        this.getRecentCourses = getRecentCourses;
    }

   // private void
}
