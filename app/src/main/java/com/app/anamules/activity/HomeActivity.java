package com.app.anamules.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.anamules.R;
import com.app.anamules.adapter.GolfCourseAdapter;
import com.app.anamules.been.GameDataBeen;
import com.app.anamules.been.GetRecentCourses;
import com.app.anamules.been.RecentPlayerBeen;
import com.app.anamules.been.golfCourse.Datum;
import com.app.anamules.been.golfCourse.GolfCourse;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.StartSnapHelper;
import com.app.anamules.databinding.ActivityHomeBinding;
import com.app.anamules.listeners.ApiInterface;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.ApiClient;
import com.app.anamules.webservice.WebserviceApi;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "HomeActivity";
    public ActivityHomeBinding binding;
    public HomeActivity activity;
    public GolfCourseAdapter golfCourseAdapter;
    public BottomSheetBehavior sheetBehavior;
    public LinearLayout layoutBottomsheet;
    public RelativeLayout llGameHistory, llRecentPlayer, llChangePass, llContactUs;
    public LinearLayout llHelp, llLogout;

    private GolfCourse golfCourse;
    private List<Datum> list = new ArrayList<>();
    ApiInterface apiService;

    GameDataBeen gameDataBeen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        activity = HomeActivity.this;

        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        layoutBottomsheet = findViewById(R.id.bottom_sheet);
        llChangePass = findViewById(R.id.llChangePass);
        llGameHistory = findViewById(R.id.llGameHistory);
        llRecentPlayer = findViewById(R.id.llRecentPlayer);
        llContactUs = findViewById(R.id.llContactUs);
        llLogout = findViewById(R.id.llLogout);
        llHelp = findViewById(R.id.llHelp);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomsheet);


        binding.ivSetting.setOnClickListener(view ->
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        llChangePass.setOnClickListener(this);
        llRecentPlayer.setOnClickListener(this);
        llGameHistory.setOnClickListener(this);
        llContactUs.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llHelp.setOnClickListener(this);
        binding.tvPlayGame.setOnClickListener(this);

        binding.tvUserName.setOnClickListener(view -> {
            startActivity(new Intent(activity,ProfileActivity.class));
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });
        binding.ivUserImg.setOnClickListener(view -> {
            startActivity(new Intent(activity,ProfileActivity.class));
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        //setValue();

        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {

            callApiForNear();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume() ");
        refreshDateLogin();
        setValue();
    }

    private void callApiForNear()
    {
        Log.e(TAG,"gpsTracker.getLatitude() = "+gpsTracker.getLatitude());
        Log.e(TAG,"gpsTracker.getLatitude() = "+gpsTracker.getLongitude());

        String url = WebserviceApi.API_GOLF_COURSE + login.getData().getID() +"/"+ gpsTracker.getLatitude()+"/"+gpsTracker.getLongitude();

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    golfCourse = gson.fromJson(Response, GolfCourse.class);

                    if (golfCourse.getData() != null) {
                        list.addAll(golfCourse.getData());
                    }

                    setAdaper();
                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void setAdaper()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        binding.rvGolf.setLayoutManager(linearLayoutManager);
        golfCourseAdapter = new GolfCourseAdapter(activity, list);
        SnapHelper startSnapHelper = new StartSnapHelper();
        startSnapHelper.attachToRecyclerView(binding.rvGolf);
        binding.rvGolf.setAdapter(golfCourseAdapter);
    }

    private void setValue() {
        if(login.getData().getProfilePicture() != null && !login.getData().getProfilePicture().contains("No_Image.png"))
        {
            binding.txtName.setVisibility(View.GONE);
            Picasso.with(activity).load(login.getData().getProfilePicture()).into(binding.ivUserImg);
        } else {
            String ch = login.getData().getFirstName().charAt(0) +""+ login.getData().getLastName().charAt(0);
            binding.txtName.setText(ch.toUpperCase());
            binding.txtName.setVisibility(View.VISIBLE);
            Picasso.with(activity).load("https://www.publicdomainpictures.net/pictures/200000/nahled/plain-gray-background.jpg").into(binding.ivUserImg);
        }
        binding.tvUserName.setText(login.getData().getFirstName()+" "+login.getData().getLastName());
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llHelp:
                callHelp();
                break;

            case R.id.llGameHistory:
                callGameHistory();
                break;

            case R.id.llRecentPlayer:
                callRecentPlayer();
                break;

            case R.id.llChangePass:
                callChangePass();
                break;

            case R.id.llContactUs:
                callContactUs();
                break;

            case R.id.llLogout:
                callLogOut();
                break;

            case R.id.tv_PlayGame:
                callInitialStartGame(login.getData().getID());
                break;
        }
    }

    private void callHelp()
    {
        Log.e(TAG,"callHelp()");

        Intent intent = new Intent(HomeActivity.this, HelpActivity.class);
        intent.putExtra(Constant.NAME, getResources().getString(R.string.txt_help));
        intent.putExtra(Constant.PDF_FILE_NAME, "anamules_game_rules.pdf");
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }

    private void callInitialStartGame(String userId) {
        String url = WebserviceApi.API_GOLF_GAME_INITIAL_START_GAME + userId;

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            String message = jsonObject.getString("message");

                            gameDataBeen = gson.fromJson(jsonObject.getJSONObject("game_data").toString(), GameDataBeen.class);

                            GetRecentCourses getRecentCourses = null;
                            if(jsonObject.has("course_info")) {
                                Session.saveGameCourseDeatil(jsonObject.getString("course_info"), activity);
                                getRecentCourses = gson.fromJson(jsonObject.getJSONObject("course_info").toString(), GetRecentCourses.class);
                            }

                            JSONArray playerListArray = jsonObject.getJSONArray("player_info");

                            ArrayList<RecentPlayerBeen> playerList = new ArrayList<>();

                            for(int i = 0 ; i < playerListArray.length(); i++) {
                                RecentPlayerBeen recentPlayerBeen = gson.fromJson(playerListArray.get(i).toString(), RecentPlayerBeen.class);
                                playerList.add(recentPlayerBeen);
                            }

                            Session.saveGameDeatil(jsonObject.getJSONObject("game_data").toString(), activity);

                            Log.e(TAG, "Responce getGameID : "+gameDataBeen.getID());

                            if(getRecentCourses != null && getRecentCourses.getName() != null) {

                                if (gameDataBeen.getGameStatus().equalsIgnoreCase("pending")) {
                                    Intent intent = new Intent(HomeActivity.this, AddPlayerCourseActivity.class);
                                    intent.putExtra(Constant.PLAYER_LIST, (ArrayList<RecentPlayerBeen>) playerList);
                                    intent.putExtra(Constant.GOLF_COURCE, getRecentCourses);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                } else if (gameDataBeen.getGameStatus().equalsIgnoreCase("playing")) {
                                    Intent intent = new Intent(HomeActivity.this, HoleActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            } else {
                                Intent intent = new Intent(HomeActivity.this, AddPlayerCourseActivity.class);
                                intent.putExtra(Constant.PLAYER_LIST, (ArrayList<RecentPlayerBeen>) playerList);
                                intent.putExtra(Constant.GOLF_COURCE, getRecentCourses);
                                startActivity(intent);
                            }

                        } else {
                            Log.e(TAG, "ERROR : api ");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {

            }
        });

    }

    private void callGameHistory()
    {
        Log.e(TAG,"callGameHistory()");

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(HomeActivity.this, GameHistoryActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        },300);

    }

    private void callRecentPlayer()
    {
        Log.e(TAG,"callRecentPlayer()");

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(HomeActivity.this, RecentPlayerActivity.class);
                intent.putExtra(Constant.IS_SELECT, true);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        },300);
    }

    public void callChangePass()
    {
        Log.e(TAG,"callChangePass()");

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(HomeActivity.this,ChangePasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        },300);
    }

    public void callContactUs()
    {
        Log.e(TAG,"callContactUs()");

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, ContactUsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        },300);
    }

    private void callLogOut()
    {
        Log.e(TAG,"callLogOut()");

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Session.clearUserSession(activity);
                Intent a = new Intent(activity,SigninActivity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(a);
                finish();
            }
        },300);

    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

}
