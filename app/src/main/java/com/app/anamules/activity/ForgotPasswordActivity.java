package com.app.anamules.activity;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.common.SnackbarUtils;
import com.app.anamules.listeners.ApiInterface;
import com.app.anamules.view.CustomEditText;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    public String TAG = "ForgotPasswordActivity";
    public ForgotPasswordActivity activity;

    public LinearLayout llBack,main_content;
    CustomEditText et_email;
    CustomTextView tv_send;
    ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        activity = ForgotPasswordActivity.this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        init();
    }

    private void init()
    {
        Log.e(TAG,"init()");

        llBack = findViewById(R.id.llBack);
        main_content = findViewById(R.id.main_content);
        et_email = findViewById(R.id.et_email);
        tv_send = findViewById(R.id.tv_send);

        llBack.setOnClickListener(this);

        tv_send.setOnClickListener(view -> {

            if(et_email.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(et_email.getText().toString().trim()))
            {
                new SnackbarUtils(main_content,getString(R.string.error_enter_valid_email),
                        ContextCompat.getColor(activity, R.color.colorWhite),
                        ContextCompat.getColor(activity, R.color.colorPrimary),
                        ContextCompat.getColor(activity, R.color.colorPrimary)).snackieBar().show();
                et_email.requestFocus();
                return;
            }
            else
            {

                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    loader.show();
                    Call<Object> call = apiService.callForgotPassword(et_email.getText().toString().trim());

                    Log.e("TAG", "url = " + call.request().url());
                    Log.e("TAG", "url = " + call.request().headers());
                    Log.e("TAG", "url = " + call.request().body().toString());
                    Log.e("TAG", "url = " + call.request().toString());
                    call.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            loader.dismiss();
                            String x = gson.toJson(response.body());
                            Log.e("TAG", "response = " + x);
                            Log.e("TAG", "response = " + response.code());

                            try {
                                JSONObject jsonObject = new JSONObject(x);
                                dialogCommon.showDialog("", jsonObject.getString("message"), "", "Ok", null);
                                et_email.setText("");
                            } catch (Exception e) {
                                Log.e(TAG, "ERROR : " + e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Log.e("TAG", "t = " + t.getMessage());
                            loader.dismiss();
                        }
                    });
                }
            }

        });
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.llBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Log.e(TAG,"onBackPressed()");
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }
}
