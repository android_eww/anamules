package com.app.anamules.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.request.JsonArrayRequest;
import com.app.anamules.R;
import com.app.anamules.adapter.HoleAdapter;
import com.app.anamules.adapter.HoleViewPagerAdapter;
import com.app.anamules.adapter.UserInGameAdapter;
import com.app.anamules.adapter.UserScoreAdapter;
import com.app.anamules.been.AnamulesDetailsBeen;
import com.app.anamules.been.HoleImageBeen;
import com.app.anamules.been.HoleScoreBeen;
import com.app.anamules.been.HoleTeeBoxFilterBeen;
import com.app.anamules.been.HoleTeeboxsBeen;
import com.app.anamules.been.HoleUserAnamulesBeen;
import com.app.anamules.been.PlayerHoleDetail;
import com.app.anamules.been.TeeBoxBeen;
import com.app.anamules.common.Constant;
import com.app.anamules.common.Network;
import com.app.anamules.common.Session;
import com.app.anamules.databinding.ActivityHoleBinding;
import com.app.anamules.listeners.EditListener;
import com.app.anamules.listeners.UserScoreListener;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.ApiCall;
import com.app.anamules.webservice.WebserviceApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

public class HoleActivity extends BaseActivity implements EditListener, View.OnClickListener, UserScoreListener {

    ActivityHoleBinding binding;
    HoleActivity activity;
    List<HoleTeeBoxFilterBeen> holeList = new ArrayList<>();
    HoleAdapter holeAdapter;

    //bottom sheet
    LinearLayout bottom_sheet;
    BottomSheetBehavior sheetBehavior, scoreSheetBehavior;
    CustomTextView tv_Done,tv_count;
    ImageView iv_minus,iv_plus;
    int count = 0;

    //bottom sheet
    RelativeLayout ll_score;
    RecyclerView rv_user,rv_userScore;
    UserInGameAdapter userInGameAdapter;
    UserScoreAdapter userScoreAdapter;

    ArrayList<PlayerHoleDetail> playerHoleDetailsList;
    List<HoleTeeboxsBeen> holeTeeboxsBeenList;
    List<HoleTeeBoxFilterBeen> holeTeeBoxFilterList;
    PlayerHoleDetail editplayerDeatil;
    CustomTextView txt_hole_number;

    List<HoleScoreBeen> scoreHistoryList = new ArrayList<>();

    CustomTextView txt_total_score, txt_anamules;

    int flag = 0;
    private int holeNumber = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hole);
        activity = HoleActivity.this;
        init();
    }

    private void init() {

        holeNumber = getIntent().getIntExtra(Constant.HOLE_NUMBER, 1) - 1;

        iv_minus = findViewById(R.id.iv_minus);
        iv_minus.setOnClickListener(this);
        iv_plus = findViewById(R.id.iv_plus);
        iv_plus.setOnClickListener(this);
        tv_count = findViewById(R.id.tv_count);
        tv_count.setOnClickListener(this);
        tv_Done = findViewById(R.id.tv_Done);
        tv_Done.setOnClickListener(this);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        ll_score = findViewById(R.id.ll_game_score);
        scoreSheetBehavior = BottomSheetBehavior.from(ll_score);
        rv_user = findViewById(R.id.rv_user);
        rv_userScore = findViewById(R.id.rv_userScore);
        txt_hole_number = findViewById(R.id.txt_hole_number);
        txt_total_score = findViewById(R.id.txt_total_score);
        txt_anamules = findViewById(R.id.txt_anamules);

        playerHoleDetailsList = new ArrayList<>();
        holeTeeboxsBeenList = new ArrayList<>();
        holeTeeBoxFilterList = new ArrayList<>();
        count = 0;

        /*holeList.add(new HoleImageBeen("1"));
        holeList.add(new HoleImageBeen("1"));
        holeList.add(new HoleImageBeen("1"));*/

        /*binding.viewPagerHole.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }

        });*/

        binding.viewPagerHole.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
               Log.e(TAG, "addOnPageChangeListener : onPageScrolled");
            }

            @Override
            public void onPageSelected(int i) {
                Log.e(TAG, "addOnPageChangeListener : onPageSelected");
                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                Log.e(TAG, "addOnPageChangeListener : onPagescrollStateChanged");
            }
        });

        binding.leftNav.setOnClickListener(view -> {
            if(binding.viewPagerHole.getCurrentItem() > 0)
            {
                binding.viewPagerHole.setCurrentItem(binding.viewPagerHole.getCurrentItem()-1);
            }
            if(holeList.size() == 1)
            {
                binding.leftNav.setVisibility(View.GONE);
                binding.rightNav.setVisibility(View.GONE);
            }
            else if(binding.viewPagerHole.getCurrentItem() == 0)
            {
                binding.leftNav.setVisibility(View.GONE);
                binding.rightNav.setVisibility(View.VISIBLE);
            }
            else if(holeList.size()-1 == binding.viewPagerHole.getCurrentItem())
            {
                binding.rightNav.setVisibility(View.GONE);
                binding.leftNav.setVisibility(View.VISIBLE);
            }
            else
            {
                binding.rightNav.setVisibility(View.VISIBLE);
            }
        });

        binding.rightNav.setOnClickListener(view -> {
            if(binding.viewPagerHole.getCurrentItem() < holeList.size())
            {
                binding.viewPagerHole.setCurrentItem(binding.viewPagerHole.getCurrentItem()+1);
            }
            if(holeList.size() == 1)
            {
                binding.leftNav.setVisibility(View.GONE);
                binding.rightNav.setVisibility(View.GONE);
            }
            else if(binding.viewPagerHole.getCurrentItem() == holeList.size()-1)
            {
                binding.rightNav.setVisibility(View.GONE);
                binding.leftNav.setVisibility(View.VISIBLE);
            }
            else if(holeList.size() == binding.viewPagerHole.getCurrentItem())
            {
                binding.leftNav.setVisibility(View.GONE);
                binding.rightNav.setVisibility(View.VISIBLE);
            }
            else
            {
                binding.leftNav.setVisibility(View.VISIBLE);
            }
        });


        binding.llScore.setOnClickListener(view -> {

            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                scoreSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                if(playerHoleDetailsList != null) {
                    if(playerHoleDetailsList.size() > 0) {
                        callScoreHistory(playerHoleDetailsList.get(0).getGameID(), playerHoleDetailsList.get(0).getID());
                    }
                }
            }

        });


        binding.tvCurrentZoo.setOnClickListener(view -> {
            Intent intent = new Intent(activity, CurrentZooActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_CURRENT_ZOO);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.tvEndRound.setOnClickListener(view -> {


            //callEndGame(login.getData().getID(), Session.getCurrentGame(HoleActivity.this).getID());

            if(holeTeeBoxFilterList.size() == binding.viewPagerHole.getCurrentItem()+1) {
                /*Intent intent = new Intent(activity, ResultActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                finish();*/

                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    binding.viewPagerHole.setCurrentItem(0);
                    callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                    binding.tvBack.setText("Previous Hole");
                }

            } else {

                if(!Network.isNetwork(activity)) {
                    showInternetDialog();
                } else {
                    binding.viewPagerHole.setCurrentItem(binding.viewPagerHole.getCurrentItem() + 1);
                    callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());

                    binding.tvBack.setText("Previous Hole");
                }

                /*if (holeTeeBoxFilterList.size() == binding.viewPagerHole.getCurrentItem()+1) {
                    binding.tvEndRound.setText("End Round");
                }*/


            }
        });

        binding.llReset.setOnClickListener(view -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Do you want to reset ?");
            alertDialogBuilder.setMessage("All Anamules will return to the Zoo for all players");
            alertDialogBuilder.setPositiveButton("Yes, Reset Now", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(!Network.isNetwork(activity)) {
                        showInternetDialog();
                    } else {
                        callResetGame(Session.getCurrentGame(activity).getID());
                    }
                    //callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                }
            });
            alertDialogBuilder.setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        });

        binding.tvHelp.setOnClickListener(view -> {
            Intent intent = new Intent(activity, HelpActivity.class);
            intent.putExtra(Constant.NAME, getResources().getString(R.string.txt_help));
            intent.putExtra(Constant.PDF_FILE_NAME, "anamules_game_rules.pdf");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.tvBack.setOnClickListener(view -> {

            if(!Network.isNetwork(activity)) {
               showInternetDialog();
            } else {
                if (binding.viewPagerHole.getCurrentItem() == 0) {

                    //showBackEndGameDialgo();
                    binding.viewPagerHole.setCurrentItem(holeTeeBoxFilterList.size() - 1);
                    callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                } else {
                    binding.viewPagerHole.setCurrentItem(binding.viewPagerHole.getCurrentItem() - 1);
                    callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                    binding.tvEndRound.setText(getResources().getString(R.string.txt_next_hole));

                /*if(binding.viewPagerHole.getCurrentItem() == 0) {
                    binding.tvBack.setText("Back");
                }*/
                }
            }
        });

        binding.tvEndRoundOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ResultActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                //finish();
                //callEndGame(login.getData().getID(), Session.getCurrentGame(activity).getID(), true);
            }
        });

        if(Session.getGameCourseDatail(activity) != null) {
            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                GolfScoreCard(Session.getGameCourseDatail(activity).getID());
            }

        }

       // setScoreBottomSheet();

        //callTotalUpdatedScoredHoleWise("1", Session.getCurrentGame(activity).getID());
        //GolfScoreCard(Session.getGameCourseDatail(activity).getGolfbertID());
    }



    private void setScoreBottomSheet() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        rv_user.setLayoutManager(linearLayoutManager);
        playerHoleDetailsList.get(0).setSelect(true);
        userInGameAdapter = new UserInGameAdapter(activity, playerHoleDetailsList, this::onClickUser);
        rv_user.setAdapter(userInGameAdapter);

        //callScoreHistory(playerHoleDetailsList.get(0).getGameID(), playerHoleDetailsList.get(0).getID());

        //rv_userScore.setNestedScrollingEnabled(false);
    }

    private void setPlayerScore() {
        RecyclerView.LayoutManager linearLayoutManager2 = new LinearLayoutManager(activity);
        rv_userScore.setLayoutManager(linearLayoutManager2);
        userScoreAdapter = new UserScoreAdapter(activity, scoreHistoryList);
        rv_userScore.setAdapter(userScoreAdapter);
    }

    @Override
    public void onClickEdit(int position)
    {
        editplayerDeatil = playerHoleDetailsList.get(position);
        //sdfasdf
        count = Integer.parseInt(playerHoleDetailsList.get(position).getCurrent_hole_score().getScore());
        tv_count.setText(count+"");
        txt_hole_number.setText("Hole "+holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber()+" Score");
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    @Override
    public void onBackPressed() {
        /*if(sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }
        else if(scoreSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
        {
            scoreSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }*/
        super.onBackPressed();
    }

    @Override
    public void onClick(View view)
    {
        if(view == tv_Done)
        {
            //if(count != 0) {
            if(!Network.isNetwork(activity)) {
                showInternetDialog();
            } else {
                if (editplayerDeatil != null) {

                    //callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                    callUpdateScore(editplayerDeatil.getID(), holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), count + "");
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                }
            }
            /*} else {
                Toast.makeText(activity, "Please add Score", Toast.LENGTH_SHORT).show();
            }*/
        }
        else if(view == iv_minus)
        {
            if(count != 0)
            {
                count--;
                tv_count.setText(count+"");
            }
        }
        else if(view == iv_plus)
        {
            count++;
            tv_count.setText(count+"");
        }
    }

    private void callResetGame(String GameID) {
        String url = WebserviceApi.API_ANAMULES_RESET_ANAMULES + GameID;

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            Log.e(TAG, "Message :: "+ jsonObject.getString("message"));

                            if(!Network.isNetwork(activity)) {
                                showInternetDialog();
                            } else {
                                callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
                            }

                        } else {
                            Log.e(TAG, "ERROR : API ");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e);
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });

    }

    private void callUpdateScore(String playerId, String HoleId, String Score) {
        String url = WebserviceApi.API_GOLF_COUSE_UPDATE_SCORE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_PLAYER_ID, playerId);
        param.put(WebserviceApi.KEY_HOLE_ID, HoleId);
        param.put(WebserviceApi.KEY_SCORE, Score);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            Log.e(TAG, "SCORE UPDATE SUSSESSFULL");
                            //count = 0;

                            tv_count.setText(count+"");

                            if(!Network.isNetwork(activity)) {
                                showInternetDialog();
                            } else {
                                callTotalUpdatedScoredHoleWise(HoleId, Session.getCurrentGame(activity).getID());
                            }
                        } else {
                            Log.e(TAG, "ERROR : ");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });

    }

    private void callEndGame(String OwnerUserID, String gameID, boolean isResult) {

        String url = WebserviceApi.API_GOLF_GAME_END;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_OWNER_USER_ID, OwnerUserID);
        param.put(WebserviceApi.KEY_GAME_ID, gameID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            String barcode = jsonObject.getString("barcode");
                            String message = jsonObject.getString("message");

                            if(isResult) {

                                startActivity(new Intent(activity, ResultActivity.class));
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            } else {

                                onBackPressed();
                            }
                            //finish();

                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }

    private void callTotalUpdatedScoredHoleWise(String HoleID, String GameID) {

        String url = WebserviceApi.API_GOLF_COURSE_TOTAL_UPDATED_SCORE_HOLEWISE;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_HOLE_ID, HoleID);
        param.put(WebserviceApi.KEY_GAME_ID, GameID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            playerHoleDetailsList.clear();

                            JSONArray jsonArray = jsonObject.getJSONArray("player");

                            for(int i = 0 ; i < jsonArray.length(); i++ ) {
                                PlayerHoleDetail playerHoleDetail = gson.fromJson(jsonArray.getJSONObject(i).toString(), PlayerHoleDetail.class);
                                playerHoleDetailsList.add(playerHoleDetail);
                            }

                            setHolePlayerAdapter();
                            setScoreBottomSheet();

                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {
            if (flag != 0) {
                callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
            }
            flag++;
        }
        //callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());

    }

    private void GolfScoreCard(String courseID) {
        String url = WebserviceApi.API_GOLF_COURSE_SCORE_CARD + courseID;

        apiCall.callGet(true, url, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            JSONObject jsonData = jsonObject.getJSONObject("data");

                            String courseID = jsonData.getString("courseid");

                            JSONArray jsonArray = jsonData.getJSONArray("holeteeboxes");

                            for(int i = 0 ;i < jsonArray.length(); i++) {
                                HoleTeeboxsBeen holeTeeboxsBeen = gson.fromJson(jsonArray.get(i).toString(), HoleTeeboxsBeen.class);
                                holeTeeboxsBeenList.add(holeTeeboxsBeen);
                            }

                            Log.e(TAG, "GolfScoreCard : "+holeTeeboxsBeenList.size());

                            holeCardFilter();

                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERRROR : "+error);
            }
        });
    }

    private void setHolePlayerAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        binding.rvHole.setLayoutManager(linearLayoutManager);
        holeAdapter = new HoleAdapter(activity,this, playerHoleDetailsList, holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber());
        binding.rvHole.setAdapter(holeAdapter);
    }

    private void holeCardFilter() {

        for(int i = 0 ; i < holeTeeboxsBeenList.size(); i++) {
            if(holeTeeBoxFilterList.size() == 0) {
                setValueHoleTeeBoxFilter(holeTeeboxsBeenList.get(i));
            } else {
                boolean flag = true;
                for(int j = 0 ; j < holeTeeBoxFilterList.size(); j++) {
                    if(holeTeeBoxFilterList.get(j).getHoleid().equalsIgnoreCase(holeTeeboxsBeenList.get(i).getHoleid())) {
                        holeTeeBoxFilterList.get(j).getTeeBoxList().add(new TeeBoxBeen(holeTeeboxsBeenList.get(i).getColor(), holeTeeboxsBeenList.get(i).getLength(), holeTeeboxsBeenList.get(i).getTeeboxtype()));
                        flag = false;
                        break;
                    }
                }

                if(flag) {
                    setValueHoleTeeBoxFilter(holeTeeboxsBeenList.get(i));
                }
            }
        }

        Log.e(TAG, "holeCardFilter() : "+holeTeeBoxFilterList.size());

        HoleViewPagerAdapter holePagerAdapter = new HoleViewPagerAdapter(activity, holeTeeBoxFilterList, Session.getGameCourseDatail(activity).getName());
        binding.viewPagerHole.setAdapter(holePagerAdapter);

        binding.viewPagerHole.setCurrentItem(holeNumber);

        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {

            callTotalUpdatedScoredHoleWise(holeTeeBoxFilterList.get(binding.viewPagerHole.getCurrentItem()).getHolenumber(), Session.getCurrentGame(activity).getID());
        }

    }

    private void setValueHoleTeeBoxFilter(HoleTeeboxsBeen holeTeeboxsBeen) {
        HoleTeeBoxFilterBeen holeTeeBoxFilterBeen = new HoleTeeBoxFilterBeen();
        holeTeeBoxFilterBeen.setHoleid(holeTeeboxsBeen.getHoleid());
        holeTeeBoxFilterBeen.setHandicap(holeTeeboxsBeen.getHandicap());
        holeTeeBoxFilterBeen.setHolenumber(holeTeeboxsBeen.getHolenumber());
        holeTeeBoxFilterBeen.setPar(holeTeeboxsBeen.getPar());
        holeTeeBoxFilterBeen.getTeeBoxList().add(new TeeBoxBeen(holeTeeboxsBeen.getColor(), holeTeeboxsBeen.getLength(), holeTeeboxsBeen.getTeeboxtype()));
        holeTeeBoxFilterList.add(holeTeeBoxFilterBeen);
    }

    private void callScoreHistory(String gameID, String playerID) {
        String url = WebserviceApi.API_SCORE_SCORE_CARD;

        HashMap<String, String> param = new HashMap<>();
        param.put(WebserviceApi.KEY_GAME_ID, gameID);
        param.put(WebserviceApi.KEY_PLAYER_ID, playerID);

        apiCall.callPost(true, url, param, login.getKey(), new ApiCall.GetResponce() {
            @Override
            public void GetResponce(String Response) {
                try {

                    JSONObject jsonObject = new JSONObject(Response);

                    if(jsonObject.has("status")) {

                        if(jsonObject.getBoolean("status")) {

                            scoreHistoryList.clear();

                            String TotalScore = jsonObject.getString("TotalScore");
                            String TotalAnamules =jsonObject.getString("TotalAnamules");

                            txt_anamules.setText("Anamules "+TotalAnamules);
                            txt_total_score.setText(TotalScore);

                            JSONObject dataJson = jsonObject.getJSONObject("data");

                            if(dataJson.has("Hole-1")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-1");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(0).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(0).getPar());

                                holeScoreBeen1.setHoleNumber("1");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-2")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-2");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(1).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(1).getPar());

                                holeScoreBeen1.setHoleNumber("2");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-3")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-3");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(2).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(2).getPar());

                                holeScoreBeen1.setHoleNumber("3");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-4")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-4");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(3).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(3).getPar());

                                holeScoreBeen1.setHoleNumber("4");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-5")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-5");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(4).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(4).getPar());

                                holeScoreBeen1.setHoleNumber("5");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-6")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-6");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(5).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(5).getPar());

                                holeScoreBeen1.setHoleNumber("6");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-7")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-7");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(6).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(6).getPar());

                                holeScoreBeen1.setHoleNumber("7");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-8")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-8");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(7).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(7).getPar());

                                holeScoreBeen1.setHoleNumber("8");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-9")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-9");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(8).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(8).getPar());

                                holeScoreBeen1.setHoleNumber("9");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-10")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-10");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(9).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(9).getPar());

                                holeScoreBeen1.setHoleNumber("10");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-11")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-11");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(10).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(10).getPar());

                                holeScoreBeen1.setHoleNumber("11");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-12")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-12");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(11).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(11).getPar());

                                holeScoreBeen1.setHoleNumber("12");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-13")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-13");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(12).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(12).getPar());

                                holeScoreBeen1.setHoleNumber("13");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-14")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-14");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(13).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(13).getPar());

                                holeScoreBeen1.setHoleNumber("14");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-15")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-15");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(14).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(14).getPar());

                                holeScoreBeen1.setHoleNumber("15");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-16")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-16");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(15).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(15).getPar());

                                holeScoreBeen1.setHoleNumber("16");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-17")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-17");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(16).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(16).getPar());

                                holeScoreBeen1.setHoleNumber("17");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }

                            if(dataJson.has("Hole-18")) {

                                HoleScoreBeen holeScoreBeen1 = new HoleScoreBeen();

                                JSONObject jsonObjectHole = dataJson.getJSONObject("Hole-18");

                                holeScoreBeen1.setHcp(holeTeeBoxFilterList.get(17).getHandicap());
                                holeScoreBeen1.setPar(holeTeeBoxFilterList.get(17).getPar());

                                holeScoreBeen1.setHoleNumber("18");
                                holeScoreBeen1.setScore(jsonObjectHole.getString("score"));

                                if(jsonObjectHole.has("+")) {
                                    JSONArray pluesArray = jsonObjectHole.getJSONArray("+");

                                    for(int i = 0 ; i < pluesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(pluesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getPluse().add(anamulesDetailsBeen);
                                    }
                                }

                                if(jsonObjectHole.has("-")) {
                                    JSONArray minesArray = jsonObjectHole.getJSONArray("-");

                                    for(int i = 0 ; i < minesArray.length(); i++) {
                                        HoleUserAnamulesBeen anamulesDetailsBeen = gson.fromJson(minesArray.get(i).toString(), HoleUserAnamulesBeen.class);
                                        holeScoreBeen1.getMines().add(anamulesDetailsBeen);
                                    }
                                }

                                scoreHistoryList.add(holeScoreBeen1);

                            }


                            setPlayerScore();


                        } else {
                            Log.e(TAG, "ERROR : API");
                        }

                    } else {
                        Log.e(TAG, "ERROR : status not found");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "ERROR : "+e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "ERROR : "+error);
            }
        });
    }


    @Override
    public void onClickUser(String playerID) {
        if(!Network.isNetwork(activity)) {
            showInternetDialog();
        } else {
            callScoreHistory(playerHoleDetailsList.get(0).getGameID(), playerID);
            userScoreAdapter.notifyDataSetChanged();
            userInGameAdapter.notifyDataSetChanged();
        }
    }

    private void showBackEndGameDialgo() {

        AlertDialog.Builder builder = new AlertDialog.Builder(HoleActivity.this);


        builder.setTitle(getResources().getString(R.string.app_name));

        builder.setMessage(getResources().getString(R.string.alert_message));

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callEndGame(login.getData().getID(), Session.getCurrentGame(activity).getID(), false);
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();

    }

}
