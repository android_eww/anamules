package com.app.anamules.notification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;


import com.app.anamules.R;
import com.app.anamules.activity.HomeActivity;
import com.app.anamules.common.Constant;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;




public class NotificationUtils
{
    private static String TAG = NotificationUtils.class.getSimpleName();

    String CHANNEL_ID = "Hey";
    private Context mContext;

    public static int bundleNotificationId = 100;
    public static int singleNotificationId = 100;
    NotificationManager notificationManager;
    NotificationCompat.Builder summaryNotificationBuilder_;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
        notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void showNotificationMessage(String title, String message, String timeStamp, Intent intent)
    {
        showNotificationMessage(title, message, timeStamp, intent, null);
    }

    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl)
    {
        // Check for empty push message
        /*if (TextUtils.isEmpty(message))
            return;*/


        // notification icon
        final int icon = R.drawable.logo;//R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/raw/echo_ringtone");

        if (!TextUtils.isEmpty(imageUrl))
        {
            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches())
            {
                Bitmap bitmap = getBitmapFromURL(imageUrl);
                if (bitmap != null)
                {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                }
                else
                {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                }
            }
        }
        else
        {
            createGroupNotification(title,message,icon,intent);
            //showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
            playNotificationSound();
        }
    }


    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Constant.NOTIFICATION_ID, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Constant.NOTIFICATION_ID_BIG_IMAGE, notification);
    }

    private void createGroupNotification(String tital,String msg,int icon, Intent intent)
    {
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();

        Intent resultIntent = new Intent(mContext, HomeActivity.class);
        resultIntent.putExtra("notification", "Summary Notification Clicked");
        resultIntent.putExtra("notification_id", bundleNotificationId);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, bundleNotificationId, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        String bundle_notification_id = "bundle_notification_" + bundleNotificationId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            if (notificationManager.getNotificationChannels().size() < 2) {
                NotificationChannel groupChannel = new NotificationChannel("bundle_channel_id", "bundle_channel_name", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(groupChannel);
                NotificationChannel channel = new NotificationChannel("channel_id", "channel_name", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (singleNotificationId == bundleNotificationId)
        {
            singleNotificationId = bundleNotificationId + 1;
        }
        else {
            singleNotificationId++;
        }

        NotificationCompat.InboxStyle inboxStyle1 = new NotificationCompat.InboxStyle();
        inboxStyle1.addLine(msg);

        NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
        textStyle.bigText(msg);


        NotificationCompat.Builder summaryNotificationBuilder = new NotificationCompat.Builder(mContext, "bundle_channel_id")
                .setSmallIcon(icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.logo))
                .setGroup(bundle_notification_id)
                .setGroupSummary(true)
                .setContentTitle(tital)
                .setContentText(msg)
                //.setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setVibrate(new long[] {1, 1, 1})
                .setStyle(textStyle);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(mContext, "channel_id")
                .setSmallIcon(icon)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.logo))
                .setGroup(bundle_notification_id)
                .setContentTitle("New Notification " + singleNotificationId)
                .setContentText("Content for the notification")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroupSummary(false)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(getBitmapFromURL("https://images.all-free-download.com/images/graphicthumb/small_mouse_macro_515329.jpg")));


        notificationManager.notify(bundleNotificationId, summaryNotificationBuilder.build());
        /*notificationManager.notify(singleNotificationId, summaryNotificationBuilder.build());
        notificationManager.notify(bundleNotificationId, summaryNotificationBuilder_.build());*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {

            String GROUP_KEY_WORK_EMAIL = mContext.getApplicationContext().getPackageName();
            Log.e("TAG","GROUP_KEY_WORK_EMAIL = "+GROUP_KEY_WORK_EMAIL);

            CharSequence channelName = "All Notificaton";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification mNotificationBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                    .setSmallIcon(icon)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.app_icon))
                    .setContentTitle("GGGG")
                    .setContentText("GMM")
                    //.setDefaults(Notification.DEFAULT_ALL)
                    //.setAutoCancel(true)
                    //.setColor(ContextCompat.getColor(mContext, R.color.white))
                    *//*.setStyle(new NotificationCompat.InboxStyle()
                            .addLine("Alex Faarborg  Check this out")
                            .addLine("Jeff Chang    Launch Party"))*//*
                            //.setBigContentTitle("2 new messages")
                            //.setSummaryText("janedoe@example.com"))
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setContentIntent(contentIntent)
                    .setGroupSummary(true)
                    .setChannelId(CHANNEL_ID)
                    .build();

            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;
            notificationManager.notify(m, mNotificationBuilder);
            //notificationManager.createNotificationChannelGroup(new NotificationChannelGroup(groupId,groupName));
            //notificationManager.notify(SUMMARY_ID, mNotificationBuilder);

        }
        else
        {
            String GROUP_KEY_WORK_EMAIL = mContext.getApplicationContext().getPackageName();
            Log.e("TAG","GROUP_KEY_WORK_EMAIL = "+GROUP_KEY_WORK_EMAIL);

            Notification newMessageNotification = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                    .setSmallIcon(icon)
                    .setContentTitle("Hey")
                    .setContentText("Hi mayur")
                    //.setLargeIcon()
                    //build summary info into InboxStyle template
                    .setStyle(new NotificationCompat.InboxStyle()
                            .addLine("Alex Faarborg  Check this out")
                            .addLine("Jeff Chang    Launch Party")
                            .setBigContentTitle("2 new messages")
                            .setSummaryText("janedoe@example.com"))
                    //specify which group this notification belongs to
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    //set this notification as the summary for the group
                    .setGroupSummary(true)
                    .build();

            notificationManager.notify(NOTIFICATION_ID, newMessageNotification);
            //notificationManager.notify(SUMMARY_ID, newMessageNotification);
        }*/
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL)
    {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Playing notification sound
    public void playNotificationSound()
    {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/raw/echo_ringtone");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context)
    {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context)
    {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
