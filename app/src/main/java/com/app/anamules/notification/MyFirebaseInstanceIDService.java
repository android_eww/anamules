package com.app.anamules.notification;

/**
 * Created by Android Traine on 08-03-2018.
 */

import android.util.Log;

import com.app.anamules.common.Session;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("TAG", "Refreshed token: " + refreshedToken);

        Session.saveUserSession(Session.USER_PREFERENCE_KEY_TOKEN, refreshedToken,this);
    }

}
