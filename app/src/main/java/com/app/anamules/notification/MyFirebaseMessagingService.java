package com.app.anamules.notification;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.app.anamules.R;
import com.app.anamules.activity.BaseActivity;
import com.app.anamules.activity.HomeActivity;
import com.app.anamules.activity.SigninActivity;
import com.app.anamules.common.Constant;

import com.app.anamules.common.Session;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Android Traine on 08-03-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        if (remoteMessage!=null)
        {
            Map<String, String> data = remoteMessage.getData();

            Log.e("TAG", "onMessageReceived "+ remoteMessage.getData().toString());

            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null)
            {
                Log.e("TAG", "Notification Body: " + remoteMessage.getNotification().getBody());
                handleNotification(remoteMessage.getNotification().getBody());
            }

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0)
            {
                Log.e("TAG", "Data Payload: " + remoteMessage.getData().toString());

                try {
                    String p  = remoteMessage.getData().get("body");
                    JSONObject json = new JSONObject(p);
                    handleDataMessage(json,remoteMessage);
                } catch (Exception e) {
                    Log.e("TAG", "Exception: " + e.getMessage());
                }
            }

        }
        else
        {
            Log.e("TAG","onMessageReceived remoteMessage null");
        }

    }

    private void handleNotification(String message)
    {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext()))
        {
            Log.e("TAG","if");
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Constant.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }
        else
        {
            Log.e("TAG","else");
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json, RemoteMessage remoteMessage)
    {
        Log.e("TAG", "push json: " + json.toString());

        ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        Log.e("TAG","cn.getClassName() = "+cn.getClassName());
        //Context actvitiyContext = this.getApplicationContext();
        //Activity activity = (Activity) actvitiyContext;

        try
        {
            Intent resultIntent = new Intent(this, HomeActivity.class);


            String title="",message="",imageUrl="",timestamp="";
            boolean isBackground =false;

            String type = remoteMessage.getData().get("type");
            if(type.equalsIgnoreCase("order_picked"))
            {
                String cusId = json.getString("customer_id");
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("order_notification"))
            {
                title = "ClickLunch Customer";
                message = json.getString("message");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("order_accepted"))
            {
                //String cusId = json.getString("customer_id");
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("delivery_boy_assigned"))
            {
                String cusId = json.getString("customer_id");
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("order_rejected"))
            {
                //String cusId = json.getString("customer_id");
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("order_completed"))
            {
                String cusId = json.getString("customer_id");
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("general_notification"))
            {
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("CancelByShop"))
            {
                message = json.getString(" ");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("msg_from_vendor_dispatcher"))
            {
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("order_placed"))
            {
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("shop_notification"))
            {
                message = json.getString("message");
                title = remoteMessage.getData().get("title");
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else if(type.equalsIgnoreCase("silent"))
            {
                Session.clearUserSession(getApplicationContext());
                if(NotificationUtils.isAppIsInBackground(BaseActivity.activity))
                {
                    System.runFinalizersOnExit(true);
                }
                //android.os.Process.killProcess(android.os.Process.myPid());
                BaseActivity.activity.runOnUiThread(new Runnable() {
                    public void run() {

                        Intent intent = new Intent(BaseActivity.activity, SigninActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        BaseActivity.activity.startActivity(intent);
                        BaseActivity.activity.overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                    }
                });

            }


            Log.e("TAG", "title: " + title);
            Log.e("TAG", "message: " + message);
            Log.e("TAG", "isBackground: " + isBackground);
            Log.e("TAG", "imageUrl: " + imageUrl);
            Log.e("TAG", "timestamp: " + timestamp);


            /*if (!NotificationUtils.isAppIsInBackground(getApplicationContext()))
            {
                Log.e("TAG", "App in Foreground: ");
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Constant.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                resultIntent.putExtra("message", message);
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }
            else
            {
                Log.e("TAG", "App in Background: ");
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                resultIntent.putExtra("message", message);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl))
                {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                }
                else
                {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }*/

            /*Intent resultIntent = new Intent(this, NotificationActivity.class);
            resultIntent.putExtra("message", message);
            if (TextUtils.isEmpty(imageUrl))
            {
                showNotificationMessage(this, title, message, timestamp, resultIntent);
            }
            else
            {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(this, title, message, timestamp, resultIntent, imageUrl);
            }*/

        }
        catch (Exception e)
        {
            Log.e("TAG", "Json Exception: " + e.getMessage());
        }

    }



    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent)
    {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    @Override
    public void onNewToken(String token) {
        Log.d("TAG", "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Session.saveUserSession(Session.USER_PREFERENCE_KEY_TOKEN,token,this);
    }

}
