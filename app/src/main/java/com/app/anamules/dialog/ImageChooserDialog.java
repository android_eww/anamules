package com.app.anamules.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.anamules.R;
import com.app.anamules.listeners.CallbackClickListner;


public class ImageChooserDialog
{
    Context context;
    private CallbackClickListner callbackClickListner;

    public ImageChooserDialog(Context context, CallbackClickListner callbackClickListner) {
        this.context=context;
        this.callbackClickListner=callbackClickListner;
        openDialog();
    }

    private void openDialog()
    {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setIcon(R.drawable.logo);
        builderSingle.setTitle("Choose Image");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Camera");
        arrayAdapter.add("Gallery");

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                if(strName.equalsIgnoreCase("Camera"))
                {
                    callbackClickListner.onClickListener(0);
                    dialog.dismiss();
                }
                else if(strName.equalsIgnoreCase("Gallery"))
                {
                    callbackClickListner.onClickListener(1);
                    dialog.dismiss();
                }
            }
        });
        builderSingle.show();


        /*final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_camera);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);


        TextView tv_gallery = dialog.findViewById(R.id.dialog_gallery_message);
        TextView tv_camera = dialog.findViewById(R.id.dialog_camera_message);
        ImageView dialog_close = dialog.findViewById(R.id.dialog_close);

        dialog.show();

        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callbackClickListner.onClickListener(view,1);
                dialog.cancel();
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callbackClickListner.onClickListener(view,0);
                dialog.cancel();
            }
        });*/
    }
}
