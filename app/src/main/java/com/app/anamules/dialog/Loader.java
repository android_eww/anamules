package com.app.anamules.dialog;

import android.app.Dialog;
import android.content.Context;

import com.airbnb.lottie.LottieAnimationView;
import com.app.anamules.R;

public class Loader
{
    Dialog dialog;
    LottieAnimationView btn_like;

    public Loader(Context context)
    {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_loader);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        btn_like = dialog.findViewById(R.id.btn_like);
    }

    public void show()
    {
        dialog.show();
        btn_like.playAnimation();
    }


    public void dismiss()
    {
        if(dialog.isShowing())
        {
            dialog.dismiss();
            btn_like.cancelAnimation();
        }
    }
}
