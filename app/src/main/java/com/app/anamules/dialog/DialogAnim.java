package com.app.anamules.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.anamules.R;
import com.app.anamules.common.BlurBuilder;
import com.app.anamules.listeners.TackAnamuleListener;
import com.app.anamules.listeners.onCancelSelectAnamules;
import com.app.anamules.view.CustomTextView;
import com.app.anamules.webservice.WebserviceApi;

import com.jgabrielfreitas.core.BlurImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


public class DialogAnim
{
    Dialog dialog;
    Context context;
    ImageView iv_close, iv_animal;
    CustomTextView txt_name, txt_des, tv_giveAnimal;
    BlurImageView iv_bg;

    LinearLayout ll_main;
    TackAnamuleListener listener;
    String id;
    onCancelSelectAnamules cancelSelectAnamules;
    int postion = 0;

    public DialogAnim(Context context)
    {
        this.context = context;
        //dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_take_anim);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        ll_main = dialog.findViewById(R.id.ll_main);
        iv_close = dialog.findViewById(R.id.iv_close);
        iv_bg = dialog.findViewById(R.id.iv_bg);
        txt_name = dialog.findViewById(R.id.txt_name);
        txt_des = dialog.findViewById(R.id.txt_des);
        iv_animal = dialog.findViewById(R.id.iv_animal);
        tv_giveAnimal = dialog.findViewById(R.id.tv_giveAnimal);

        iv_bg.setBlur(25);

        /*Glide.with(context).load(R.drawable.bg_tran_white)
                .bitmapTransform(new BlurTransformation(context))
                .into(iv_bg);*/

        /*Bitmap resultBmp = BlurBuilder.blur(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_tran_white));
        iv_bg.setImageBitmap(resultBmp);*/

        iv_close.setOnClickListener(view -> {
            dialog.dismiss();
        });

        tv_giveAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    public DialogAnim(Context context, TackAnamuleListener listener)
    {
        this.context = context;
        dialog = new Dialog(context); //,android.R.style.Theme_Black_NoTitleBar_Fullscreen
        dialog.setContentView(R.layout.dialog_take_anim);
        //dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.listener = listener;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        ll_main = dialog.findViewById(R.id.ll_main);
        iv_close = dialog.findViewById(R.id.iv_close);
        iv_bg = dialog.findViewById(R.id.iv_bg);
        txt_name = dialog.findViewById(R.id.txt_name);
        txt_des = dialog.findViewById(R.id.txt_des);
        iv_animal = dialog.findViewById(R.id.iv_animal);
        tv_giveAnimal = dialog.findViewById(R.id.tv_giveAnimal);

        iv_close.setOnClickListener(view -> {
            cancelSelectAnamules.onClickClose(postion);
            dialog.dismiss();
        });

        tv_giveAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                /*if(listener != null) {
                    if(id != null) {
                        listener.onClickTackAnamule(id);
                    }
                }*/

            }
        });

    }

    public void setValue(String image, String name, String des, String id) {
        Picasso.with(context).load(image).into(iv_animal);

        txt_name.setText(name);
        txt_des.setText(des);
        this.id = id;
    }

    public void setValue(String image, String name, String des, String id, int postion, onCancelSelectAnamules cancelSelectAnamules) {
        Picasso.with(context).load(image).into(iv_animal);

        txt_name.setText(name);
        txt_des.setText(des);
        this.id = id;
        this.cancelSelectAnamules = cancelSelectAnamules;
        this.postion = postion;
    }

    public void show()
    {
        dialog.show();
    }


}
