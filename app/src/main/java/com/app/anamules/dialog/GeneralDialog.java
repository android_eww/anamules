package com.app.anamules.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;

import com.app.anamules.R;
import com.app.anamules.listeners.DialogClikListener;

public class GeneralDialog extends AlertDialog.Builder
{
    Context context;

    public GeneralDialog(@NonNull Context context)
    {
        super(context);
        this.context = context;
    }

    public void showDialog(String title, String message, String textNoBtn, String textYesBtn, DialogClikListener  dialogClikListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomDialogTheme);
        builder.setCancelable(false);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(textYesBtn,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(dialogClikListener != null)
                        {
                            dialogClikListener.clikOnPositive();
                        }
                        dialog.dismiss();
                    }
                });


        builder.setNegativeButton(textNoBtn,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(dialogClikListener != null)
                        {
                            dialogClikListener.clikOnNegative();
                        }
                        dialog.dismiss();
                    }
                });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorBlack));
            }
        });

        dialog.show();
    }
}
