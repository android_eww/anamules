package com.app.anamules.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.app.anamules.been.GameDataBeen;
import com.app.anamules.been.GolfCourseBeen;
import com.google.android.gms.common.internal.service.Common;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Session
{
    public static String PREFRENCE_USER = "USER_DETAIL";
    public static String CURRENT_GAME_DEATAIL = "current_game_detail";

    public static String USER_PREFERENCE_KEY_TOKEN = "deviceToken";
    public static String USER_PREFERENCE_LOGIN = "login";

    public static String COURCE_DEATIL = "COURCE_DEATIL";

    public static void saveUserSession(String key, String value, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    // Get Data's from SharedPreferences
    public static String getUserSession(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFRENCE_USER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void clearUserSession(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }

    public static void saveGameDeatil(String value, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(CURRENT_GAME_DEATAIL, Activity.MODE_PRIVATE).edit();
        editor.putString(Constant.GAME_DETAIL, value);
        editor.commit();
    }

    public static GameDataBeen getCurrentGame(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(CURRENT_GAME_DEATAIL, Activity.MODE_PRIVATE);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(prefs.getString(Constant.GAME_DETAIL, ""), GameDataBeen.class);
    }

    public static void saveGameCourseDeatil(String value, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(CURRENT_GAME_DEATAIL, Activity.MODE_PRIVATE).edit();
        editor.putString(COURCE_DEATIL, value);
        editor.commit();
    }

    public static GolfCourseBeen getGameCourseDatail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(CURRENT_GAME_DEATAIL, Activity.MODE_PRIVATE);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(prefs.getString(COURCE_DEATIL, ""), GolfCourseBeen.class);
    }

    public static void clearGameSesstion(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(CURRENT_GAME_DEATAIL, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }
}
