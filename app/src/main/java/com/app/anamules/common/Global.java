package com.app.anamules.common;

import android.util.Log;


import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Global {

    private static String TAG  = "Global";

    private static String serverDateFormat = "yyyy-MM-dd HH:mm:ss";//2019-09-09 01:15:24
    private static String displayDateFormat = "MMMM dd, yyyy";

    public static String getDateFormated(String date){
        SimpleDateFormat serverDateFormate = new SimpleDateFormat(serverDateFormat);
        SimpleDateFormat displayDateFormate = new SimpleDateFormat(displayDateFormat);

        String rDate = "";

        try {

            Date date1 = serverDateFormate.parse(date);
            rDate = displayDateFormate.format(date1);

        } catch (Exception e) {
            Log.e(TAG, "ERROR : "+e.getMessage());
        }

        return rDate;

    }
}
