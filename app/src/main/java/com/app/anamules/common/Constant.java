package com.app.anamules.common;

public class Constant
{
    public static double lat_=0.0;
    public static double lon_=0.0;

    public static String FROM_RECENTLY_PLAYED = "recently_played";
    public static String FROM_GOLF_NEAR_ME = "golf_near_me";
    public static String FROM_CURRENT_ZOO = "Current Zoo";
    public static String FROM_THE_ZOO = "The Zoo";
    public static String SELECT_TYPE = "select_type";
    public static String GAME_DETAIL = "game_detail";
    public static String USER_ZOO = "USER_ZOO";
    public static String FROM_USER_ZOO = "FROM_USER_ZOO";
    public static String USER_LIST = "USER_LIST";
    public static String HOLE_NUMBER = "hole_number";
    public static String BARCODE = "barcode";
    public static String TYPE_PLAYER = "TYPER_PLAYER";
    public static String PLAYER_LIST = "PLAYER_LIST";
    public static String GOLF_COURCE = "golf_cource";
    public static String PDF_FILE_NAME = "pdf_file_name";

    public static String PLAYER_ID = "player_id";
    public static String LIST_PLAYERS = "player_list";
    public static String IS_SELECT = "IS_SELECT";

    public static String NAME = "name";
    public static String MOBILE_NO = "mobile_no";


    public static int SELECTED_GOLF_COUSE = 101;
    public static String FROM = "from";
    public static String FROM_SEARCH = "search";

    public static int SELECTED_PLAYER = 102;
    public static int SELECTED_CONTACT = 103;
    public static int PERMISSION_READ = 103;

    public static final int REQUEST_WRITE_PERMISSION = 20;
    public static final int GALLERY_PERMISSION = 21;
    public static final int PHOTO_REQUEST = 10;
    public static final int OPEN_GALLERY = 22;

    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SELECTED_PLAYER_DATA = "player_data";

    public static final String SELECTED_GOLF = "golf_course";
    public static final int SELECTED_GOLF_RQQUEST = 104;
}
