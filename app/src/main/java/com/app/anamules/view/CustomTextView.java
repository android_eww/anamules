package com.app.anamules.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.app.anamules.R;

import java.util.HashMap;

public class CustomTextView extends AppCompatTextView
{
    HashMap<String, Typeface> typefaceMap;
    Context context;

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context,attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context,attrs);
    }

    private void setTypeFace(Context context, AttributeSet attrs)
    {
        if(typefaceMap == null)
        {
            typefaceMap = new HashMap<String, Typeface>();
        }
        this.context = context;
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        if(typedArray != null)
        {
            String typefacepath = typedArray.getString(R.styleable.CustomTextView_customTypeFaceTv);

            if(typefacepath != null)
            {
                Typeface typeface = null;
                if(typefaceMap.containsKey(typefacepath))
                {
                    typeface = typefaceMap.get(typefacepath);
                }
                else
                {
                    AssetManager asset = context.getAssets();
                    typeface = Typeface.createFromAsset(asset,typefacepath);
                    typefaceMap.put(typefacepath,typeface);
                }

                setTypeface(typeface);
            }
            else
            {
                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),context.getResources().getString(R.string.tf_regular));
                this.setTypeface(typeface);
            }

            typedArray.recycle();
        }
    }
}
