package com.app.anamules.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.app.anamules.R;

import java.util.HashMap;

public class CustomEditText extends AppCompatEditText
{

    HashMap<String, Typeface> typefaceMap;
    Context context;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context,attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context,attrs);
    }

    private void setTypeFace(Context context, AttributeSet attrs)
    {
        if(typefaceMap == null)
        {
            typefaceMap = new HashMap<>();
        }
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);

        if(typedArray != null)
        {
            String value = typedArray.getString(R.styleable.CustomEditText_customTypeFaceEt);
            if(value != null)
            {
                Typeface typeface = null;
                if(typefaceMap.containsKey(value))
                {
                    typeface = typefaceMap.get(value);
                }
                else
                {
                    AssetManager assetManager = context.getAssets();
                    typeface = Typeface.createFromAsset(assetManager,value);
                    typefaceMap.put(value,typeface);
                }

                setTypeface(typeface);
            }
            else
            {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.tf_regular));
                setTypeface(typeface);
            }

            typedArray.recycle();
        }
    }
}
